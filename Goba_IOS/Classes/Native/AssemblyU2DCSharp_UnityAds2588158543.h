﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityAds
struct UnityAds_t2588158543;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityAds
struct  UnityAds_t2588158543  : public MonoBehaviour_t1158329972
{
public:
	// System.String UnityAds::androidAppID
	String_t* ___androidAppID_3;
	// System.String UnityAds::iOSAppID
	String_t* ___iOSAppID_4;

public:
	inline static int32_t get_offset_of_androidAppID_3() { return static_cast<int32_t>(offsetof(UnityAds_t2588158543, ___androidAppID_3)); }
	inline String_t* get_androidAppID_3() const { return ___androidAppID_3; }
	inline String_t** get_address_of_androidAppID_3() { return &___androidAppID_3; }
	inline void set_androidAppID_3(String_t* value)
	{
		___androidAppID_3 = value;
		Il2CppCodeGenWriteBarrier(&___androidAppID_3, value);
	}

	inline static int32_t get_offset_of_iOSAppID_4() { return static_cast<int32_t>(offsetof(UnityAds_t2588158543, ___iOSAppID_4)); }
	inline String_t* get_iOSAppID_4() const { return ___iOSAppID_4; }
	inline String_t** get_address_of_iOSAppID_4() { return &___iOSAppID_4; }
	inline void set_iOSAppID_4(String_t* value)
	{
		___iOSAppID_4 = value;
		Il2CppCodeGenWriteBarrier(&___iOSAppID_4, value);
	}
};

struct UnityAds_t2588158543_StaticFields
{
public:
	// UnityAds UnityAds::instance
	UnityAds_t2588158543 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(UnityAds_t2588158543_StaticFields, ___instance_2)); }
	inline UnityAds_t2588158543 * get_instance_2() const { return ___instance_2; }
	inline UnityAds_t2588158543 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(UnityAds_t2588158543 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
