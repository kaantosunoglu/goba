﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_EnumUtil1099402118.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_EnumUtil3288905799.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_BufferUt1200764395.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_JavaScri4013793858.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_JsonToken387653842.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_LateBoun3208546116.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_LateBound322633509.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_MathUtils722929707.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Miscella2828154915.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Property1902004952.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Property1762676876.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflecti2294713146.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflecti4222298801.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflecti1424089768.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflecti1856937010.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflectio290853069.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflectio694137596.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflecti3884221258.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflecti2709377751.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Reflecti3402508478.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_StringBu2484172789.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_StringRe1873850304.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_StringRef223215850.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_StringUt1768673176.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_TypeExte2974070984.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Validati1621959402.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Reso1785396551.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa3395990442.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa1136768961.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa1223709752.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defau571394784.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defau571394785.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defau571394786.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa1209949948.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa1593031697.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa1593031666.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa1062508680.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa2461993261.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Defa3055062677.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Error615697659.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Erro3365615597.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json2625589241.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonC793504011.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json4218948260.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Seri1081880527.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Seri3980565167.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Exte1323311354.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Exte2652384534.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json1566984540.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json1580437102.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json4289117481.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json3573211912.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json3196859494.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonI122701872.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json3556659186.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json2091736265.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json2604782005.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json2712067825.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json3302934105.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonS795582376.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json2304948129.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json3254279720.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonS220200932.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json3614597300.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json4254133687.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonS352056653.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json3234521618.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonSe27144642.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json2979008531.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Json1473969596.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonT104114218.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_JsonT614151917.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Nami3996993727.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_OnEr1208384365.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Reflec49628816.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Refle879419187.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Trac2055941074.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Serialization_Trace821541056.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_CommentHandli3545414906.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_LineInfoHandl2747370961.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_Extensions784000136.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2000[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (EnumUtils_t1099402118), -1, sizeof(EnumUtils_t1099402118_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2001[1] = 
{
	EnumUtils_t1099402118_StaticFields::get_offset_of_EnumMemberNamesPerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (U3CU3Ec_t3288905799), -1, sizeof(U3CU3Ec_t3288905799_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2002[3] = 
{
	U3CU3Ec_t3288905799_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3288905799_StaticFields::get_offset_of_U3CU3E9__1_0_1(),
	U3CU3Ec_t3288905799_StaticFields::get_offset_of_U3CU3E9__5_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (BufferUtils_t1200764395), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (JavaScriptUtils_t4013793858), -1, sizeof(JavaScriptUtils_t4013793858_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2004[3] = 
{
	JavaScriptUtils_t4013793858_StaticFields::get_offset_of_SingleQuoteCharEscapeFlags_0(),
	JavaScriptUtils_t4013793858_StaticFields::get_offset_of_DoubleQuoteCharEscapeFlags_1(),
	JavaScriptUtils_t4013793858_StaticFields::get_offset_of_HtmlCharEscapeFlags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (JsonTokenUtils_t387653842), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (LateBoundReflectionDelegateFactory_t3208546116), -1, sizeof(LateBoundReflectionDelegateFactory_t3208546116_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2006[1] = 
{
	LateBoundReflectionDelegateFactory_t3208546116_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (U3CU3Ec__DisplayClass3_0_t322633509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2007[2] = 
{
	U3CU3Ec__DisplayClass3_0_t322633509::get_offset_of_c_0(),
	U3CU3Ec__DisplayClass3_0_t322633509::get_offset_of_method_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2009[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2010[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2011[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2012[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2013[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (MathUtils_t722929707), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (MiscellaneousUtils_t2828154915), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (PropertyNameTable_t1902004952), -1, sizeof(PropertyNameTable_t1902004952_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2017[4] = 
{
	PropertyNameTable_t1902004952_StaticFields::get_offset_of_HashCodeRandomizer_0(),
	PropertyNameTable_t1902004952::get_offset_of__count_1(),
	PropertyNameTable_t1902004952::get_offset_of__entries_2(),
	PropertyNameTable_t1902004952::get_offset_of__mask_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (Entry_t1762676876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2018[3] = 
{
	Entry_t1762676876::get_offset_of_Value_0(),
	Entry_t1762676876::get_offset_of_HashCode_1(),
	Entry_t1762676876::get_offset_of_Next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (ReflectionDelegateFactory_t2294713146), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (ReflectionMember_t4222298801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2020[3] = 
{
	ReflectionMember_t4222298801::get_offset_of_U3CMemberTypeU3Ek__BackingField_0(),
	ReflectionMember_t4222298801::get_offset_of_U3CGetterU3Ek__BackingField_1(),
	ReflectionMember_t4222298801::get_offset_of_U3CSetterU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (ReflectionObject_t1424089768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2021[2] = 
{
	ReflectionObject_t1424089768::get_offset_of_U3CCreatorU3Ek__BackingField_0(),
	ReflectionObject_t1424089768::get_offset_of_U3CMembersU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (U3CU3Ec__DisplayClass13_0_t1856937010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2022[1] = 
{
	U3CU3Ec__DisplayClass13_0_t1856937010::get_offset_of_ctor_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (U3CU3Ec__DisplayClass13_1_t290853069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2023[1] = 
{
	U3CU3Ec__DisplayClass13_1_t290853069::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (U3CU3Ec__DisplayClass13_2_t694137596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2024[1] = 
{
	U3CU3Ec__DisplayClass13_2_t694137596::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (ReflectionUtils_t3884221258), -1, sizeof(ReflectionUtils_t3884221258_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2025[1] = 
{
	ReflectionUtils_t3884221258_StaticFields::get_offset_of_EmptyTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (U3CU3Ec_t2709377751), -1, sizeof(U3CU3Ec_t2709377751_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2026[5] = 
{
	U3CU3Ec_t2709377751_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2709377751_StaticFields::get_offset_of_U3CU3E9__10_0_1(),
	U3CU3Ec_t2709377751_StaticFields::get_offset_of_U3CU3E9__29_0_2(),
	U3CU3Ec_t2709377751_StaticFields::get_offset_of_U3CU3E9__37_0_3(),
	U3CU3Ec_t2709377751_StaticFields::get_offset_of_U3CU3E9__39_0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (U3CU3Ec__DisplayClass42_0_t3402508478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2027[1] = 
{
	U3CU3Ec__DisplayClass42_0_t3402508478::get_offset_of_subTypeProperty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (StringBuffer_t2484172789)+ sizeof (Il2CppObject), sizeof(StringBuffer_t2484172789_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2028[2] = 
{
	StringBuffer_t2484172789::get_offset_of__buffer_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringBuffer_t2484172789::get_offset_of__position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (StringReference_t1873850304)+ sizeof (Il2CppObject), sizeof(StringReference_t1873850304_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2029[3] = 
{
	StringReference_t1873850304::get_offset_of__chars_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringReference_t1873850304::get_offset_of__startIndex_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringReference_t1873850304::get_offset_of__length_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (StringReferenceExtensions_t223215850), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (StringUtils_t1768673176), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2032[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2033[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (TypeExtensions_t2974070984), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (ValidationUtils_t1621959402), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2036[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (ResolverContractKey_t1785396551)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2037[2] = 
{
	ResolverContractKey_t1785396551::get_offset_of__resolverType_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ResolverContractKey_t1785396551::get_offset_of__contractType_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (DefaultContractResolverState_t3395990442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2038[2] = 
{
	DefaultContractResolverState_t3395990442::get_offset_of_ContractCache_0(),
	DefaultContractResolverState_t3395990442::get_offset_of_NameTable_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (DefaultContractResolver_t1136768961), -1, sizeof(DefaultContractResolver_t1136768961_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2039[11] = 
{
	DefaultContractResolver_t1136768961_StaticFields::get_offset_of__instance_0(),
	DefaultContractResolver_t1136768961_StaticFields::get_offset_of_BuiltInConverters_1(),
	DefaultContractResolver_t1136768961_StaticFields::get_offset_of_TypeContractCacheLock_2(),
	DefaultContractResolver_t1136768961_StaticFields::get_offset_of__sharedState_3(),
	DefaultContractResolver_t1136768961::get_offset_of__instanceState_4(),
	DefaultContractResolver_t1136768961::get_offset_of__sharedCache_5(),
	DefaultContractResolver_t1136768961::get_offset_of_U3CDefaultMembersSearchFlagsU3Ek__BackingField_6(),
	DefaultContractResolver_t1136768961::get_offset_of_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7(),
	DefaultContractResolver_t1136768961::get_offset_of_U3CIgnoreSerializableInterfaceU3Ek__BackingField_8(),
	DefaultContractResolver_t1136768961::get_offset_of_U3CIgnoreSerializableAttributeU3Ek__BackingField_9(),
	DefaultContractResolver_t1136768961::get_offset_of_U3CNamingStrategyU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2040[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2041[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (U3CU3Ec_t1223709752), -1, sizeof(U3CU3Ec_t1223709752_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2042[7] = 
{
	U3CU3Ec_t1223709752_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1223709752_StaticFields::get_offset_of_U3CU3E9__34_0_1(),
	U3CU3Ec_t1223709752_StaticFields::get_offset_of_U3CU3E9__34_1_2(),
	U3CU3Ec_t1223709752_StaticFields::get_offset_of_U3CU3E9__37_0_3(),
	U3CU3Ec_t1223709752_StaticFields::get_offset_of_U3CU3E9__37_1_4(),
	U3CU3Ec_t1223709752_StaticFields::get_offset_of_U3CU3E9__40_0_5(),
	U3CU3Ec_t1223709752_StaticFields::get_offset_of_U3CU3E9__64_0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (U3CU3Ec__DisplayClass38_0_t571394784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2043[2] = 
{
	U3CU3Ec__DisplayClass38_0_t571394784::get_offset_of_getExtensionDataDictionary_0(),
	U3CU3Ec__DisplayClass38_0_t571394784::get_offset_of_member_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (U3CU3Ec__DisplayClass38_1_t571394785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2044[4] = 
{
	U3CU3Ec__DisplayClass38_1_t571394785::get_offset_of_setExtensionDataDictionary_0(),
	U3CU3Ec__DisplayClass38_1_t571394785::get_offset_of_createExtensionDataDictionary_1(),
	U3CU3Ec__DisplayClass38_1_t571394785::get_offset_of_setExtensionDataDictionaryValue_2(),
	U3CU3Ec__DisplayClass38_1_t571394785::get_offset_of_CSU24U3CU3E8__locals1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (U3CU3Ec__DisplayClass38_2_t571394786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2045[2] = 
{
	U3CU3Ec__DisplayClass38_2_t571394786::get_offset_of_createEnumerableWrapper_0(),
	U3CU3Ec__DisplayClass38_2_t571394786::get_offset_of_CSU24U3CU3E8__locals2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (U3CU3Ec__DisplayClass52_0_t1209949948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2046[1] = 
{
	U3CU3Ec__DisplayClass52_0_t1209949948::get_offset_of_namingStrategy_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (U3CU3Ec__DisplayClass68_0_t1593031697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2047[1] = 
{
	U3CU3Ec__DisplayClass68_0_t1593031697::get_offset_of_shouldSerializeCall_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (U3CU3Ec__DisplayClass69_0_t1593031666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2048[1] = 
{
	U3CU3Ec__DisplayClass69_0_t1593031666::get_offset_of_specifiedPropertyGet_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (DefaultReferenceResolver_t1062508680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2049[1] = 
{
	DefaultReferenceResolver_t1062508680::get_offset_of__referenceCount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (DefaultSerializationBinder_t2461993261), -1, sizeof(DefaultSerializationBinder_t2461993261_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2050[2] = 
{
	DefaultSerializationBinder_t2461993261_StaticFields::get_offset_of_Instance_0(),
	DefaultSerializationBinder_t2461993261::get_offset_of__typeCache_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (TypeNameKey_t3055062677)+ sizeof (Il2CppObject), sizeof(TypeNameKey_t3055062677_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2051[2] = 
{
	TypeNameKey_t3055062677::get_offset_of_AssemblyName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TypeNameKey_t3055062677::get_offset_of_TypeName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (ErrorContext_t615697659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2052[6] = 
{
	ErrorContext_t615697659::get_offset_of_U3CTracedU3Ek__BackingField_0(),
	ErrorContext_t615697659::get_offset_of_U3CErrorU3Ek__BackingField_1(),
	ErrorContext_t615697659::get_offset_of_U3COriginalObjectU3Ek__BackingField_2(),
	ErrorContext_t615697659::get_offset_of_U3CMemberU3Ek__BackingField_3(),
	ErrorContext_t615697659::get_offset_of_U3CPathU3Ek__BackingField_4(),
	ErrorContext_t615697659::get_offset_of_U3CHandledU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (ErrorEventArgs_t3365615597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2053[2] = 
{
	ErrorEventArgs_t3365615597::get_offset_of_U3CCurrentObjectU3Ek__BackingField_1(),
	ErrorEventArgs_t3365615597::get_offset_of_U3CErrorContextU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (JsonArrayContract_t2625589241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2059[13] = 
{
	JsonArrayContract_t2625589241::get_offset_of_U3CCollectionItemTypeU3Ek__BackingField_27(),
	JsonArrayContract_t2625589241::get_offset_of_U3CIsMultidimensionalArrayU3Ek__BackingField_28(),
	JsonArrayContract_t2625589241::get_offset_of__genericCollectionDefinitionType_29(),
	JsonArrayContract_t2625589241::get_offset_of__genericWrapperType_30(),
	JsonArrayContract_t2625589241::get_offset_of__genericWrapperCreator_31(),
	JsonArrayContract_t2625589241::get_offset_of__genericTemporaryCollectionCreator_32(),
	JsonArrayContract_t2625589241::get_offset_of_U3CIsArrayU3Ek__BackingField_33(),
	JsonArrayContract_t2625589241::get_offset_of_U3CShouldCreateWrapperU3Ek__BackingField_34(),
	JsonArrayContract_t2625589241::get_offset_of_U3CCanDeserializeU3Ek__BackingField_35(),
	JsonArrayContract_t2625589241::get_offset_of__parameterizedConstructor_36(),
	JsonArrayContract_t2625589241::get_offset_of__parameterizedCreator_37(),
	JsonArrayContract_t2625589241::get_offset_of__overrideCreator_38(),
	JsonArrayContract_t2625589241::get_offset_of_U3CHasParameterizedCreatorU3Ek__BackingField_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (JsonContainerContract_t793504011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2060[6] = 
{
	JsonContainerContract_t793504011::get_offset_of__itemContract_21(),
	JsonContainerContract_t793504011::get_offset_of__finalItemContract_22(),
	JsonContainerContract_t793504011::get_offset_of_U3CItemConverterU3Ek__BackingField_23(),
	JsonContainerContract_t793504011::get_offset_of_U3CItemIsReferenceU3Ek__BackingField_24(),
	JsonContainerContract_t793504011::get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_25(),
	JsonContainerContract_t793504011::get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (JsonContractType_t4218948260)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2061[10] = 
{
	JsonContractType_t4218948260::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (SerializationCallback_t1081880527), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (SerializationErrorCallback_t3980565167), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (ExtensionDataSetter_t1323311354), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (ExtensionDataGetter_t2652384534), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (JsonContract_t1566984540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2066[21] = 
{
	JsonContract_t1566984540::get_offset_of_IsNullable_0(),
	JsonContract_t1566984540::get_offset_of_IsConvertable_1(),
	JsonContract_t1566984540::get_offset_of_IsEnum_2(),
	JsonContract_t1566984540::get_offset_of_NonNullableUnderlyingType_3(),
	JsonContract_t1566984540::get_offset_of_InternalReadType_4(),
	JsonContract_t1566984540::get_offset_of_ContractType_5(),
	JsonContract_t1566984540::get_offset_of_IsReadOnlyOrFixedSize_6(),
	JsonContract_t1566984540::get_offset_of_IsSealed_7(),
	JsonContract_t1566984540::get_offset_of_IsInstantiable_8(),
	JsonContract_t1566984540::get_offset_of__onDeserializedCallbacks_9(),
	JsonContract_t1566984540::get_offset_of__onDeserializingCallbacks_10(),
	JsonContract_t1566984540::get_offset_of__onSerializedCallbacks_11(),
	JsonContract_t1566984540::get_offset_of__onSerializingCallbacks_12(),
	JsonContract_t1566984540::get_offset_of__onErrorCallbacks_13(),
	JsonContract_t1566984540::get_offset_of__createdType_14(),
	JsonContract_t1566984540::get_offset_of_U3CUnderlyingTypeU3Ek__BackingField_15(),
	JsonContract_t1566984540::get_offset_of_U3CIsReferenceU3Ek__BackingField_16(),
	JsonContract_t1566984540::get_offset_of_U3CConverterU3Ek__BackingField_17(),
	JsonContract_t1566984540::get_offset_of_U3CInternalConverterU3Ek__BackingField_18(),
	JsonContract_t1566984540::get_offset_of_U3CDefaultCreatorU3Ek__BackingField_19(),
	JsonContract_t1566984540::get_offset_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (U3CU3Ec__DisplayClass73_0_t1580437102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2067[1] = 
{
	U3CU3Ec__DisplayClass73_0_t1580437102::get_offset_of_callbackMethodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (U3CU3Ec__DisplayClass74_0_t4289117481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2068[1] = 
{
	U3CU3Ec__DisplayClass74_0_t4289117481::get_offset_of_callbackMethodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (JsonDictionaryContract_t3573211912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2069[13] = 
{
	JsonDictionaryContract_t3573211912::get_offset_of_U3CDictionaryKeyResolverU3Ek__BackingField_27(),
	JsonDictionaryContract_t3573211912::get_offset_of_U3CDictionaryKeyTypeU3Ek__BackingField_28(),
	JsonDictionaryContract_t3573211912::get_offset_of_U3CDictionaryValueTypeU3Ek__BackingField_29(),
	JsonDictionaryContract_t3573211912::get_offset_of_U3CKeyContractU3Ek__BackingField_30(),
	JsonDictionaryContract_t3573211912::get_offset_of__genericCollectionDefinitionType_31(),
	JsonDictionaryContract_t3573211912::get_offset_of__genericWrapperType_32(),
	JsonDictionaryContract_t3573211912::get_offset_of__genericWrapperCreator_33(),
	JsonDictionaryContract_t3573211912::get_offset_of__genericTemporaryDictionaryCreator_34(),
	JsonDictionaryContract_t3573211912::get_offset_of_U3CShouldCreateWrapperU3Ek__BackingField_35(),
	JsonDictionaryContract_t3573211912::get_offset_of__parameterizedConstructor_36(),
	JsonDictionaryContract_t3573211912::get_offset_of__overrideCreator_37(),
	JsonDictionaryContract_t3573211912::get_offset_of__parameterizedCreator_38(),
	JsonDictionaryContract_t3573211912::get_offset_of_U3CHasParameterizedCreatorU3Ek__BackingField_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (JsonFormatterConverter_t3196859494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2070[3] = 
{
	JsonFormatterConverter_t3196859494::get_offset_of__reader_0(),
	JsonFormatterConverter_t3196859494::get_offset_of__contract_1(),
	JsonFormatterConverter_t3196859494::get_offset_of__member_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (JsonISerializableContract_t122701872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2071[1] = 
{
	JsonISerializableContract_t122701872::get_offset_of_U3CISerializableCreatorU3Ek__BackingField_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (JsonLinqContract_t3556659186), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (JsonObjectContract_t2091736265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2073[13] = 
{
	JsonObjectContract_t2091736265::get_offset_of_U3CMemberSerializationU3Ek__BackingField_27(),
	JsonObjectContract_t2091736265::get_offset_of_U3CItemRequiredU3Ek__BackingField_28(),
	JsonObjectContract_t2091736265::get_offset_of_U3CPropertiesU3Ek__BackingField_29(),
	JsonObjectContract_t2091736265::get_offset_of_U3CExtensionDataSetterU3Ek__BackingField_30(),
	JsonObjectContract_t2091736265::get_offset_of_U3CExtensionDataGetterU3Ek__BackingField_31(),
	JsonObjectContract_t2091736265::get_offset_of_ExtensionDataIsJToken_32(),
	JsonObjectContract_t2091736265::get_offset_of__hasRequiredOrDefaultValueProperties_33(),
	JsonObjectContract_t2091736265::get_offset_of__parametrizedConstructor_34(),
	JsonObjectContract_t2091736265::get_offset_of__overrideConstructor_35(),
	JsonObjectContract_t2091736265::get_offset_of__overrideCreator_36(),
	JsonObjectContract_t2091736265::get_offset_of__parameterizedCreator_37(),
	JsonObjectContract_t2091736265::get_offset_of__creatorParameters_38(),
	JsonObjectContract_t2091736265::get_offset_of__extensionDataValueType_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (JsonPrimitiveContract_t2604782005), -1, sizeof(JsonPrimitiveContract_t2604782005_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2074[2] = 
{
	JsonPrimitiveContract_t2604782005::get_offset_of_U3CTypeCodeU3Ek__BackingField_21(),
	JsonPrimitiveContract_t2604782005_StaticFields::get_offset_of_ReadTypeMap_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (JsonProperty_t2712067825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2075[33] = 
{
	JsonProperty_t2712067825::get_offset_of__required_0(),
	JsonProperty_t2712067825::get_offset_of__hasExplicitDefaultValue_1(),
	JsonProperty_t2712067825::get_offset_of__defaultValue_2(),
	JsonProperty_t2712067825::get_offset_of__hasGeneratedDefaultValue_3(),
	JsonProperty_t2712067825::get_offset_of__propertyName_4(),
	JsonProperty_t2712067825::get_offset_of__skipPropertyNameEscape_5(),
	JsonProperty_t2712067825::get_offset_of__propertyType_6(),
	JsonProperty_t2712067825::get_offset_of_U3CPropertyContractU3Ek__BackingField_7(),
	JsonProperty_t2712067825::get_offset_of_U3CDeclaringTypeU3Ek__BackingField_8(),
	JsonProperty_t2712067825::get_offset_of_U3COrderU3Ek__BackingField_9(),
	JsonProperty_t2712067825::get_offset_of_U3CUnderlyingNameU3Ek__BackingField_10(),
	JsonProperty_t2712067825::get_offset_of_U3CValueProviderU3Ek__BackingField_11(),
	JsonProperty_t2712067825::get_offset_of_U3CAttributeProviderU3Ek__BackingField_12(),
	JsonProperty_t2712067825::get_offset_of_U3CConverterU3Ek__BackingField_13(),
	JsonProperty_t2712067825::get_offset_of_U3CMemberConverterU3Ek__BackingField_14(),
	JsonProperty_t2712067825::get_offset_of_U3CIgnoredU3Ek__BackingField_15(),
	JsonProperty_t2712067825::get_offset_of_U3CReadableU3Ek__BackingField_16(),
	JsonProperty_t2712067825::get_offset_of_U3CWritableU3Ek__BackingField_17(),
	JsonProperty_t2712067825::get_offset_of_U3CHasMemberAttributeU3Ek__BackingField_18(),
	JsonProperty_t2712067825::get_offset_of_U3CIsReferenceU3Ek__BackingField_19(),
	JsonProperty_t2712067825::get_offset_of_U3CNullValueHandlingU3Ek__BackingField_20(),
	JsonProperty_t2712067825::get_offset_of_U3CDefaultValueHandlingU3Ek__BackingField_21(),
	JsonProperty_t2712067825::get_offset_of_U3CReferenceLoopHandlingU3Ek__BackingField_22(),
	JsonProperty_t2712067825::get_offset_of_U3CObjectCreationHandlingU3Ek__BackingField_23(),
	JsonProperty_t2712067825::get_offset_of_U3CTypeNameHandlingU3Ek__BackingField_24(),
	JsonProperty_t2712067825::get_offset_of_U3CShouldSerializeU3Ek__BackingField_25(),
	JsonProperty_t2712067825::get_offset_of_U3CShouldDeserializeU3Ek__BackingField_26(),
	JsonProperty_t2712067825::get_offset_of_U3CGetIsSpecifiedU3Ek__BackingField_27(),
	JsonProperty_t2712067825::get_offset_of_U3CSetIsSpecifiedU3Ek__BackingField_28(),
	JsonProperty_t2712067825::get_offset_of_U3CItemConverterU3Ek__BackingField_29(),
	JsonProperty_t2712067825::get_offset_of_U3CItemIsReferenceU3Ek__BackingField_30(),
	JsonProperty_t2712067825::get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_31(),
	JsonProperty_t2712067825::get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (JsonPropertyCollection_t3302934105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2076[2] = 
{
	JsonPropertyCollection_t3302934105::get_offset_of__type_5(),
	JsonPropertyCollection_t3302934105::get_offset_of__list_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (JsonSerializerInternalBase_t795582376), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2077[5] = 
{
	JsonSerializerInternalBase_t795582376::get_offset_of__currentErrorContext_0(),
	JsonSerializerInternalBase_t795582376::get_offset_of__mappings_1(),
	JsonSerializerInternalBase_t795582376::get_offset_of_Serializer_2(),
	JsonSerializerInternalBase_t795582376::get_offset_of_TraceWriter_3(),
	JsonSerializerInternalBase_t795582376::get_offset_of_InternalSerializer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (ReferenceEqualsEqualityComparer_t2304948129), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (JsonSerializerInternalReader_t3254279720), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (PropertyPresence_t220200932)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2080[4] = 
{
	PropertyPresence_t220200932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (CreatorPropertyContext_t3614597300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2081[6] = 
{
	CreatorPropertyContext_t3614597300::get_offset_of_Name_0(),
	CreatorPropertyContext_t3614597300::get_offset_of_Property_1(),
	CreatorPropertyContext_t3614597300::get_offset_of_ConstructorProperty_2(),
	CreatorPropertyContext_t3614597300::get_offset_of_Presence_3(),
	CreatorPropertyContext_t3614597300::get_offset_of_Value_4(),
	CreatorPropertyContext_t3614597300::get_offset_of_Used_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (U3CU3Ec__DisplayClass36_0_t4254133687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2082[1] = 
{
	U3CU3Ec__DisplayClass36_0_t4254133687::get_offset_of_property_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (U3CU3Ec_t352056653), -1, sizeof(U3CU3Ec_t352056653_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2083[5] = 
{
	U3CU3Ec_t352056653_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t352056653_StaticFields::get_offset_of_U3CU3E9__36_0_1(),
	U3CU3Ec_t352056653_StaticFields::get_offset_of_U3CU3E9__36_2_2(),
	U3CU3Ec_t352056653_StaticFields::get_offset_of_U3CU3E9__41_0_3(),
	U3CU3Ec_t352056653_StaticFields::get_offset_of_U3CU3E9__41_1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (JsonSerializerInternalWriter_t3234521618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2084[3] = 
{
	JsonSerializerInternalWriter_t3234521618::get_offset_of__rootType_5(),
	JsonSerializerInternalWriter_t3234521618::get_offset_of__rootLevel_6(),
	JsonSerializerInternalWriter_t3234521618::get_offset_of__serializeStack_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (JsonSerializerProxy_t27144642), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2085[3] = 
{
	JsonSerializerProxy_t27144642::get_offset_of__serializerReader_31(),
	JsonSerializerProxy_t27144642::get_offset_of__serializerWriter_32(),
	JsonSerializerProxy_t27144642::get_offset_of__serializer_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (JsonStringContract_t2979008531), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (JsonTypeReflector_t1473969596), -1, sizeof(JsonTypeReflector_t1473969596_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2087[4] = 
{
	JsonTypeReflector_t1473969596_StaticFields::get_offset_of__fullyTrusted_0(),
	JsonTypeReflector_t1473969596_StaticFields::get_offset_of_CreatorCache_1(),
	JsonTypeReflector_t1473969596_StaticFields::get_offset_of_AssociatedMetadataTypesCache_2(),
	JsonTypeReflector_t1473969596_StaticFields::get_offset_of__metadataTypeAttributeReflectionObject_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (U3CU3Ec__DisplayClass20_0_t104114218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2088[2] = 
{
	U3CU3Ec__DisplayClass20_0_t104114218::get_offset_of_type_0(),
	U3CU3Ec__DisplayClass20_0_t104114218::get_offset_of_defaultConstructor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (U3CU3Ec_t614151917), -1, sizeof(U3CU3Ec_t614151917_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2089[2] = 
{
	U3CU3Ec_t614151917_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t614151917_StaticFields::get_offset_of_U3CU3E9__20_1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (NamingStrategy_t3996993727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2090[2] = 
{
	NamingStrategy_t3996993727::get_offset_of_U3CProcessDictionaryKeysU3Ek__BackingField_0(),
	NamingStrategy_t3996993727::get_offset_of_U3COverrideSpecifiedNamesU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (OnErrorAttribute_t1208384365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (ReflectionAttributeProvider_t49628816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2093[1] = 
{
	ReflectionAttributeProvider_t49628816::get_offset_of__attributeProvider_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (ReflectionValueProvider_t879419187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2094[1] = 
{
	ReflectionValueProvider_t879419187::get_offset_of__memberInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (TraceJsonReader_t2055941074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2095[3] = 
{
	TraceJsonReader_t2055941074::get_offset_of__innerReader_15(),
	TraceJsonReader_t2055941074::get_offset_of__textWriter_16(),
	TraceJsonReader_t2055941074::get_offset_of__sw_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (TraceJsonWriter_t821541056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2096[3] = 
{
	TraceJsonWriter_t821541056::get_offset_of__innerWriter_13(),
	TraceJsonWriter_t821541056::get_offset_of__textWriter_14(),
	TraceJsonWriter_t821541056::get_offset_of__sw_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (CommentHandling_t3545414906)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2097[3] = 
{
	CommentHandling_t3545414906::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (LineInfoHandling_t2747370961)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2098[3] = 
{
	LineInfoHandling_t2747370961::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (Extensions_t784000136), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
