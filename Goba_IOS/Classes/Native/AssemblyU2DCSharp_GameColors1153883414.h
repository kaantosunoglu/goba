﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// GameColor[]
struct GameColorU5BU5D_t2027709500;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameColors
struct  GameColors_t1153883414  : public Il2CppObject
{
public:
	// GameColor[] GameColors::<gameColors>k__BackingField
	GameColorU5BU5D_t2027709500* ___U3CgameColorsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CgameColorsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GameColors_t1153883414, ___U3CgameColorsU3Ek__BackingField_0)); }
	inline GameColorU5BU5D_t2027709500* get_U3CgameColorsU3Ek__BackingField_0() const { return ___U3CgameColorsU3Ek__BackingField_0; }
	inline GameColorU5BU5D_t2027709500** get_address_of_U3CgameColorsU3Ek__BackingField_0() { return &___U3CgameColorsU3Ek__BackingField_0; }
	inline void set_U3CgameColorsU3Ek__BackingField_0(GameColorU5BU5D_t2027709500* value)
	{
		___U3CgameColorsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CgameColorsU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
