﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// ParticlePool
struct ParticlePool_t3081421488;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticlePool
struct  ParticlePool_t3081421488  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ParticlePool::ParticleRing
	GameObject_t1756533147 * ___ParticleRing_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ParticlePool::ParticleRings
	List_1_t1125654279 * ___ParticleRings_4;

public:
	inline static int32_t get_offset_of_ParticleRing_3() { return static_cast<int32_t>(offsetof(ParticlePool_t3081421488, ___ParticleRing_3)); }
	inline GameObject_t1756533147 * get_ParticleRing_3() const { return ___ParticleRing_3; }
	inline GameObject_t1756533147 ** get_address_of_ParticleRing_3() { return &___ParticleRing_3; }
	inline void set_ParticleRing_3(GameObject_t1756533147 * value)
	{
		___ParticleRing_3 = value;
		Il2CppCodeGenWriteBarrier(&___ParticleRing_3, value);
	}

	inline static int32_t get_offset_of_ParticleRings_4() { return static_cast<int32_t>(offsetof(ParticlePool_t3081421488, ___ParticleRings_4)); }
	inline List_1_t1125654279 * get_ParticleRings_4() const { return ___ParticleRings_4; }
	inline List_1_t1125654279 ** get_address_of_ParticleRings_4() { return &___ParticleRings_4; }
	inline void set_ParticleRings_4(List_1_t1125654279 * value)
	{
		___ParticleRings_4 = value;
		Il2CppCodeGenWriteBarrier(&___ParticleRings_4, value);
	}
};

struct ParticlePool_t3081421488_StaticFields
{
public:
	// ParticlePool ParticlePool::instance
	ParticlePool_t3081421488 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(ParticlePool_t3081421488_StaticFields, ___instance_2)); }
	inline ParticlePool_t3081421488 * get_instance_2() const { return ___instance_2; }
	inline ParticlePool_t3081421488 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(ParticlePool_t3081421488 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
