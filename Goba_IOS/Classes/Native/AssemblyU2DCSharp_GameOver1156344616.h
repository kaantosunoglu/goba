﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameOver
struct  GameOver_t1156344616  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioClip GameOver::GameOverAudio
	AudioClip_t1932558630 * ___GameOverAudio_2;

public:
	inline static int32_t get_offset_of_GameOverAudio_2() { return static_cast<int32_t>(offsetof(GameOver_t1156344616, ___GameOverAudio_2)); }
	inline AudioClip_t1932558630 * get_GameOverAudio_2() const { return ___GameOverAudio_2; }
	inline AudioClip_t1932558630 ** get_address_of_GameOverAudio_2() { return &___GameOverAudio_2; }
	inline void set_GameOverAudio_2(AudioClip_t1932558630 * value)
	{
		___GameOverAudio_2 = value;
		Il2CppCodeGenWriteBarrier(&___GameOverAudio_2, value);
	}
};

struct GameOver_t1156344616_StaticFields
{
public:
	// System.Action`1<System.Boolean> GameOver::<>f__am$cache0
	Action_1_t3627374100 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(GameOver_t1156344616_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
