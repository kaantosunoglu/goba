﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Action`1<UnityEngine.Vector2>
struct Action_1_t2045506961;
// System.Action
struct Action_t3226471752;
// InputManager
struct InputManager_t1610719423;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InputManager
struct  InputManager_t1610719423  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.EventSystems.EventSystem InputManager::eventSystem
	EventSystem_t3466835263 * ___eventSystem_9;
	// UnityEngine.AudioClip InputManager::ClickSound
	AudioClip_t1932558630 * ___ClickSound_10;

public:
	inline static int32_t get_offset_of_eventSystem_9() { return static_cast<int32_t>(offsetof(InputManager_t1610719423, ___eventSystem_9)); }
	inline EventSystem_t3466835263 * get_eventSystem_9() const { return ___eventSystem_9; }
	inline EventSystem_t3466835263 ** get_address_of_eventSystem_9() { return &___eventSystem_9; }
	inline void set_eventSystem_9(EventSystem_t3466835263 * value)
	{
		___eventSystem_9 = value;
		Il2CppCodeGenWriteBarrier(&___eventSystem_9, value);
	}

	inline static int32_t get_offset_of_ClickSound_10() { return static_cast<int32_t>(offsetof(InputManager_t1610719423, ___ClickSound_10)); }
	inline AudioClip_t1932558630 * get_ClickSound_10() const { return ___ClickSound_10; }
	inline AudioClip_t1932558630 ** get_address_of_ClickSound_10() { return &___ClickSound_10; }
	inline void set_ClickSound_10(AudioClip_t1932558630 * value)
	{
		___ClickSound_10 = value;
		Il2CppCodeGenWriteBarrier(&___ClickSound_10, value);
	}
};

struct InputManager_t1610719423_StaticFields
{
public:
	// System.Action`1<UnityEngine.Vector2> InputManager::OnTouchDownEvent
	Action_1_t2045506961 * ___OnTouchDownEvent_2;
	// System.Action`1<UnityEngine.Vector2> InputManager::OnTouchUpEvent
	Action_1_t2045506961 * ___OnTouchUpEvent_3;
	// System.Action`1<UnityEngine.Vector2> InputManager::OnMouseDownEvent
	Action_1_t2045506961 * ___OnMouseDownEvent_4;
	// System.Action`1<UnityEngine.Vector2> InputManager::OnMouseUpEvent
	Action_1_t2045506961 * ___OnMouseUpEvent_5;
	// System.Action InputManager::OnBackButtonPressedEvent
	Action_t3226471752 * ___OnBackButtonPressedEvent_6;
	// InputManager InputManager::instance
	InputManager_t1610719423 * ___instance_7;
	// System.Boolean InputManager::isTouchAvailable
	bool ___isTouchAvailable_8;

public:
	inline static int32_t get_offset_of_OnTouchDownEvent_2() { return static_cast<int32_t>(offsetof(InputManager_t1610719423_StaticFields, ___OnTouchDownEvent_2)); }
	inline Action_1_t2045506961 * get_OnTouchDownEvent_2() const { return ___OnTouchDownEvent_2; }
	inline Action_1_t2045506961 ** get_address_of_OnTouchDownEvent_2() { return &___OnTouchDownEvent_2; }
	inline void set_OnTouchDownEvent_2(Action_1_t2045506961 * value)
	{
		___OnTouchDownEvent_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnTouchDownEvent_2, value);
	}

	inline static int32_t get_offset_of_OnTouchUpEvent_3() { return static_cast<int32_t>(offsetof(InputManager_t1610719423_StaticFields, ___OnTouchUpEvent_3)); }
	inline Action_1_t2045506961 * get_OnTouchUpEvent_3() const { return ___OnTouchUpEvent_3; }
	inline Action_1_t2045506961 ** get_address_of_OnTouchUpEvent_3() { return &___OnTouchUpEvent_3; }
	inline void set_OnTouchUpEvent_3(Action_1_t2045506961 * value)
	{
		___OnTouchUpEvent_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnTouchUpEvent_3, value);
	}

	inline static int32_t get_offset_of_OnMouseDownEvent_4() { return static_cast<int32_t>(offsetof(InputManager_t1610719423_StaticFields, ___OnMouseDownEvent_4)); }
	inline Action_1_t2045506961 * get_OnMouseDownEvent_4() const { return ___OnMouseDownEvent_4; }
	inline Action_1_t2045506961 ** get_address_of_OnMouseDownEvent_4() { return &___OnMouseDownEvent_4; }
	inline void set_OnMouseDownEvent_4(Action_1_t2045506961 * value)
	{
		___OnMouseDownEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___OnMouseDownEvent_4, value);
	}

	inline static int32_t get_offset_of_OnMouseUpEvent_5() { return static_cast<int32_t>(offsetof(InputManager_t1610719423_StaticFields, ___OnMouseUpEvent_5)); }
	inline Action_1_t2045506961 * get_OnMouseUpEvent_5() const { return ___OnMouseUpEvent_5; }
	inline Action_1_t2045506961 ** get_address_of_OnMouseUpEvent_5() { return &___OnMouseUpEvent_5; }
	inline void set_OnMouseUpEvent_5(Action_1_t2045506961 * value)
	{
		___OnMouseUpEvent_5 = value;
		Il2CppCodeGenWriteBarrier(&___OnMouseUpEvent_5, value);
	}

	inline static int32_t get_offset_of_OnBackButtonPressedEvent_6() { return static_cast<int32_t>(offsetof(InputManager_t1610719423_StaticFields, ___OnBackButtonPressedEvent_6)); }
	inline Action_t3226471752 * get_OnBackButtonPressedEvent_6() const { return ___OnBackButtonPressedEvent_6; }
	inline Action_t3226471752 ** get_address_of_OnBackButtonPressedEvent_6() { return &___OnBackButtonPressedEvent_6; }
	inline void set_OnBackButtonPressedEvent_6(Action_t3226471752 * value)
	{
		___OnBackButtonPressedEvent_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnBackButtonPressedEvent_6, value);
	}

	inline static int32_t get_offset_of_instance_7() { return static_cast<int32_t>(offsetof(InputManager_t1610719423_StaticFields, ___instance_7)); }
	inline InputManager_t1610719423 * get_instance_7() const { return ___instance_7; }
	inline InputManager_t1610719423 ** get_address_of_instance_7() { return &___instance_7; }
	inline void set_instance_7(InputManager_t1610719423 * value)
	{
		___instance_7 = value;
		Il2CppCodeGenWriteBarrier(&___instance_7, value);
	}

	inline static int32_t get_offset_of_isTouchAvailable_8() { return static_cast<int32_t>(offsetof(InputManager_t1610719423_StaticFields, ___isTouchAvailable_8)); }
	inline bool get_isTouchAvailable_8() const { return ___isTouchAvailable_8; }
	inline bool* get_address_of_isTouchAvailable_8() { return &___isTouchAvailable_8; }
	inline void set_isTouchAvailable_8(bool value)
	{
		___isTouchAvailable_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
