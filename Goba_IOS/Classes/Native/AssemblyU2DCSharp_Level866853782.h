﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// RingBorder[]
struct RingBorderU5BU5D_t1121309361;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level
struct  Level_t866853782  : public Il2CppObject
{
public:
	// RingBorder[] Level::<rings>k__BackingField
	RingBorderU5BU5D_t1121309361* ___U3CringsU3Ek__BackingField_0;
	// System.Int32 Level::ballCount
	int32_t ___ballCount_1;

public:
	inline static int32_t get_offset_of_U3CringsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Level_t866853782, ___U3CringsU3Ek__BackingField_0)); }
	inline RingBorderU5BU5D_t1121309361* get_U3CringsU3Ek__BackingField_0() const { return ___U3CringsU3Ek__BackingField_0; }
	inline RingBorderU5BU5D_t1121309361** get_address_of_U3CringsU3Ek__BackingField_0() { return &___U3CringsU3Ek__BackingField_0; }
	inline void set_U3CringsU3Ek__BackingField_0(RingBorderU5BU5D_t1121309361* value)
	{
		___U3CringsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CringsU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_ballCount_1() { return static_cast<int32_t>(offsetof(Level_t866853782, ___ballCount_1)); }
	inline int32_t get_ballCount_1() const { return ___ballCount_1; }
	inline int32_t* get_address_of_ballCount_1() { return &___ballCount_1; }
	inline void set_ballCount_1(int32_t value)
	{
		___ballCount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
