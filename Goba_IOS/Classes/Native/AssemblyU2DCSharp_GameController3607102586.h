﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_GameController_GameStatus3461812585.h"

// GameController
struct GameController_t3607102586;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// Levels
struct Levels_t748035019;
// Player
struct Player_t1147783557;
// GameColors
struct GameColors_t1153883414;
// GameColor
struct GameColor_t1500385825;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController
struct  GameController_t3607102586  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera GameController::UICamera
	Camera_t189460977 * ___UICamera_3;
	// UnityEngine.Canvas GameController::UICanvas
	Canvas_t209405766 * ___UICanvas_4;
	// UnityEngine.EventSystems.EventSystem GameController::eventSystem
	EventSystem_t3466835263 * ___eventSystem_5;
	// Levels GameController::Levels
	Levels_t748035019 * ___Levels_6;
	// Player GameController::Player
	Player_t1147783557 * ___Player_7;
	// GameColors GameController::GameColors
	GameColors_t1153883414 * ___GameColors_8;
	// GameColor GameController::GameColor
	GameColor_t1500385825 * ___GameColor_9;
	// System.Int32 GameController::onTheFly
	int32_t ___onTheFly_10;
	// GameController/GameStatus GameController::gameStatus
	int32_t ___gameStatus_11;
	// System.Boolean GameController::isGamePaused
	bool ___isGamePaused_12;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GameController::GameScreens
	List_1_t1125654279 * ___GameScreens_13;
	// UnityEngine.GameObject GameController::LastScreen
	GameObject_t1756533147 * ___LastScreen_14;

public:
	inline static int32_t get_offset_of_UICamera_3() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___UICamera_3)); }
	inline Camera_t189460977 * get_UICamera_3() const { return ___UICamera_3; }
	inline Camera_t189460977 ** get_address_of_UICamera_3() { return &___UICamera_3; }
	inline void set_UICamera_3(Camera_t189460977 * value)
	{
		___UICamera_3 = value;
		Il2CppCodeGenWriteBarrier(&___UICamera_3, value);
	}

	inline static int32_t get_offset_of_UICanvas_4() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___UICanvas_4)); }
	inline Canvas_t209405766 * get_UICanvas_4() const { return ___UICanvas_4; }
	inline Canvas_t209405766 ** get_address_of_UICanvas_4() { return &___UICanvas_4; }
	inline void set_UICanvas_4(Canvas_t209405766 * value)
	{
		___UICanvas_4 = value;
		Il2CppCodeGenWriteBarrier(&___UICanvas_4, value);
	}

	inline static int32_t get_offset_of_eventSystem_5() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___eventSystem_5)); }
	inline EventSystem_t3466835263 * get_eventSystem_5() const { return ___eventSystem_5; }
	inline EventSystem_t3466835263 ** get_address_of_eventSystem_5() { return &___eventSystem_5; }
	inline void set_eventSystem_5(EventSystem_t3466835263 * value)
	{
		___eventSystem_5 = value;
		Il2CppCodeGenWriteBarrier(&___eventSystem_5, value);
	}

	inline static int32_t get_offset_of_Levels_6() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___Levels_6)); }
	inline Levels_t748035019 * get_Levels_6() const { return ___Levels_6; }
	inline Levels_t748035019 ** get_address_of_Levels_6() { return &___Levels_6; }
	inline void set_Levels_6(Levels_t748035019 * value)
	{
		___Levels_6 = value;
		Il2CppCodeGenWriteBarrier(&___Levels_6, value);
	}

	inline static int32_t get_offset_of_Player_7() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___Player_7)); }
	inline Player_t1147783557 * get_Player_7() const { return ___Player_7; }
	inline Player_t1147783557 ** get_address_of_Player_7() { return &___Player_7; }
	inline void set_Player_7(Player_t1147783557 * value)
	{
		___Player_7 = value;
		Il2CppCodeGenWriteBarrier(&___Player_7, value);
	}

	inline static int32_t get_offset_of_GameColors_8() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___GameColors_8)); }
	inline GameColors_t1153883414 * get_GameColors_8() const { return ___GameColors_8; }
	inline GameColors_t1153883414 ** get_address_of_GameColors_8() { return &___GameColors_8; }
	inline void set_GameColors_8(GameColors_t1153883414 * value)
	{
		___GameColors_8 = value;
		Il2CppCodeGenWriteBarrier(&___GameColors_8, value);
	}

	inline static int32_t get_offset_of_GameColor_9() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___GameColor_9)); }
	inline GameColor_t1500385825 * get_GameColor_9() const { return ___GameColor_9; }
	inline GameColor_t1500385825 ** get_address_of_GameColor_9() { return &___GameColor_9; }
	inline void set_GameColor_9(GameColor_t1500385825 * value)
	{
		___GameColor_9 = value;
		Il2CppCodeGenWriteBarrier(&___GameColor_9, value);
	}

	inline static int32_t get_offset_of_onTheFly_10() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___onTheFly_10)); }
	inline int32_t get_onTheFly_10() const { return ___onTheFly_10; }
	inline int32_t* get_address_of_onTheFly_10() { return &___onTheFly_10; }
	inline void set_onTheFly_10(int32_t value)
	{
		___onTheFly_10 = value;
	}

	inline static int32_t get_offset_of_gameStatus_11() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___gameStatus_11)); }
	inline int32_t get_gameStatus_11() const { return ___gameStatus_11; }
	inline int32_t* get_address_of_gameStatus_11() { return &___gameStatus_11; }
	inline void set_gameStatus_11(int32_t value)
	{
		___gameStatus_11 = value;
	}

	inline static int32_t get_offset_of_isGamePaused_12() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___isGamePaused_12)); }
	inline bool get_isGamePaused_12() const { return ___isGamePaused_12; }
	inline bool* get_address_of_isGamePaused_12() { return &___isGamePaused_12; }
	inline void set_isGamePaused_12(bool value)
	{
		___isGamePaused_12 = value;
	}

	inline static int32_t get_offset_of_GameScreens_13() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___GameScreens_13)); }
	inline List_1_t1125654279 * get_GameScreens_13() const { return ___GameScreens_13; }
	inline List_1_t1125654279 ** get_address_of_GameScreens_13() { return &___GameScreens_13; }
	inline void set_GameScreens_13(List_1_t1125654279 * value)
	{
		___GameScreens_13 = value;
		Il2CppCodeGenWriteBarrier(&___GameScreens_13, value);
	}

	inline static int32_t get_offset_of_LastScreen_14() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___LastScreen_14)); }
	inline GameObject_t1756533147 * get_LastScreen_14() const { return ___LastScreen_14; }
	inline GameObject_t1756533147 ** get_address_of_LastScreen_14() { return &___LastScreen_14; }
	inline void set_LastScreen_14(GameObject_t1756533147 * value)
	{
		___LastScreen_14 = value;
		Il2CppCodeGenWriteBarrier(&___LastScreen_14, value);
	}
};

struct GameController_t3607102586_StaticFields
{
public:
	// GameController GameController::instance
	GameController_t3607102586 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(GameController_t3607102586_StaticFields, ___instance_2)); }
	inline GameController_t3607102586 * get_instance_2() const { return ___instance_2; }
	inline GameController_t3607102586 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(GameController_t3607102586 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
