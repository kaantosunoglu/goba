﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// AudioManager
struct AudioManager_t4222704959;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager
struct  AudioManager_t4222704959  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean AudioManager::isSoundEnabled
	bool ___isSoundEnabled_4;
	// System.Boolean AudioManager::isMusicEnabled
	bool ___isMusicEnabled_5;

public:
	inline static int32_t get_offset_of_isSoundEnabled_4() { return static_cast<int32_t>(offsetof(AudioManager_t4222704959, ___isSoundEnabled_4)); }
	inline bool get_isSoundEnabled_4() const { return ___isSoundEnabled_4; }
	inline bool* get_address_of_isSoundEnabled_4() { return &___isSoundEnabled_4; }
	inline void set_isSoundEnabled_4(bool value)
	{
		___isSoundEnabled_4 = value;
	}

	inline static int32_t get_offset_of_isMusicEnabled_5() { return static_cast<int32_t>(offsetof(AudioManager_t4222704959, ___isMusicEnabled_5)); }
	inline bool get_isMusicEnabled_5() const { return ___isMusicEnabled_5; }
	inline bool* get_address_of_isMusicEnabled_5() { return &___isMusicEnabled_5; }
	inline void set_isMusicEnabled_5(bool value)
	{
		___isMusicEnabled_5 = value;
	}
};

struct AudioManager_t4222704959_StaticFields
{
public:
	// System.Action`1<System.Boolean> AudioManager::OnSoundStatusChangedEvent
	Action_1_t3627374100 * ___OnSoundStatusChangedEvent_2;
	// System.Action`1<System.Boolean> AudioManager::OnMusicStatusChangedEvent
	Action_1_t3627374100 * ___OnMusicStatusChangedEvent_3;
	// AudioManager AudioManager::_instance
	AudioManager_t4222704959 * ____instance_6;

public:
	inline static int32_t get_offset_of_OnSoundStatusChangedEvent_2() { return static_cast<int32_t>(offsetof(AudioManager_t4222704959_StaticFields, ___OnSoundStatusChangedEvent_2)); }
	inline Action_1_t3627374100 * get_OnSoundStatusChangedEvent_2() const { return ___OnSoundStatusChangedEvent_2; }
	inline Action_1_t3627374100 ** get_address_of_OnSoundStatusChangedEvent_2() { return &___OnSoundStatusChangedEvent_2; }
	inline void set_OnSoundStatusChangedEvent_2(Action_1_t3627374100 * value)
	{
		___OnSoundStatusChangedEvent_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnSoundStatusChangedEvent_2, value);
	}

	inline static int32_t get_offset_of_OnMusicStatusChangedEvent_3() { return static_cast<int32_t>(offsetof(AudioManager_t4222704959_StaticFields, ___OnMusicStatusChangedEvent_3)); }
	inline Action_1_t3627374100 * get_OnMusicStatusChangedEvent_3() const { return ___OnMusicStatusChangedEvent_3; }
	inline Action_1_t3627374100 ** get_address_of_OnMusicStatusChangedEvent_3() { return &___OnMusicStatusChangedEvent_3; }
	inline void set_OnMusicStatusChangedEvent_3(Action_1_t3627374100 * value)
	{
		___OnMusicStatusChangedEvent_3 = value;
		Il2CppCodeGenWriteBarrier(&___OnMusicStatusChangedEvent_3, value);
	}

	inline static int32_t get_offset_of__instance_6() { return static_cast<int32_t>(offsetof(AudioManager_t4222704959_StaticFields, ____instance_6)); }
	inline AudioManager_t4222704959 * get__instance_6() const { return ____instance_6; }
	inline AudioManager_t4222704959 ** get_address_of__instance_6() { return &____instance_6; }
	inline void set__instance_6(AudioManager_t4222704959 * value)
	{
		____instance_6 = value;
		Il2CppCodeGenWriteBarrier(&____instance_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
