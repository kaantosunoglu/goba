﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVertical1968298610.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup_U3CDelay3228926346.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder2155218138.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility4076838048.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup2468316403.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "Newtonsoft_Json_U3CModuleU3E3783534214.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ConstructorHandlin4150360451.h"
#include "Newtonsoft_Json_Newtonsoft_Json_DateFormatHandling147563782.h"
#include "Newtonsoft_Json_Newtonsoft_Json_DateParseHandling822016916.h"
#include "Newtonsoft_Json_Newtonsoft_Json_DateTimeZoneHandlin478160270.h"
#include "Newtonsoft_Json_Newtonsoft_Json_DefaultValueHandli3457895463.h"
#include "Newtonsoft_Json_Newtonsoft_Json_FloatFormatHandling898035958.h"
#include "Newtonsoft_Json_Newtonsoft_Json_FloatParseHandling1928971464.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Formatting4009318759.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonArrayAttribute3639750789.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonConstructorAtt4031199386.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonContainerAttribu47210975.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonConvert3949895659.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonConverter1964060750.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonConverterAttri3918837738.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonConverterColle3315164788.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonDictionaryAttr1423825346.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonException2657548905.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonExtensionDataA2083108381.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonIgnoreAttribute898097550.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonObjectAttribute162755825.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonContainerType1137362403.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonPosition687338249.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonPropertyAttrib2023370155.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonReader3154730733.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonReader_State2444238258.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonReaderExceptio2043888884.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonRequiredAttrib2513079179.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonSerializationE1492322735.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonSerializer1719617802.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonSerializerSetti842388167.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ReadType2202027848.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonTextReader726416198.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonTextWriter2524035668.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonToken620654565.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonWriter1973729997.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonWriter_State3285832914.h"
#include "Newtonsoft_Json_Newtonsoft_Json_JsonWriterExceptio1246029574.h"
#include "Newtonsoft_Json_Newtonsoft_Json_MemberSerialization687984360.h"
#include "Newtonsoft_Json_Newtonsoft_Json_MetadataPropertyHan693038949.h"
#include "Newtonsoft_Json_Newtonsoft_Json_MissingMemberHandli367517353.h"
#include "Newtonsoft_Json_Newtonsoft_Json_NullValueHandling3618095365.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ObjectCreationHand3720134651.h"
#include "Newtonsoft_Json_Newtonsoft_Json_PreserveReferences3019117943.h"
#include "Newtonsoft_Json_Newtonsoft_Json_ReferenceLoopHandl1017855894.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Required2961887721.h"
#include "Newtonsoft_Json_Newtonsoft_Json_StringEscapeHandli3827428951.h"
#include "Newtonsoft_Json_Newtonsoft_Json_TypeNameHandling1331513094.h"
#include "Newtonsoft_Json_Newtonsoft_Json_WriteState1009238728.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Base64En2682770749.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Collecti3679343665.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_Primitiv2593001762.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_TypeInfo1531269270.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ParseRes3601995942.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertU2984810590.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertU1788482786.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertU2285525878.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ConvertU2278087657.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_ParserTi3773981114.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_DateTime1235213504.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Utilities_DateTimeU919483584.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (HorizontalOrVerticalLayoutGroup_t1968298610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1900[5] = 
{
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandHeight_12(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlWidth_13(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1906[7] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1907[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1908[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1909[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1910[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1913[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1914[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1915[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1916[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1921[11] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Uv2S_4(),
	VertexHelper_t385374196::get_offset_of_m_Uv3S_5(),
	VertexHelper_t385374196::get_offset_of_m_Normals_6(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_7(),
	VertexHelper_t385374196::get_offset_of_m_Indices_8(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1923[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1928[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1929[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (ConstructorHandling_t4150360451)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1932[3] = 
{
	ConstructorHandling_t4150360451::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (DateFormatHandling_t147563782)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1933[3] = 
{
	DateFormatHandling_t147563782::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (DateParseHandling_t822016916)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1934[4] = 
{
	DateParseHandling_t822016916::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (DateTimeZoneHandling_t478160270)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1935[5] = 
{
	DateTimeZoneHandling_t478160270::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (DefaultValueHandling_t3457895463)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1936[5] = 
{
	DefaultValueHandling_t3457895463::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (FloatFormatHandling_t898035958)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1937[4] = 
{
	FloatFormatHandling_t898035958::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (FloatParseHandling_t1928971464)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1938[3] = 
{
	FloatParseHandling_t1928971464::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (Formatting_t4009318759)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1939[3] = 
{
	Formatting_t4009318759::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (JsonArrayAttribute_t3639750789), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (JsonConstructorAttribute_t4031199386), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (JsonContainerAttribute_t47210975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1944[9] = 
{
	JsonContainerAttribute_t47210975::get_offset_of_U3CItemConverterTypeU3Ek__BackingField_0(),
	JsonContainerAttribute_t47210975::get_offset_of_U3CItemConverterParametersU3Ek__BackingField_1(),
	JsonContainerAttribute_t47210975::get_offset_of_U3CNamingStrategyInstanceU3Ek__BackingField_2(),
	JsonContainerAttribute_t47210975::get_offset_of__isReference_3(),
	JsonContainerAttribute_t47210975::get_offset_of__itemIsReference_4(),
	JsonContainerAttribute_t47210975::get_offset_of__itemReferenceLoopHandling_5(),
	JsonContainerAttribute_t47210975::get_offset_of__itemTypeNameHandling_6(),
	JsonContainerAttribute_t47210975::get_offset_of__namingStrategyType_7(),
	JsonContainerAttribute_t47210975::get_offset_of__namingStrategyParameters_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (JsonConvert_t3949895659), -1, sizeof(JsonConvert_t3949895659_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1945[8] = 
{
	JsonConvert_t3949895659_StaticFields::get_offset_of_U3CDefaultSettingsU3Ek__BackingField_0(),
	JsonConvert_t3949895659_StaticFields::get_offset_of_True_1(),
	JsonConvert_t3949895659_StaticFields::get_offset_of_False_2(),
	JsonConvert_t3949895659_StaticFields::get_offset_of_Null_3(),
	JsonConvert_t3949895659_StaticFields::get_offset_of_Undefined_4(),
	JsonConvert_t3949895659_StaticFields::get_offset_of_PositiveInfinity_5(),
	JsonConvert_t3949895659_StaticFields::get_offset_of_NegativeInfinity_6(),
	JsonConvert_t3949895659_StaticFields::get_offset_of_NaN_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (JsonConverter_t1964060750), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (JsonConverterAttribute_t3918837738), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1947[2] = 
{
	JsonConverterAttribute_t3918837738::get_offset_of__converterType_0(),
	JsonConverterAttribute_t3918837738::get_offset_of_U3CConverterParametersU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (JsonConverterCollection_t3315164788), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (JsonDictionaryAttribute_t1423825346), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (JsonException_t2657548905), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (JsonExtensionDataAttribute_t2083108381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1951[2] = 
{
	JsonExtensionDataAttribute_t2083108381::get_offset_of_U3CWriteDataU3Ek__BackingField_0(),
	JsonExtensionDataAttribute_t2083108381::get_offset_of_U3CReadDataU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (JsonIgnoreAttribute_t898097550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (JsonObjectAttribute_t162755825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1953[2] = 
{
	JsonObjectAttribute_t162755825::get_offset_of__memberSerialization_9(),
	JsonObjectAttribute_t162755825::get_offset_of__itemRequired_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (JsonContainerType_t1137362403)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1954[5] = 
{
	JsonContainerType_t1137362403::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (JsonPosition_t687338249)+ sizeof (Il2CppObject), sizeof(JsonPosition_t687338249_marshaled_pinvoke), sizeof(JsonPosition_t687338249_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1955[5] = 
{
	JsonPosition_t687338249_StaticFields::get_offset_of_SpecialCharacters_0(),
	JsonPosition_t687338249::get_offset_of_Type_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JsonPosition_t687338249::get_offset_of_Position_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JsonPosition_t687338249::get_offset_of_PropertyName_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	JsonPosition_t687338249::get_offset_of_HasIndex_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (JsonPropertyAttribute_t2023370155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1956[16] = 
{
	JsonPropertyAttribute_t2023370155::get_offset_of__nullValueHandling_0(),
	JsonPropertyAttribute_t2023370155::get_offset_of__defaultValueHandling_1(),
	JsonPropertyAttribute_t2023370155::get_offset_of__referenceLoopHandling_2(),
	JsonPropertyAttribute_t2023370155::get_offset_of__objectCreationHandling_3(),
	JsonPropertyAttribute_t2023370155::get_offset_of__typeNameHandling_4(),
	JsonPropertyAttribute_t2023370155::get_offset_of__isReference_5(),
	JsonPropertyAttribute_t2023370155::get_offset_of__order_6(),
	JsonPropertyAttribute_t2023370155::get_offset_of__required_7(),
	JsonPropertyAttribute_t2023370155::get_offset_of__itemIsReference_8(),
	JsonPropertyAttribute_t2023370155::get_offset_of__itemReferenceLoopHandling_9(),
	JsonPropertyAttribute_t2023370155::get_offset_of__itemTypeNameHandling_10(),
	JsonPropertyAttribute_t2023370155::get_offset_of_U3CItemConverterTypeU3Ek__BackingField_11(),
	JsonPropertyAttribute_t2023370155::get_offset_of_U3CItemConverterParametersU3Ek__BackingField_12(),
	JsonPropertyAttribute_t2023370155::get_offset_of_U3CNamingStrategyTypeU3Ek__BackingField_13(),
	JsonPropertyAttribute_t2023370155::get_offset_of_U3CNamingStrategyParametersU3Ek__BackingField_14(),
	JsonPropertyAttribute_t2023370155::get_offset_of_U3CPropertyNameU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (JsonReader_t3154730733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1957[15] = 
{
	JsonReader_t3154730733::get_offset_of__tokenType_0(),
	JsonReader_t3154730733::get_offset_of__value_1(),
	JsonReader_t3154730733::get_offset_of__quoteChar_2(),
	JsonReader_t3154730733::get_offset_of__currentState_3(),
	JsonReader_t3154730733::get_offset_of__currentPosition_4(),
	JsonReader_t3154730733::get_offset_of__culture_5(),
	JsonReader_t3154730733::get_offset_of__dateTimeZoneHandling_6(),
	JsonReader_t3154730733::get_offset_of__maxDepth_7(),
	JsonReader_t3154730733::get_offset_of__hasExceededMaxDepth_8(),
	JsonReader_t3154730733::get_offset_of__dateParseHandling_9(),
	JsonReader_t3154730733::get_offset_of__floatParseHandling_10(),
	JsonReader_t3154730733::get_offset_of__dateFormatString_11(),
	JsonReader_t3154730733::get_offset_of__stack_12(),
	JsonReader_t3154730733::get_offset_of_U3CCloseInputU3Ek__BackingField_13(),
	JsonReader_t3154730733::get_offset_of_U3CSupportMultipleContentU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (State_t2444238258)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1958[14] = 
{
	State_t2444238258::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (JsonReaderException_t2043888884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1959[3] = 
{
	JsonReaderException_t2043888884::get_offset_of_U3CLineNumberU3Ek__BackingField_11(),
	JsonReaderException_t2043888884::get_offset_of_U3CLinePositionU3Ek__BackingField_12(),
	JsonReaderException_t2043888884::get_offset_of_U3CPathU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (JsonRequiredAttribute_t2513079179), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (JsonSerializationException_t1492322735), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (JsonSerializer_t1719617802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1962[31] = 
{
	JsonSerializer_t1719617802::get_offset_of__typeNameHandling_0(),
	JsonSerializer_t1719617802::get_offset_of__typeNameAssemblyFormat_1(),
	JsonSerializer_t1719617802::get_offset_of__preserveReferencesHandling_2(),
	JsonSerializer_t1719617802::get_offset_of__referenceLoopHandling_3(),
	JsonSerializer_t1719617802::get_offset_of__missingMemberHandling_4(),
	JsonSerializer_t1719617802::get_offset_of__objectCreationHandling_5(),
	JsonSerializer_t1719617802::get_offset_of__nullValueHandling_6(),
	JsonSerializer_t1719617802::get_offset_of__defaultValueHandling_7(),
	JsonSerializer_t1719617802::get_offset_of__constructorHandling_8(),
	JsonSerializer_t1719617802::get_offset_of__metadataPropertyHandling_9(),
	JsonSerializer_t1719617802::get_offset_of__converters_10(),
	JsonSerializer_t1719617802::get_offset_of__contractResolver_11(),
	JsonSerializer_t1719617802::get_offset_of__traceWriter_12(),
	JsonSerializer_t1719617802::get_offset_of__equalityComparer_13(),
	JsonSerializer_t1719617802::get_offset_of__binder_14(),
	JsonSerializer_t1719617802::get_offset_of__context_15(),
	JsonSerializer_t1719617802::get_offset_of__referenceResolver_16(),
	JsonSerializer_t1719617802::get_offset_of__formatting_17(),
	JsonSerializer_t1719617802::get_offset_of__dateFormatHandling_18(),
	JsonSerializer_t1719617802::get_offset_of__dateTimeZoneHandling_19(),
	JsonSerializer_t1719617802::get_offset_of__dateParseHandling_20(),
	JsonSerializer_t1719617802::get_offset_of__floatFormatHandling_21(),
	JsonSerializer_t1719617802::get_offset_of__floatParseHandling_22(),
	JsonSerializer_t1719617802::get_offset_of__stringEscapeHandling_23(),
	JsonSerializer_t1719617802::get_offset_of__culture_24(),
	JsonSerializer_t1719617802::get_offset_of__maxDepth_25(),
	JsonSerializer_t1719617802::get_offset_of__maxDepthSet_26(),
	JsonSerializer_t1719617802::get_offset_of__checkAdditionalContent_27(),
	JsonSerializer_t1719617802::get_offset_of__dateFormatString_28(),
	JsonSerializer_t1719617802::get_offset_of__dateFormatStringSet_29(),
	JsonSerializer_t1719617802::get_offset_of_Error_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (JsonSerializerSettings_t842388167), -1, sizeof(JsonSerializerSettings_t842388167_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1963[33] = 
{
	JsonSerializerSettings_t842388167_StaticFields::get_offset_of_DefaultContext_0(),
	JsonSerializerSettings_t842388167_StaticFields::get_offset_of_DefaultCulture_1(),
	JsonSerializerSettings_t842388167::get_offset_of__formatting_2(),
	JsonSerializerSettings_t842388167::get_offset_of__dateFormatHandling_3(),
	JsonSerializerSettings_t842388167::get_offset_of__dateTimeZoneHandling_4(),
	JsonSerializerSettings_t842388167::get_offset_of__dateParseHandling_5(),
	JsonSerializerSettings_t842388167::get_offset_of__floatFormatHandling_6(),
	JsonSerializerSettings_t842388167::get_offset_of__floatParseHandling_7(),
	JsonSerializerSettings_t842388167::get_offset_of__stringEscapeHandling_8(),
	JsonSerializerSettings_t842388167::get_offset_of__culture_9(),
	JsonSerializerSettings_t842388167::get_offset_of__checkAdditionalContent_10(),
	JsonSerializerSettings_t842388167::get_offset_of__maxDepth_11(),
	JsonSerializerSettings_t842388167::get_offset_of__maxDepthSet_12(),
	JsonSerializerSettings_t842388167::get_offset_of__dateFormatString_13(),
	JsonSerializerSettings_t842388167::get_offset_of__dateFormatStringSet_14(),
	JsonSerializerSettings_t842388167::get_offset_of__typeNameAssemblyFormat_15(),
	JsonSerializerSettings_t842388167::get_offset_of__defaultValueHandling_16(),
	JsonSerializerSettings_t842388167::get_offset_of__preserveReferencesHandling_17(),
	JsonSerializerSettings_t842388167::get_offset_of__nullValueHandling_18(),
	JsonSerializerSettings_t842388167::get_offset_of__objectCreationHandling_19(),
	JsonSerializerSettings_t842388167::get_offset_of__missingMemberHandling_20(),
	JsonSerializerSettings_t842388167::get_offset_of__referenceLoopHandling_21(),
	JsonSerializerSettings_t842388167::get_offset_of__context_22(),
	JsonSerializerSettings_t842388167::get_offset_of__constructorHandling_23(),
	JsonSerializerSettings_t842388167::get_offset_of__typeNameHandling_24(),
	JsonSerializerSettings_t842388167::get_offset_of__metadataPropertyHandling_25(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CConvertersU3Ek__BackingField_26(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CContractResolverU3Ek__BackingField_27(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CEqualityComparerU3Ek__BackingField_28(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CReferenceResolverProviderU3Ek__BackingField_29(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CTraceWriterU3Ek__BackingField_30(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CBinderU3Ek__BackingField_31(),
	JsonSerializerSettings_t842388167::get_offset_of_U3CErrorU3Ek__BackingField_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (ReadType_t2202027848)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1964[10] = 
{
	ReadType_t2202027848::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (JsonTextReader_t726416198), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1965[11] = 
{
	JsonTextReader_t726416198::get_offset_of__reader_15(),
	JsonTextReader_t726416198::get_offset_of__chars_16(),
	JsonTextReader_t726416198::get_offset_of__charsUsed_17(),
	JsonTextReader_t726416198::get_offset_of__charPos_18(),
	JsonTextReader_t726416198::get_offset_of__lineStartPos_19(),
	JsonTextReader_t726416198::get_offset_of__lineNumber_20(),
	JsonTextReader_t726416198::get_offset_of__isEndOfFile_21(),
	JsonTextReader_t726416198::get_offset_of__stringBuffer_22(),
	JsonTextReader_t726416198::get_offset_of__stringReference_23(),
	JsonTextReader_t726416198::get_offset_of__arrayPool_24(),
	JsonTextReader_t726416198::get_offset_of_NameTable_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (JsonTextWriter_t2524035668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1966[10] = 
{
	JsonTextWriter_t2524035668::get_offset_of__writer_13(),
	JsonTextWriter_t2524035668::get_offset_of__base64Encoder_14(),
	JsonTextWriter_t2524035668::get_offset_of__indentChar_15(),
	JsonTextWriter_t2524035668::get_offset_of__indentation_16(),
	JsonTextWriter_t2524035668::get_offset_of__quoteChar_17(),
	JsonTextWriter_t2524035668::get_offset_of__quoteName_18(),
	JsonTextWriter_t2524035668::get_offset_of__charEscapeFlags_19(),
	JsonTextWriter_t2524035668::get_offset_of__writeBuffer_20(),
	JsonTextWriter_t2524035668::get_offset_of__arrayPool_21(),
	JsonTextWriter_t2524035668::get_offset_of__indentChars_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (JsonToken_t620654565)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1967[19] = 
{
	JsonToken_t620654565::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (JsonWriter_t1973729997), -1, sizeof(JsonWriter_t1973729997_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1968[13] = 
{
	JsonWriter_t1973729997_StaticFields::get_offset_of_StateArray_0(),
	JsonWriter_t1973729997_StaticFields::get_offset_of_StateArrayTempate_1(),
	JsonWriter_t1973729997::get_offset_of__stack_2(),
	JsonWriter_t1973729997::get_offset_of__currentPosition_3(),
	JsonWriter_t1973729997::get_offset_of__currentState_4(),
	JsonWriter_t1973729997::get_offset_of__formatting_5(),
	JsonWriter_t1973729997::get_offset_of_U3CCloseOutputU3Ek__BackingField_6(),
	JsonWriter_t1973729997::get_offset_of__dateFormatHandling_7(),
	JsonWriter_t1973729997::get_offset_of__dateTimeZoneHandling_8(),
	JsonWriter_t1973729997::get_offset_of__stringEscapeHandling_9(),
	JsonWriter_t1973729997::get_offset_of__floatFormatHandling_10(),
	JsonWriter_t1973729997::get_offset_of__dateFormatString_11(),
	JsonWriter_t1973729997::get_offset_of__culture_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (State_t3285832914)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1969[11] = 
{
	State_t3285832914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (JsonWriterException_t1246029574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1970[1] = 
{
	JsonWriterException_t1246029574::get_offset_of_U3CPathU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (MemberSerialization_t687984360)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1971[4] = 
{
	MemberSerialization_t687984360::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (MetadataPropertyHandling_t693038949)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1972[4] = 
{
	MetadataPropertyHandling_t693038949::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (MissingMemberHandling_t367517353)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1973[3] = 
{
	MissingMemberHandling_t367517353::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (NullValueHandling_t3618095365)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1974[3] = 
{
	NullValueHandling_t3618095365::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (ObjectCreationHandling_t3720134651)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1975[4] = 
{
	ObjectCreationHandling_t3720134651::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (PreserveReferencesHandling_t3019117943)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1976[5] = 
{
	PreserveReferencesHandling_t3019117943::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (ReferenceLoopHandling_t1017855894)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1977[4] = 
{
	ReferenceLoopHandling_t1017855894::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (Required_t2961887721)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1978[5] = 
{
	Required_t2961887721::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (StringEscapeHandling_t3827428951)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1979[4] = 
{
	StringEscapeHandling_t3827428951::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (TypeNameHandling_t1331513094)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1980[6] = 
{
	TypeNameHandling_t1331513094::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (WriteState_t1009238728)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1981[8] = 
{
	WriteState_t1009238728::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (Base64Encoder_t2682770749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1982[4] = 
{
	Base64Encoder_t2682770749::get_offset_of__charsLine_0(),
	Base64Encoder_t2682770749::get_offset_of__writer_1(),
	Base64Encoder_t2682770749::get_offset_of__leftOverBytes_2(),
	Base64Encoder_t2682770749::get_offset_of__leftOverBytesCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1983[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (CollectionUtils_t3679343665), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1986[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (PrimitiveTypeCode_t2593001762)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1987[43] = 
{
	PrimitiveTypeCode_t2593001762::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (TypeInformation_t1531269270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1988[2] = 
{
	TypeInformation_t1531269270::get_offset_of_U3CTypeU3Ek__BackingField_0(),
	TypeInformation_t1531269270::get_offset_of_U3CTypeCodeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (ParseResult_t3601995942)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1989[5] = 
{
	ParseResult_t3601995942::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (ConvertUtils_t2984810590), -1, sizeof(ConvertUtils_t2984810590_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1990[3] = 
{
	ConvertUtils_t2984810590_StaticFields::get_offset_of_TypeCodeMap_0(),
	ConvertUtils_t2984810590_StaticFields::get_offset_of_PrimitiveTypeCodes_1(),
	ConvertUtils_t2984810590_StaticFields::get_offset_of_CastConverters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (TypeConvertKey_t1788482786)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1991[2] = 
{
	TypeConvertKey_t1788482786::get_offset_of__initialType_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TypeConvertKey_t1788482786::get_offset_of__targetType_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (ConvertResult_t2285525878)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1992[5] = 
{
	ConvertResult_t2285525878::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (U3CU3Ec__DisplayClass9_0_t2278087657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1993[1] = 
{
	U3CU3Ec__DisplayClass9_0_t2278087657::get_offset_of_call_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (ParserTimeZone_t3773981114)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1994[5] = 
{
	ParserTimeZone_t3773981114::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (DateTimeParser_t1235213504)+ sizeof (Il2CppObject), sizeof(DateTimeParser_t1235213504_marshaled_pinvoke), sizeof(DateTimeParser_t1235213504_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1995[26] = 
{
	DateTimeParser_t1235213504::get_offset_of_Year_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1235213504::get_offset_of_Month_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1235213504::get_offset_of_Day_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1235213504::get_offset_of_Hour_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1235213504::get_offset_of_Minute_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1235213504::get_offset_of_Second_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1235213504::get_offset_of_Fraction_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1235213504::get_offset_of_ZoneHour_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1235213504::get_offset_of_ZoneMinute_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1235213504::get_offset_of_Zone_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1235213504::get_offset_of__text_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1235213504::get_offset_of__end_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DateTimeParser_t1235213504_StaticFields::get_offset_of_Power10_12(),
	DateTimeParser_t1235213504_StaticFields::get_offset_of_Lzyyyy_13(),
	DateTimeParser_t1235213504_StaticFields::get_offset_of_Lzyyyy__14(),
	DateTimeParser_t1235213504_StaticFields::get_offset_of_Lzyyyy_MM_15(),
	DateTimeParser_t1235213504_StaticFields::get_offset_of_Lzyyyy_MM__16(),
	DateTimeParser_t1235213504_StaticFields::get_offset_of_Lzyyyy_MM_dd_17(),
	DateTimeParser_t1235213504_StaticFields::get_offset_of_Lzyyyy_MM_ddT_18(),
	DateTimeParser_t1235213504_StaticFields::get_offset_of_LzHH_19(),
	DateTimeParser_t1235213504_StaticFields::get_offset_of_LzHH__20(),
	DateTimeParser_t1235213504_StaticFields::get_offset_of_LzHH_mm_21(),
	DateTimeParser_t1235213504_StaticFields::get_offset_of_LzHH_mm__22(),
	DateTimeParser_t1235213504_StaticFields::get_offset_of_LzHH_mm_ss_23(),
	DateTimeParser_t1235213504_StaticFields::get_offset_of_Lz__24(),
	DateTimeParser_t1235213504_StaticFields::get_offset_of_Lz_zz_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (DateTimeUtils_t919483584), -1, sizeof(DateTimeUtils_t919483584_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1996[3] = 
{
	DateTimeUtils_t919483584_StaticFields::get_offset_of_InitialJavaScriptDateTicks_0(),
	DateTimeUtils_t919483584_StaticFields::get_offset_of_DaysToMonth365_1(),
	DateTimeUtils_t919483584_StaticFields::get_offset_of_DaysToMonth366_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1998[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1999[1] = 
{
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
