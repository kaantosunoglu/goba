﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Button
struct Button_t2872111280;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PassedPanel
struct  PassedPanel_t1159949582  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button PassedPanel::appstoreButton
	Button_t2872111280 * ___appstoreButton_2;
	// UnityEngine.UI.Button PassedPanel::googleplayButton
	Button_t2872111280 * ___googleplayButton_3;

public:
	inline static int32_t get_offset_of_appstoreButton_2() { return static_cast<int32_t>(offsetof(PassedPanel_t1159949582, ___appstoreButton_2)); }
	inline Button_t2872111280 * get_appstoreButton_2() const { return ___appstoreButton_2; }
	inline Button_t2872111280 ** get_address_of_appstoreButton_2() { return &___appstoreButton_2; }
	inline void set_appstoreButton_2(Button_t2872111280 * value)
	{
		___appstoreButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___appstoreButton_2, value);
	}

	inline static int32_t get_offset_of_googleplayButton_3() { return static_cast<int32_t>(offsetof(PassedPanel_t1159949582, ___googleplayButton_3)); }
	inline Button_t2872111280 * get_googleplayButton_3() const { return ___googleplayButton_3; }
	inline Button_t2872111280 ** get_address_of_googleplayButton_3() { return &___googleplayButton_3; }
	inline void set_googleplayButton_3(Button_t2872111280 * value)
	{
		___googleplayButton_3 = value;
		Il2CppCodeGenWriteBarrier(&___googleplayButton_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
