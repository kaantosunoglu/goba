﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// BGMusicController
struct BGMusicController_t4257240066;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BGMusicController
struct  BGMusicController_t4257240066  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean BGMusicController::ispaused
	bool ___ispaused_3;

public:
	inline static int32_t get_offset_of_ispaused_3() { return static_cast<int32_t>(offsetof(BGMusicController_t4257240066, ___ispaused_3)); }
	inline bool get_ispaused_3() const { return ___ispaused_3; }
	inline bool* get_address_of_ispaused_3() { return &___ispaused_3; }
	inline void set_ispaused_3(bool value)
	{
		___ispaused_3 = value;
	}
};

struct BGMusicController_t4257240066_StaticFields
{
public:
	// BGMusicController BGMusicController::instance
	BGMusicController_t4257240066 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(BGMusicController_t4257240066_StaticFields, ___instance_2)); }
	inline BGMusicController_t4257240066 * get_instance_2() const { return ___instance_2; }
	inline BGMusicController_t4257240066 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(BGMusicController_t4257240066 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
