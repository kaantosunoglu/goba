﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// Level[]
struct LevelU5BU5D_t2825411923;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Levels
struct  Levels_t748035019  : public Il2CppObject
{
public:
	// Level[] Levels::<levels>k__BackingField
	LevelU5BU5D_t2825411923* ___U3ClevelsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3ClevelsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Levels_t748035019, ___U3ClevelsU3Ek__BackingField_0)); }
	inline LevelU5BU5D_t2825411923* get_U3ClevelsU3Ek__BackingField_0() const { return ___U3ClevelsU3Ek__BackingField_0; }
	inline LevelU5BU5D_t2825411923** get_address_of_U3ClevelsU3Ek__BackingField_0() { return &___U3ClevelsU3Ek__BackingField_0; }
	inline void set_U3ClevelsU3Ek__BackingField_0(LevelU5BU5D_t2825411923* value)
	{
		___U3ClevelsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClevelsU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
