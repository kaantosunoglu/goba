﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShakeObject
struct  ShakeObject_t4224104523  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 ShakeObject::objectOrigin
	Vector3_t2243707580  ___objectOrigin_2;
	// UnityEngine.Vector3 ShakeObject::strength
	Vector3_t2243707580  ___strength_3;
	// UnityEngine.Vector3 ShakeObject::strengthDefault
	Vector3_t2243707580  ___strengthDefault_4;
	// System.Single ShakeObject::decay
	float ___decay_5;
	// System.Single ShakeObject::shakeTime
	float ___shakeTime_6;
	// System.Single ShakeObject::shakeTimeDefault
	float ___shakeTimeDefault_7;
	// System.Boolean ShakeObject::isShaking
	bool ___isShaking_8;

public:
	inline static int32_t get_offset_of_objectOrigin_2() { return static_cast<int32_t>(offsetof(ShakeObject_t4224104523, ___objectOrigin_2)); }
	inline Vector3_t2243707580  get_objectOrigin_2() const { return ___objectOrigin_2; }
	inline Vector3_t2243707580 * get_address_of_objectOrigin_2() { return &___objectOrigin_2; }
	inline void set_objectOrigin_2(Vector3_t2243707580  value)
	{
		___objectOrigin_2 = value;
	}

	inline static int32_t get_offset_of_strength_3() { return static_cast<int32_t>(offsetof(ShakeObject_t4224104523, ___strength_3)); }
	inline Vector3_t2243707580  get_strength_3() const { return ___strength_3; }
	inline Vector3_t2243707580 * get_address_of_strength_3() { return &___strength_3; }
	inline void set_strength_3(Vector3_t2243707580  value)
	{
		___strength_3 = value;
	}

	inline static int32_t get_offset_of_strengthDefault_4() { return static_cast<int32_t>(offsetof(ShakeObject_t4224104523, ___strengthDefault_4)); }
	inline Vector3_t2243707580  get_strengthDefault_4() const { return ___strengthDefault_4; }
	inline Vector3_t2243707580 * get_address_of_strengthDefault_4() { return &___strengthDefault_4; }
	inline void set_strengthDefault_4(Vector3_t2243707580  value)
	{
		___strengthDefault_4 = value;
	}

	inline static int32_t get_offset_of_decay_5() { return static_cast<int32_t>(offsetof(ShakeObject_t4224104523, ___decay_5)); }
	inline float get_decay_5() const { return ___decay_5; }
	inline float* get_address_of_decay_5() { return &___decay_5; }
	inline void set_decay_5(float value)
	{
		___decay_5 = value;
	}

	inline static int32_t get_offset_of_shakeTime_6() { return static_cast<int32_t>(offsetof(ShakeObject_t4224104523, ___shakeTime_6)); }
	inline float get_shakeTime_6() const { return ___shakeTime_6; }
	inline float* get_address_of_shakeTime_6() { return &___shakeTime_6; }
	inline void set_shakeTime_6(float value)
	{
		___shakeTime_6 = value;
	}

	inline static int32_t get_offset_of_shakeTimeDefault_7() { return static_cast<int32_t>(offsetof(ShakeObject_t4224104523, ___shakeTimeDefault_7)); }
	inline float get_shakeTimeDefault_7() const { return ___shakeTimeDefault_7; }
	inline float* get_address_of_shakeTimeDefault_7() { return &___shakeTimeDefault_7; }
	inline void set_shakeTimeDefault_7(float value)
	{
		___shakeTimeDefault_7 = value;
	}

	inline static int32_t get_offset_of_isShaking_8() { return static_cast<int32_t>(offsetof(ShakeObject_t4224104523, ___isShaking_8)); }
	inline bool get_isShaking_8() const { return ___isShaking_8; }
	inline bool* get_address_of_isShaking_8() { return &___isShaking_8; }
	inline void set_isShaking_8(bool value)
	{
		___isShaking_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
