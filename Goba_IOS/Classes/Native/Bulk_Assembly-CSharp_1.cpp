﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_SetBorderPosition3912786437.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_BoxCollider2D948534547.h"
#include "AssemblyU2DCSharp_ShakeObject4224104523.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "AssemblyU2DCSharp_SoundButton2170099235.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClicked2455055323.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097.h"
#include "mscorlib_System_Action_1_gen3627374100.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "AssemblyU2DCSharp_AudioManager4222704959.h"
#include "AssemblyU2DCSharp_InputManager1610719423.h"
#include "AssemblyU2DCSharp_StoreController3314850793.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_AppstoreHandler4742128.h"
#include "AssemblyU2DCSharp_UnityAds2588158543.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme1358843767.h"
#include "mscorlib_System_Action_1_gen258469394.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertisemen456670012.h"
#include "AssemblyU2DCSharp_UnityAds_U3CShowAdsU3Ec__AnonSto1075161772.h"
#include "AssemblyU2DCSharp_UnityAds_U3CShowAdsWithResultU3E1779434140.h"
#include "AssemblyU2DCSharp_AdController332951955.h"
#include "AssemblyU2DCSharp_WindowTransition4263452729.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"

// SetBorderPosition
struct SetBorderPosition_t3912786437;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t948534547;
// System.Object
struct Il2CppObject;
// ShakeObject
struct ShakeObject_t4224104523;
// UnityEngine.Component
struct Component_t3819376471;
// SoundButton
struct SoundButton_t2170099235;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t2455055323;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// AudioManager
struct AudioManager_t4222704959;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Sprite
struct Sprite_t309593783;
// InputManager
struct InputManager_t1610719423;
// StoreController
struct StoreController_t3314850793;
// AppstoreHandler
struct AppstoreHandler_t4742128;
// System.String
struct String_t;
// UnityAds
struct UnityAds_t2588158543;
// UnityEngine.Object
struct Object_t1021602117;
// UnityEngine.Advertisements.ShowOptions
struct ShowOptions_t1358843767;
// System.Action`1<UnityEngine.Advertisements.ShowResult>
struct Action_1_t258469394;
// UnityAds/<ShowAds>c__AnonStorey0
struct U3CShowAdsU3Ec__AnonStorey0_t1075161772;
// UnityAds/<ShowAdsWithResult>c__AnonStorey1
struct U3CShowAdsWithResultU3Ec__AnonStorey1_t1779434140;
// AdController
struct AdController_t332951955;
// WindowTransition
struct WindowTransition_t4263452729;
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var;
extern const uint32_t SetBorderPosition_UpdateBorderPosition_m1547445462_MetadataUsageId;
extern Il2CppClass* SoundButton_t2170099235_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_t4025899511_il2cpp_TypeInfo_var;
extern const MethodInfo* SoundButton_U3CStartU3Em__0_m1140240249_MethodInfo_var;
extern const uint32_t SoundButton_Start_m230627070_MetadataUsageId;
extern Il2CppClass* Action_1_t3627374100_il2cpp_TypeInfo_var;
extern const MethodInfo* SoundButton_OnSoundStatusChanged_m3804381305_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1505245730_MethodInfo_var;
extern const uint32_t SoundButton_OnEnable_m805870782_MetadataUsageId;
extern const uint32_t SoundButton_OnDisable_m4157794471_MetadataUsageId;
extern Il2CppClass* InputManager_t1610719423_il2cpp_TypeInfo_var;
extern const uint32_t SoundButton_U3CStartU3Em__0_m1140240249_MetadataUsageId;
extern Il2CppClass* Singleton_1_t1180626515_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Singleton_1_get_Instance_m1587111280_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1816603299;
extern Il2CppCodeGenString* _stringLiteral1928816527;
extern const uint32_t StoreController_OpenStore_m1069579799_MetadataUsageId;
extern Il2CppClass* UnityAds_t2588158543_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t UnityAds_Awake_m496078031_MetadataUsageId;
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern const uint32_t UnityAds_init_m2368137515_MetadataUsageId;
extern const uint32_t UnityAds_init_m3042105259_MetadataUsageId;
extern const uint32_t UnityAds_IsReady_m3903979447_MetadataUsageId;
extern const uint32_t UnityAds_IsReady_m3992081477_MetadataUsageId;
extern const uint32_t UnityAds_isShowing_m327126927_MetadataUsageId;
extern const uint32_t UnityAds_isSupported_m3159842756_MetadataUsageId;
extern const uint32_t UnityAds_getGameID_m2968177404_MetadataUsageId;
extern const uint32_t UnityAds_ShowAds_m3299494663_MetadataUsageId;
extern const uint32_t UnityAds_ShowAds_m1766596889_MetadataUsageId;
extern const uint32_t UnityAds_ShowAds_m2333705529_MetadataUsageId;
extern Il2CppClass* U3CShowAdsU3Ec__AnonStorey0_t1075161772_il2cpp_TypeInfo_var;
extern Il2CppClass* ShowOptions_t1358843767_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t258469394_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CShowAdsU3Ec__AnonStorey0_U3CU3Em__0_m948133887_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m1775481480_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m2290710958_MethodInfo_var;
extern const uint32_t UnityAds_ShowAds_m3875099862_MetadataUsageId;
extern Il2CppClass* U3CShowAdsWithResultU3Ec__AnonStorey1_t1779434140_il2cpp_TypeInfo_var;
extern Il2CppClass* AdController_t332951955_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CShowAdsWithResultU3Ec__AnonStorey1_U3CU3Em__0_m4294232865_MethodInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t UnityAds_ShowAdsWithResult_m2127664455_MetadataUsageId;
extern const uint32_t U3CShowAdsU3Ec__AnonStorey0_U3CU3Em__0_m948133887_MetadataUsageId;
extern const uint32_t U3CShowAdsWithResultU3Ec__AnonStorey1_U3CU3Em__0_m4294232865_MetadataUsageId;
extern const uint32_t WindowTransition_Awake_m955870837_MetadataUsageId;
extern const uint32_t WindowTransition_OnWindowAdded_m950623741_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4158966261;
extern const uint32_t WindowTransition_OnWindowRemove_m151772023_MetadataUsageId;
extern const uint32_t WindowTransition_AnimateWindowOnLoad_m3850184268_MetadataUsageId;
extern const uint32_t WindowTransition_AnimateWindowOnDestroy_m3935909200_MetadataUsageId;
extern const uint32_t WindowTransition_OnRemoveTransitionComplete_m3541400377_MetadataUsageId;



// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1505245730_gshared (Action_1_t3627374100 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// T Singleton`1<System.Object>::get_Instance()
extern "C"  Il2CppObject * Singleton_1_get_Instance_m1676179234_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
// System.Void System.Action`1<UnityEngine.Advertisements.ShowResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1775481480_gshared (Action_1_t258469394 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Void System.Action`1<UnityEngine.Advertisements.ShowResult>::Invoke(!0)
extern "C"  void Action_1_Invoke_m2290710958_gshared (Action_1_t258469394 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Action`1<System.Boolean>::Invoke(!0)
extern "C"  void Action_1_Invoke_m3662000152_gshared (Action_1_t3627374100 * __this, bool p0, const MethodInfo* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t189460977 * Camera_get_main_m475173995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_orthographicSize()
extern "C"  float Camera_get_orthographicSize_m1801974358 (Camera_t189460977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetBorderPosition::UpdateBorderPosition()
extern "C"  void SetBorderPosition_UpdateBorderPosition_m1547445462 (SetBorderPosition_t3912786437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m41137238 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1051800773 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3275118058 * GameObject_get_transform_m909382139 (GameObject_t1756533147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m2638739322 (Vector3_t2243707580 * __this, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2469242620 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.BoxCollider2D>()
#define GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(__this, method) ((  BoxCollider2D_t948534547 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3067419446 (Vector2_t2243707579 * __this, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BoxCollider2D::set_size(UnityEngine.Vector2)
extern "C"  void BoxCollider2D_set_size_m3505152706 (BoxCollider2D_t948534547 * __this, Vector2_t2243707579  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m1104419803 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m2233168104 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C"  float Random_Range_m2884721203 (Il2CppObject * __this /* static, unused */, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_op_Multiply_m1351554733 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Inequality_m799191452 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
extern "C"  ButtonClickedEvent_t2455055323 * Button_get_onClick_m1595880935 (Button_t2872111280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction__ctor_m2649891629 (UnityAction_t4025899511 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
extern "C"  void UnityEvent_AddListener_m1596810379 (UnityEvent_t408735097 * __this, UnityAction_t4025899511 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m1505245730(__this, p0, p1, method) ((  void (*) (Action_1_t3627374100 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m1505245730_gshared)(__this, p0, p1, method)
// System.Void AudioManager::add_OnSoundStatusChangedEvent(System.Action`1<System.Boolean>)
extern "C"  void AudioManager_add_OnSoundStatusChangedEvent_m651860201 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundButton::initSoundStatus()
extern "C"  void SoundButton_initSoundStatus_m3239875359 (SoundButton_t2170099235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioManager::remove_OnSoundStatusChangedEvent(System.Action`1<System.Boolean>)
extern "C"  void AudioManager_remove_OnSoundStatusChangedEvent_m786182558 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AudioManager AudioManager::get_instance()
extern "C"  AudioManager_t4222704959 * AudioManager_get_instance_m335610332 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
extern "C"  void Image_set_sprite_m1800056820 (Image_t2042527209 * __this, Sprite_t309593783 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InputManager::canInput(System.Single,System.Boolean)
extern "C"  bool InputManager_canInput_m3448272564 (InputManager_t1610719423 * __this, float ___delay0, bool ___disableOnAvailable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputManager::DisableTouchForDelay(System.Single)
extern "C"  void InputManager_DisableTouchForDelay_m1438816274 (InputManager_t1610719423 * __this, float ___delay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputManager::AddButtonTouchEffect()
extern "C"  void InputManager_AddButtonTouchEffect_m393219379 (InputManager_t1610719423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioManager::ToggleSoundStatus()
extern "C"  void AudioManager_ToggleSoundStatus_m36763651 (AudioManager_t4222704959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Application::get_isEditor()
extern "C"  bool Application_get_isEditor_m2474583393 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// T Singleton`1<AppstoreHandler>::get_Instance()
#define Singleton_1_get_Instance_m1587111280(__this /* static, unused */, method) ((  AppstoreHandler_t4742128 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_Instance_m1676179234_gshared)(__this /* static, unused */, method)
// System.Void AppstoreHandler::openAppInStore(System.String)
extern "C"  void AppstoreHandler_openAppInStore_m4154941916 (AppstoreHandler_t4742128 * __this, String_t* ___appID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m920475918 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m4145850038 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityAds::init(System.Boolean)
extern "C"  void UnityAds_init_m2368137515 (UnityAds_t2588158543 * __this, bool ___enableTestMode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.Advertisement::get_isInitialized()
extern "C"  bool Advertisement_get_isInitialized_m167127575 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String)
extern "C"  void Advertisement_Initialize_m1497874238 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean)
extern "C"  void Advertisement_Initialize_m2995186509 (Il2CppObject * __this /* static, unused */, String_t* p0, bool p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.Advertisement::IsReady()
extern "C"  bool Advertisement_IsReady_m876308337 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.Advertisement::IsReady(System.String)
extern "C"  bool Advertisement_IsReady_m1389121855 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.Advertisement::get_isShowing()
extern "C"  bool Advertisement_get_isShowing_m2678672924 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.Advertisement::get_isSupported()
extern "C"  bool Advertisement_get_isSupported_m1183881609 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.Advertisement::get_gameId()
extern "C"  String_t* Advertisement_get_gameId_m3262742795 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.Advertisement::Show()
extern "C"  void Advertisement_Show_m2036493855 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.Advertisement::Show(System.String)
extern "C"  void Advertisement_Show_m994665589 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.Advertisement::Show(System.String,UnityEngine.Advertisements.ShowOptions)
extern "C"  void Advertisement_Show_m3789622005 (Il2CppObject * __this /* static, unused */, String_t* p0, ShowOptions_t1358843767 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityAds/<ShowAds>c__AnonStorey0::.ctor()
extern "C"  void U3CShowAdsU3Ec__AnonStorey0__ctor_m2604436093 (U3CShowAdsU3Ec__AnonStorey0_t1075161772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.ShowOptions::.ctor()
extern "C"  void ShowOptions__ctor_m3915763896 (ShowOptions_t1358843767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<UnityEngine.Advertisements.ShowResult>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m1775481480(__this, p0, p1, method) ((  void (*) (Action_1_t258469394 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_1__ctor_m1775481480_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Advertisements.ShowOptions::set_resultCallback(System.Action`1<UnityEngine.Advertisements.ShowResult>)
extern "C"  void ShowOptions_set_resultCallback_m3136348778 (ShowOptions_t1358843767 * __this, Action_1_t258469394 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<UnityEngine.Advertisements.ShowResult>::Invoke(!0)
#define Action_1_Invoke_m2290710958(__this, p0, method) ((  void (*) (Action_1_t258469394 *, int32_t, const MethodInfo*))Action_1_Invoke_m2290710958_gshared)(__this, p0, method)
// System.Void UnityAds/<ShowAdsWithResult>c__AnonStorey1::.ctor()
extern "C"  void U3CShowAdsWithResultU3Ec__AnonStorey1__ctor_m3151670947 (U3CShowAdsWithResultU3Ec__AnonStorey1_t1779434140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdController::hideBanner()
extern "C"  void AdController_hideBanner_m1716557388 (AdController_t332951955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<System.Boolean>::Invoke(!0)
#define Action_1_Invoke_m3662000152(__this, p0, method) ((  void (*) (Action_1_t3627374100 *, bool, const MethodInfo*))Action_1_Invoke_m3662000152_gshared)(__this, p0, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t2243707580  Vector3_get_zero_m1527993324 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m2856731593 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C"  Vector3_t2243707580  Transform_get_localPosition_m2533925116 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern "C"  void MonoBehaviour_Invoke_m666563676 (MonoBehaviour_t1158329972 * __this, String_t* p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowTransition::OnRemoveTransitionComplete()
extern "C"  void WindowTransition_OnRemoveTransitionComplete_m3541400377 (WindowTransition_t4263452729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowTransition::FadeInBackLayOnLoad()
extern "C"  void WindowTransition_FadeInBackLayOnLoad_m4095212997 (WindowTransition_t4263452729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WindowTransition::FadeOutBacklayOnDestroy()
extern "C"  void WindowTransition_FadeOutBacklayOnDestroy_m617660916 (WindowTransition_t4263452729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m1909920690 (Color_t2020392075 * __this, float p0, float p1, float p2, float p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m1026930133 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m2887581199 (GameObject_t1756533147 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SetBorderPosition::.ctor()
extern "C"  void SetBorderPosition__ctor_m1256989040 (SetBorderPosition_t3912786437 * __this, const MethodInfo* method)
{
	{
		__this->set_CameraOtrhoSize_2((5.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SetBorderPosition::Awake()
extern "C"  void SetBorderPosition_Awake_m3929930017 (SetBorderPosition_t3912786437 * __this, const MethodInfo* method)
{
	{
		Camera_t189460977 * L_0 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		float L_1 = Camera_get_orthographicSize_m1801974358(L_0, /*hidden argument*/NULL);
		__this->set_CameraOtrhoSize_2(L_1);
		return;
	}
}
// System.Void SetBorderPosition::Start()
extern "C"  void SetBorderPosition_Start_m503800224 (SetBorderPosition_t3912786437 * __this, const MethodInfo* method)
{
	{
		SetBorderPosition_UpdateBorderPosition_m1547445462(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SetBorderPosition::UpdateBorderPosition()
extern "C"  void SetBorderPosition_UpdateBorderPosition_m1547445462 (SetBorderPosition_t3912786437 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SetBorderPosition_UpdateBorderPosition_m1547445462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = __this->get_CameraOtrhoSize_2();
		V_0 = ((float)((float)L_0*(float)(2.0f)));
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = V_0;
		V_1 = ((float)((float)((float)((float)(((float)((float)L_1)))/(float)(((float)((float)L_2)))))*(float)L_3));
		GameObject_t1756533147 * L_4 = __this->get_borderLeft_3();
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		float L_6 = V_1;
		Vector3_t2243707580  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m2638739322(&L_7, ((-((float)((float)L_6/(float)(2.0f))))), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_position_m2469242620(L_5, L_7, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = __this->get_borderRight_4();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		float L_10 = V_1;
		Vector3_t2243707580  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Vector3__ctor_m2638739322(&L_11, ((float)((float)L_10/(float)(2.0f))), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_position_m2469242620(L_9, L_11, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_12 = __this->get_borderTop_5();
		NullCheck(L_12);
		BoxCollider2D_t948534547 * L_13 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_12, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		float L_14 = V_1;
		Vector2_t2243707579  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Vector2__ctor_m3067419446(&L_15, L_14, (0.1f), /*hidden argument*/NULL);
		NullCheck(L_13);
		BoxCollider2D_set_size_m3505152706(L_13, L_15, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_16 = __this->get_borderBottom_6();
		NullCheck(L_16);
		BoxCollider2D_t948534547 * L_17 = GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061(L_16, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t948534547_m1829960061_MethodInfo_var);
		float L_18 = V_1;
		Vector2_t2243707579  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector2__ctor_m3067419446(&L_19, L_18, (0.1f), /*hidden argument*/NULL);
		NullCheck(L_17);
		BoxCollider2D_set_size_m3505152706(L_17, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShakeObject::.ctor()
extern "C"  void ShakeObject__ctor_m1112667244 (ShakeObject_t4224104523 * __this, const MethodInfo* method)
{
	{
		__this->set_decay_5((0.8f));
		__this->set_shakeTime_6((1.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShakeObject::Start()
extern "C"  void ShakeObject_Start_m2563221968 (ShakeObject_t4224104523 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		__this->set_objectOrigin_2(L_1);
		Vector3_t2243707580  L_2 = __this->get_strength_3();
		__this->set_strengthDefault_4(L_2);
		float L_3 = __this->get_shakeTime_6();
		__this->set_shakeTimeDefault_7(L_3);
		return;
	}
}
// System.Void ShakeObject::Update()
extern "C"  void ShakeObject_Update_m14455671 (ShakeObject_t4224104523 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = __this->get_isShaking_8();
		if (!L_0)
		{
			goto IL_0144;
		}
	}
	{
		float L_1 = __this->get_shakeTime_6();
		if ((!(((float)L_1) > ((float)(0.0f)))))
		{
			goto IL_00ee;
		}
	}
	{
		float L_2 = __this->get_shakeTime_6();
		float L_3 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_shakeTime_6(((float)((float)L_2-(float)L_3)));
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Vector3_t2243707580 * L_6 = __this->get_address_of_objectOrigin_2();
		float L_7 = L_6->get_x_1();
		Vector3_t2243707580 * L_8 = __this->get_address_of_strength_3();
		float L_9 = L_8->get_x_1();
		Vector3_t2243707580 * L_10 = __this->get_address_of_strength_3();
		float L_11 = L_10->get_x_1();
		float L_12 = Random_Range_m2884721203(NULL /*static, unused*/, ((-L_9)), L_11, /*hidden argument*/NULL);
		(&V_0)->set_x_1(((float)((float)L_7+(float)L_12)));
		Vector3_t2243707580 * L_13 = __this->get_address_of_objectOrigin_2();
		float L_14 = L_13->get_y_2();
		Vector3_t2243707580 * L_15 = __this->get_address_of_strength_3();
		float L_16 = L_15->get_y_2();
		Vector3_t2243707580 * L_17 = __this->get_address_of_strength_3();
		float L_18 = L_17->get_y_2();
		float L_19 = Random_Range_m2884721203(NULL /*static, unused*/, ((-L_16)), L_18, /*hidden argument*/NULL);
		(&V_0)->set_y_2(((float)((float)L_14+(float)L_19)));
		Vector3_t2243707580 * L_20 = __this->get_address_of_objectOrigin_2();
		float L_21 = L_20->get_z_3();
		Vector3_t2243707580 * L_22 = __this->get_address_of_strength_3();
		float L_23 = L_22->get_z_3();
		Vector3_t2243707580 * L_24 = __this->get_address_of_strength_3();
		float L_25 = L_24->get_z_3();
		float L_26 = Random_Range_m2884721203(NULL /*static, unused*/, ((-L_23)), L_25, /*hidden argument*/NULL);
		(&V_0)->set_z_3(((float)((float)L_21+(float)L_26)));
		Transform_t3275118058 * L_27 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_28 = V_0;
		NullCheck(L_27);
		Transform_set_position_m2469242620(L_27, L_28, /*hidden argument*/NULL);
		Vector3_t2243707580  L_29 = __this->get_strength_3();
		float L_30 = __this->get_decay_5();
		Vector3_t2243707580  L_31 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		__this->set_strength_3(L_31);
		goto IL_0144;
	}

IL_00ee:
	{
		Transform_t3275118058 * L_32 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		Vector3_t2243707580  L_33 = Transform_get_position_m1104419803(L_32, /*hidden argument*/NULL);
		Vector3_t2243707580  L_34 = __this->get_objectOrigin_2();
		bool L_35 = Vector3_op_Inequality_m799191452(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_0144;
		}
	}
	{
		__this->set_shakeTime_6((0.0f));
		Transform_t3275118058 * L_36 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_37 = __this->get_objectOrigin_2();
		NullCheck(L_36);
		Transform_set_position_m2469242620(L_36, L_37, /*hidden argument*/NULL);
		__this->set_isShaking_8((bool)0);
		Vector3_t2243707580  L_38 = __this->get_strengthDefault_4();
		__this->set_strength_3(L_38);
		float L_39 = __this->get_shakeTimeDefault_7();
		__this->set_shakeTime_6(L_39);
	}

IL_0144:
	{
		return;
	}
}
// System.Void ShakeObject::StopShake()
extern "C"  void ShakeObject_StopShake_m960177446 (ShakeObject_t4224104523 * __this, const MethodInfo* method)
{
	{
		__this->set_shakeTime_6((0.0f));
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = __this->get_objectOrigin_2();
		NullCheck(L_0);
		Transform_set_position_m2469242620(L_0, L_1, /*hidden argument*/NULL);
		__this->set_isShaking_8((bool)0);
		Vector3_t2243707580  L_2 = __this->get_strengthDefault_4();
		__this->set_strength_3(L_2);
		float L_3 = __this->get_shakeTimeDefault_7();
		__this->set_shakeTime_6(L_3);
		return;
	}
}
// System.Void ShakeObject::StartShake()
extern "C"  void ShakeObject_StartShake_m2998555612 (ShakeObject_t4224104523 * __this, const MethodInfo* method)
{
	{
		__this->set_isShaking_8((bool)1);
		Vector3_t2243707580  L_0 = __this->get_strengthDefault_4();
		__this->set_strength_3(L_0);
		float L_1 = __this->get_shakeTimeDefault_7();
		__this->set_shakeTime_6(L_1);
		return;
	}
}
// System.Void SoundButton::.ctor()
extern "C"  void SoundButton__ctor_m1116494034 (SoundButton_t2170099235 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundButton::Start()
extern "C"  void SoundButton_Start_m230627070 (SoundButton_t2170099235 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoundButton_Start_m230627070_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ButtonClickedEvent_t2455055323 * G_B2_0 = NULL;
	ButtonClickedEvent_t2455055323 * G_B1_0 = NULL;
	{
		Button_t2872111280 * L_0 = __this->get_btnSound_2();
		NullCheck(L_0);
		ButtonClickedEvent_t2455055323 * L_1 = Button_get_onClick_m1595880935(L_0, /*hidden argument*/NULL);
		UnityAction_t4025899511 * L_2 = ((SoundButton_t2170099235_StaticFields*)SoundButton_t2170099235_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_6();
		G_B1_0 = L_1;
		if (L_2)
		{
			G_B2_0 = L_1;
			goto IL_0023;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)SoundButton_U3CStartU3Em__0_m1140240249_MethodInfo_var);
		UnityAction_t4025899511 * L_4 = (UnityAction_t4025899511 *)il2cpp_codegen_object_new(UnityAction_t4025899511_il2cpp_TypeInfo_var);
		UnityAction__ctor_m2649891629(L_4, NULL, L_3, /*hidden argument*/NULL);
		((SoundButton_t2170099235_StaticFields*)SoundButton_t2170099235_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_6(L_4);
		G_B2_0 = G_B1_0;
	}

IL_0023:
	{
		UnityAction_t4025899511 * L_5 = ((SoundButton_t2170099235_StaticFields*)SoundButton_t2170099235_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_6();
		NullCheck(G_B2_0);
		UnityEvent_AddListener_m1596810379(G_B2_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundButton::OnEnable()
extern "C"  void SoundButton_OnEnable_m805870782 (SoundButton_t2170099235 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoundButton_OnEnable_m805870782_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)SoundButton_OnSoundStatusChanged_m3804381305_MethodInfo_var);
		Action_1_t3627374100 * L_1 = (Action_1_t3627374100 *)il2cpp_codegen_object_new(Action_1_t3627374100_il2cpp_TypeInfo_var);
		Action_1__ctor_m1505245730(L_1, __this, L_0, /*hidden argument*/Action_1__ctor_m1505245730_MethodInfo_var);
		AudioManager_add_OnSoundStatusChangedEvent_m651860201(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		SoundButton_initSoundStatus_m3239875359(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundButton::OnDisable()
extern "C"  void SoundButton_OnDisable_m4157794471 (SoundButton_t2170099235 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoundButton_OnDisable_m4157794471_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)SoundButton_OnSoundStatusChanged_m3804381305_MethodInfo_var);
		Action_1_t3627374100 * L_1 = (Action_1_t3627374100 *)il2cpp_codegen_object_new(Action_1_t3627374100_il2cpp_TypeInfo_var);
		Action_1__ctor_m1505245730(L_1, __this, L_0, /*hidden argument*/Action_1__ctor_m1505245730_MethodInfo_var);
		AudioManager_remove_OnSoundStatusChangedEvent_m786182558(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundButton::initSoundStatus()
extern "C"  void SoundButton_initSoundStatus_m3239875359 (SoundButton_t2170099235 * __this, const MethodInfo* method)
{
	Image_t2042527209 * G_B2_0 = NULL;
	Image_t2042527209 * G_B1_0 = NULL;
	Sprite_t309593783 * G_B3_0 = NULL;
	Image_t2042527209 * G_B3_1 = NULL;
	{
		Image_t2042527209 * L_0 = __this->get_btnSoundImage_3();
		AudioManager_t4222704959 * L_1 = AudioManager_get_instance_m335610332(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = L_1->get_isSoundEnabled_4();
		G_B1_0 = L_0;
		if (!L_2)
		{
			G_B2_0 = L_0;
			goto IL_0020;
		}
	}
	{
		Sprite_t309593783 * L_3 = __this->get_soundOnSprite_4();
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		goto IL_0026;
	}

IL_0020:
	{
		Sprite_t309593783 * L_4 = __this->get_soundOffSprite_5();
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
	}

IL_0026:
	{
		NullCheck(G_B3_1);
		Image_set_sprite_m1800056820(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundButton::OnSoundStatusChanged(System.Boolean)
extern "C"  void SoundButton_OnSoundStatusChanged_m3804381305 (SoundButton_t2170099235 * __this, bool ___isSoundEnabled0, const MethodInfo* method)
{
	Image_t2042527209 * G_B2_0 = NULL;
	Image_t2042527209 * G_B1_0 = NULL;
	Sprite_t309593783 * G_B3_0 = NULL;
	Image_t2042527209 * G_B3_1 = NULL;
	{
		Image_t2042527209 * L_0 = __this->get_btnSoundImage_3();
		bool L_1 = ___isSoundEnabled0;
		G_B1_0 = L_0;
		if (!L_1)
		{
			G_B2_0 = L_0;
			goto IL_0017;
		}
	}
	{
		Sprite_t309593783 * L_2 = __this->get_soundOnSprite_4();
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_001d;
	}

IL_0017:
	{
		Sprite_t309593783 * L_3 = __this->get_soundOffSprite_5();
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_001d:
	{
		NullCheck(G_B3_1);
		Image_set_sprite_m1800056820(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SoundButton::<Start>m__0()
extern "C"  void SoundButton_U3CStartU3Em__0_m1140240249 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoundButton_U3CStartU3Em__0_m1140240249_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InputManager_t1610719423_il2cpp_TypeInfo_var);
		InputManager_t1610719423 * L_0 = ((InputManager_t1610719423_StaticFields*)InputManager_t1610719423_il2cpp_TypeInfo_var->static_fields)->get_instance_7();
		NullCheck(L_0);
		bool L_1 = InputManager_canInput_m3448272564(L_0, (0.5f), (bool)1, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InputManager_t1610719423_il2cpp_TypeInfo_var);
		InputManager_t1610719423 * L_2 = ((InputManager_t1610719423_StaticFields*)InputManager_t1610719423_il2cpp_TypeInfo_var->static_fields)->get_instance_7();
		NullCheck(L_2);
		InputManager_DisableTouchForDelay_m1438816274(L_2, (0.5f), /*hidden argument*/NULL);
		InputManager_t1610719423 * L_3 = ((InputManager_t1610719423_StaticFields*)InputManager_t1610719423_il2cpp_TypeInfo_var->static_fields)->get_instance_7();
		NullCheck(L_3);
		InputManager_AddButtonTouchEffect_m393219379(L_3, /*hidden argument*/NULL);
		AudioManager_t4222704959 * L_4 = AudioManager_get_instance_m335610332(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		AudioManager_ToggleSoundStatus_m36763651(L_4, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void StoreController::.ctor()
extern "C"  void StoreController__ctor_m2325745012 (StoreController_t3314850793 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StoreController::Start()
extern "C"  void StoreController_Start_m3142075460 (StoreController_t3314850793 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void StoreController::Update()
extern "C"  void StoreController_Update_m3540022313 (StoreController_t3314850793 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void StoreController::OpenStore()
extern "C"  void StoreController_OpenStore_m1069579799 (StoreController_t3314850793 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StoreController_OpenStore_m1069579799_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_1_t1180626515_il2cpp_TypeInfo_var);
		AppstoreHandler_t4742128 * L_1 = Singleton_1_get_Instance_m1587111280(NULL /*static, unused*/, /*hidden argument*/Singleton_1_get_Instance_m1587111280_MethodInfo_var);
		NullCheck(L_1);
		AppstoreHandler_openAppInStore_m4154941916(L_1, _stringLiteral1816603299, /*hidden argument*/NULL);
		goto IL_0028;
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1928816527, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void UnityAds::.ctor()
extern "C"  void UnityAds__ctor_m2385973988 (UnityAds_t2588158543 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityAds::Awake()
extern "C"  void UnityAds_Awake_m496078031 (UnityAds_t2588158543 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAds_Awake_m496078031_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityAds_t2588158543 * L_0 = ((UnityAds_t2588158543_StaticFields*)UnityAds_t2588158543_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}

IL_001c:
	{
		((UnityAds_t2588158543_StaticFields*)UnityAds_t2588158543_il2cpp_TypeInfo_var->static_fields)->set_instance_2(__this);
		UnityAds_init_m2368137515(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityAds::init(System.Boolean)
extern "C"  void UnityAds_init_m2368137515 (UnityAds_t2588158543 * __this, bool ___enableTestMode0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAds_init_m2368137515_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_0 = Advertisement_get_isInitialized_m167127575(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		String_t* L_1 = __this->get_iOSAppID_4();
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		Advertisement_Initialize_m1497874238(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityAds::init(System.String,System.String,System.Boolean)
extern "C"  void UnityAds_init_m3042105259 (UnityAds_t2588158543 * __this, String_t* ___androidAppId0, String_t* ___iosAppId1, bool ___enableTestMode2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAds_init_m3042105259_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_0 = Advertisement_get_isInitialized_m167127575(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___iosAppId1;
		bool L_2 = ___enableTestMode2;
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		Advertisement_Initialize_m2995186509(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean UnityAds::IsReady()
extern "C"  bool UnityAds_IsReady_m3903979447 (UnityAds_t2588158543 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAds_IsReady_m3903979447_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_0 = Advertisement_IsReady_m876308337(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityAds::IsReady(System.String)
extern "C"  bool UnityAds_IsReady_m3992081477 (UnityAds_t2588158543 * __this, String_t* ___adZone0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAds_IsReady_m3992081477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___adZone0;
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_1 = Advertisement_IsReady_m1389121855(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityAds::isShowing()
extern "C"  bool UnityAds_isShowing_m327126927 (UnityAds_t2588158543 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAds_isShowing_m327126927_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_0 = Advertisement_get_isShowing_m2678672924(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityAds::isSupported()
extern "C"  bool UnityAds_isSupported_m3159842756 (UnityAds_t2588158543 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAds_isSupported_m3159842756_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_0 = Advertisement_get_isSupported_m1183881609(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityAds::getGameID()
extern "C"  String_t* UnityAds_getGameID_m2968177404 (UnityAds_t2588158543 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAds_getGameID_m2968177404_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		String_t* L_0 = Advertisement_get_gameId_m3262742795(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityAds::SetCampaignDataURL(System.String)
extern "C"  void UnityAds_SetCampaignDataURL_m2599277803 (UnityAds_t2588158543 * __this, String_t* ___URL0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityAds::ShowAds()
extern "C"  void UnityAds_ShowAds_m3299494663 (UnityAds_t2588158543 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAds_ShowAds_m3299494663_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_0 = Advertisement_IsReady_m876308337(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		Advertisement_Show_m2036493855(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000f:
	{
		return;
	}
}
// System.Void UnityAds::ShowAds(System.String)
extern "C"  void UnityAds_ShowAds_m1766596889 (UnityAds_t2588158543 * __this, String_t* ___zoneId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAds_ShowAds_m1766596889_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___zoneId0;
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_1 = Advertisement_IsReady_m1389121855(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_2 = ___zoneId0;
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		Advertisement_Show_m994665589(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void UnityAds::ShowAds(System.String,UnityEngine.Advertisements.ShowOptions)
extern "C"  void UnityAds_ShowAds_m2333705529 (UnityAds_t2588158543 * __this, String_t* ___zoneId0, ShowOptions_t1358843767 * ___options1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAds_ShowAds_m2333705529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___zoneId0;
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_1 = Advertisement_IsReady_m1389121855(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___zoneId0;
		ShowOptions_t1358843767 * L_3 = ___options1;
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		Advertisement_Show_m3789622005(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityAds::ShowAds(System.String,System.Action`1<UnityEngine.Advertisements.ShowResult>)
extern "C"  void UnityAds_ShowAds_m3875099862 (UnityAds_t2588158543 * __this, String_t* ___zoneId0, Action_1_t258469394 * ___adResult1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAds_ShowAds_m3875099862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CShowAdsU3Ec__AnonStorey0_t1075161772 * V_0 = NULL;
	ShowOptions_t1358843767 * V_1 = NULL;
	{
		U3CShowAdsU3Ec__AnonStorey0_t1075161772 * L_0 = (U3CShowAdsU3Ec__AnonStorey0_t1075161772 *)il2cpp_codegen_object_new(U3CShowAdsU3Ec__AnonStorey0_t1075161772_il2cpp_TypeInfo_var);
		U3CShowAdsU3Ec__AnonStorey0__ctor_m2604436093(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CShowAdsU3Ec__AnonStorey0_t1075161772 * L_1 = V_0;
		Action_1_t258469394 * L_2 = ___adResult1;
		NullCheck(L_1);
		L_1->set_adResult_0(L_2);
		String_t* L_3 = ___zoneId0;
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_4 = Advertisement_IsReady_m1389121855(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003c;
		}
	}
	{
		String_t* L_5 = ___zoneId0;
		ShowOptions_t1358843767 * L_6 = (ShowOptions_t1358843767 *)il2cpp_codegen_object_new(ShowOptions_t1358843767_il2cpp_TypeInfo_var);
		ShowOptions__ctor_m3915763896(L_6, /*hidden argument*/NULL);
		V_1 = L_6;
		ShowOptions_t1358843767 * L_7 = V_1;
		U3CShowAdsU3Ec__AnonStorey0_t1075161772 * L_8 = V_0;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)U3CShowAdsU3Ec__AnonStorey0_U3CU3Em__0_m948133887_MethodInfo_var);
		Action_1_t258469394 * L_10 = (Action_1_t258469394 *)il2cpp_codegen_object_new(Action_1_t258469394_il2cpp_TypeInfo_var);
		Action_1__ctor_m1775481480(L_10, L_8, L_9, /*hidden argument*/Action_1__ctor_m1775481480_MethodInfo_var);
		NullCheck(L_7);
		ShowOptions_set_resultCallback_m3136348778(L_7, L_10, /*hidden argument*/NULL);
		ShowOptions_t1358843767 * L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		Advertisement_Show_m3789622005(NULL /*static, unused*/, L_5, L_11, /*hidden argument*/NULL);
		goto IL_0048;
	}

IL_003c:
	{
		U3CShowAdsU3Ec__AnonStorey0_t1075161772 * L_12 = V_0;
		NullCheck(L_12);
		Action_1_t258469394 * L_13 = L_12->get_adResult_0();
		NullCheck(L_13);
		Action_1_Invoke_m2290710958(L_13, 0, /*hidden argument*/Action_1_Invoke_m2290710958_MethodInfo_var);
	}

IL_0048:
	{
		return;
	}
}
// System.Void UnityAds::ShowAdsWithResult(System.String,System.Action`1<System.Boolean>)
extern "C"  void UnityAds_ShowAdsWithResult_m2127664455 (UnityAds_t2588158543 * __this, String_t* ___zoneId0, Action_1_t3627374100 * ___adResult1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAds_ShowAdsWithResult_m2127664455_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CShowAdsWithResultU3Ec__AnonStorey1_t1779434140 * V_0 = NULL;
	ShowOptions_t1358843767 * V_1 = NULL;
	{
		U3CShowAdsWithResultU3Ec__AnonStorey1_t1779434140 * L_0 = (U3CShowAdsWithResultU3Ec__AnonStorey1_t1779434140 *)il2cpp_codegen_object_new(U3CShowAdsWithResultU3Ec__AnonStorey1_t1779434140_il2cpp_TypeInfo_var);
		U3CShowAdsWithResultU3Ec__AnonStorey1__ctor_m3151670947(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CShowAdsWithResultU3Ec__AnonStorey1_t1779434140 * L_1 = V_0;
		Action_1_t3627374100 * L_2 = ___adResult1;
		NullCheck(L_1);
		L_1->set_adResult_0(L_2);
		String_t* L_3 = ___zoneId0;
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_4 = Advertisement_IsReady_m1389121855(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0046;
		}
	}
	{
		AdController_t332951955 * L_5 = ((AdController_t332951955_StaticFields*)AdController_t332951955_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_5);
		AdController_hideBanner_m1716557388(L_5, /*hidden argument*/NULL);
		String_t* L_6 = ___zoneId0;
		ShowOptions_t1358843767 * L_7 = (ShowOptions_t1358843767 *)il2cpp_codegen_object_new(ShowOptions_t1358843767_il2cpp_TypeInfo_var);
		ShowOptions__ctor_m3915763896(L_7, /*hidden argument*/NULL);
		V_1 = L_7;
		ShowOptions_t1358843767 * L_8 = V_1;
		U3CShowAdsWithResultU3Ec__AnonStorey1_t1779434140 * L_9 = V_0;
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)U3CShowAdsWithResultU3Ec__AnonStorey1_U3CU3Em__0_m4294232865_MethodInfo_var);
		Action_1_t258469394 * L_11 = (Action_1_t258469394 *)il2cpp_codegen_object_new(Action_1_t258469394_il2cpp_TypeInfo_var);
		Action_1__ctor_m1775481480(L_11, L_9, L_10, /*hidden argument*/Action_1__ctor_m1775481480_MethodInfo_var);
		NullCheck(L_8);
		ShowOptions_set_resultCallback_m3136348778(L_8, L_11, /*hidden argument*/NULL);
		ShowOptions_t1358843767 * L_12 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		Advertisement_Show_m3789622005(NULL /*static, unused*/, L_6, L_12, /*hidden argument*/NULL);
		goto IL_0052;
	}

IL_0046:
	{
		U3CShowAdsWithResultU3Ec__AnonStorey1_t1779434140 * L_13 = V_0;
		NullCheck(L_13);
		Action_1_t3627374100 * L_14 = L_13->get_adResult_0();
		NullCheck(L_14);
		Action_1_Invoke_m3662000152(L_14, (bool)0, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_0052:
	{
		return;
	}
}
// System.Void UnityAds/<ShowAds>c__AnonStorey0::.ctor()
extern "C"  void U3CShowAdsU3Ec__AnonStorey0__ctor_m2604436093 (U3CShowAdsU3Ec__AnonStorey0_t1075161772 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityAds/<ShowAds>c__AnonStorey0::<>m__0(UnityEngine.Advertisements.ShowResult)
extern "C"  void U3CShowAdsU3Ec__AnonStorey0_U3CU3Em__0_m948133887 (U3CShowAdsU3Ec__AnonStorey0_t1075161772 * __this, int32_t ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CShowAdsU3Ec__AnonStorey0_U3CU3Em__0_m948133887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t258469394 * L_0 = __this->get_adResult_0();
		int32_t L_1 = ___result0;
		NullCheck(L_0);
		Action_1_Invoke_m2290710958(L_0, L_1, /*hidden argument*/Action_1_Invoke_m2290710958_MethodInfo_var);
		return;
	}
}
// System.Void UnityAds/<ShowAdsWithResult>c__AnonStorey1::.ctor()
extern "C"  void U3CShowAdsWithResultU3Ec__AnonStorey1__ctor_m3151670947 (U3CShowAdsWithResultU3Ec__AnonStorey1_t1779434140 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityAds/<ShowAdsWithResult>c__AnonStorey1::<>m__0(UnityEngine.Advertisements.ShowResult)
extern "C"  void U3CShowAdsWithResultU3Ec__AnonStorey1_U3CU3Em__0_m4294232865 (U3CShowAdsWithResultU3Ec__AnonStorey1_t1779434140 * __this, int32_t ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CShowAdsWithResultU3Ec__AnonStorey1_U3CU3Em__0_m4294232865_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t3627374100 * G_B2_0 = NULL;
	Action_1_t3627374100 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	Action_1_t3627374100 * G_B3_1 = NULL;
	{
		Action_1_t3627374100 * L_0 = __this->get_adResult_0();
		int32_t L_1 = ___result0;
		G_B1_0 = L_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			G_B2_0 = L_0;
			goto IL_0013;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_0014:
	{
		NullCheck(G_B3_1);
		Action_1_Invoke_m3662000152(G_B3_1, (bool)G_B3_0, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
		return;
	}
}
// System.Void WindowTransition::.ctor()
extern "C"  void WindowTransition__ctor_m2841876986 (WindowTransition_t4263452729 * __this, const MethodInfo* method)
{
	{
		__this->set_doAnimateOnLoad_2((bool)1);
		__this->set_doAnimateOnDestroy_3((bool)1);
		__this->set_doFadeInBackLayOnLoad_4((bool)1);
		__this->set_doFadeOutBacklayOnDestroy_5((bool)1);
		__this->set_TransitionDuration_9((0.35f));
		Vector3_t2243707580  L_0 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_initialPosition_10(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WindowTransition::Awake()
extern "C"  void WindowTransition_Awake_m955870837 (WindowTransition_t4263452729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WindowTransition_Awake_m955870837_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_WindowContent_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0010;
		}
	}

IL_0010:
	{
		return;
	}
}
// System.Void WindowTransition::OnWindowAdded()
extern "C"  void WindowTransition_OnWindowAdded_m950623741 (WindowTransition_t4263452729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WindowTransition_OnWindowAdded_m950623741_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_doAnimateOnLoad_2();
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		GameObject_t1756533147 * L_1 = __this->get_WindowContent_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		GameObject_t1756533147 * L_3 = __this->get_WindowContent_8();
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_localPosition_m2533925116(L_4, /*hidden argument*/NULL);
		__this->set_initialPosition_10(L_5);
	}

IL_0032:
	{
		bool L_6 = __this->get_doFadeInBackLayOnLoad_4();
		if (!L_6)
		{
			goto IL_003d;
		}
	}

IL_003d:
	{
		return;
	}
}
// System.Void WindowTransition::OnWindowRemove()
extern "C"  void WindowTransition_OnWindowRemove_m151772023 (WindowTransition_t4263452729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WindowTransition_OnWindowRemove_m151772023_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_doAnimateOnDestroy_3();
		if (!L_0)
		{
			goto IL_003c;
		}
	}
	{
		GameObject_t1756533147 * L_1 = __this->get_WindowContent_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003c;
		}
	}
	{
		bool L_3 = __this->get_doFadeOutBacklayOnDestroy_5();
		if (!L_3)
		{
			goto IL_0027;
		}
	}

IL_0027:
	{
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral4158966261, (0.5f), /*hidden argument*/NULL);
		goto IL_0062;
	}

IL_003c:
	{
		bool L_4 = __this->get_doFadeOutBacklayOnDestroy_5();
		if (!L_4)
		{
			goto IL_005c;
		}
	}
	{
		MonoBehaviour_Invoke_m666563676(__this, _stringLiteral4158966261, (0.5f), /*hidden argument*/NULL);
		goto IL_0062;
	}

IL_005c:
	{
		WindowTransition_OnRemoveTransitionComplete_m3541400377(__this, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void WindowTransition::AnimateWindowOnLoad()
extern "C"  void WindowTransition_AnimateWindowOnLoad_m3850184268 (WindowTransition_t4263452729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WindowTransition_AnimateWindowOnLoad_m3850184268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_doAnimateOnLoad_2();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		GameObject_t1756533147 * L_1 = __this->get_WindowContent_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}

IL_001c:
	{
		WindowTransition_FadeInBackLayOnLoad_m4095212997(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WindowTransition::AnimateWindowOnDestroy()
extern "C"  void WindowTransition_AnimateWindowOnDestroy_m3935909200 (WindowTransition_t4263452729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WindowTransition_AnimateWindowOnDestroy_m3935909200_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_doAnimateOnDestroy_3();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		GameObject_t1756533147 * L_1 = __this->get_WindowContent_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}

IL_001c:
	{
		WindowTransition_FadeOutBacklayOnDestroy_m617660916(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WindowTransition::FadeInBackLayOnLoad()
extern "C"  void WindowTransition_FadeInBackLayOnLoad_m4095212997 (WindowTransition_t4263452729 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_doFadeInBackLayOnLoad_4();
		if (!L_0)
		{
			goto IL_000b;
		}
	}

IL_000b:
	{
		return;
	}
}
// System.Void WindowTransition::FadeOutBacklayOnDestroy()
extern "C"  void WindowTransition_FadeOutBacklayOnDestroy_m617660916 (WindowTransition_t4263452729 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_doFadeOutBacklayOnDestroy_5();
		if (!L_0)
		{
			goto IL_000b;
		}
	}

IL_000b:
	{
		return;
	}
}
// System.Void WindowTransition::OnOpacityUpdate(System.Single)
extern "C"  void WindowTransition_OnOpacityUpdate_m1802184534 (WindowTransition_t4263452729 * __this, float ___Opacity0, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t2020392075  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Color_t2020392075  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Image_t2042527209 * L_0 = __this->get_BackLay_7();
		Image_t2042527209 * L_1 = __this->get_BackLay_7();
		NullCheck(L_1);
		Color_t2020392075  L_2 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_1);
		V_0 = L_2;
		float L_3 = (&V_0)->get_r_0();
		Image_t2042527209 * L_4 = __this->get_BackLay_7();
		NullCheck(L_4);
		Color_t2020392075  L_5 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_4);
		V_1 = L_5;
		float L_6 = (&V_1)->get_g_1();
		Image_t2042527209 * L_7 = __this->get_BackLay_7();
		NullCheck(L_7);
		Color_t2020392075  L_8 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_7);
		V_2 = L_8;
		float L_9 = (&V_2)->get_b_2();
		float L_10 = ___Opacity0;
		Color_t2020392075  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Color__ctor_m1909920690(&L_11, L_3, L_6, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_11);
		return;
	}
}
// System.Void WindowTransition::OnRemoveTransitionComplete()
extern "C"  void WindowTransition_OnRemoveTransitionComplete_m3541400377 (WindowTransition_t4263452729 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WindowTransition_OnRemoveTransitionComplete_m3541400377_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_DestroyOnFinish_6();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_003d;
	}

IL_001b:
	{
		GameObject_t1756533147 * L_2 = __this->get_WindowContent_8();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139(L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = __this->get_initialPosition_10();
		NullCheck(L_3);
		Transform_set_localPosition_m1026930133(L_3, L_4, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, (bool)0, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
