﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// GamePlay
struct GamePlay_t1646832012;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t1389513207;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.UI.Image
struct Image_t2042527209;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GamePlay
struct  GamePlay_t1646832012  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text GamePlay::txtScore
	Text_t356221433 * ___txtScore_3;
	// UnityEngine.UI.Text GamePlay::ballCount
	Text_t356221433 * ___ballCount_4;
	// UnityEngine.GameObject GamePlay::gameOverPanel
	GameObject_t1756533147 * ___gameOverPanel_5;
	// UnityEngine.AudioClip GamePlay::SuccessHit
	AudioClip_t1932558630 * ___SuccessHit_6;
	// UnityEngine.AudioClip GamePlay::RingHit
	AudioClip_t1932558630 * ___RingHit_7;
	// System.Collections.Generic.List`1<UnityEngine.Color> GamePlay::BGColors
	List_1_t1389513207 * ___BGColors_8;
	// System.Boolean GamePlay::isRescued
	bool ___isRescued_9;
	// System.Int32 GamePlay::targetScore
	int32_t ___targetScore_10;
	// UnityEngine.GameObject GamePlay::HowToPanel
	GameObject_t1756533147 * ___HowToPanel_11;
	// UnityEngine.GameObject GamePlay::ring
	GameObject_t1756533147 * ___ring_12;
	// UnityEngine.SpriteRenderer GamePlay::background
	SpriteRenderer_t1209076198 * ___background_13;
	// UnityEngine.SpriteRenderer GamePlay::cannonBall
	SpriteRenderer_t1209076198 * ___cannonBall_14;
	// UnityEngine.UI.Image GamePlay::targetBack
	Image_t2042527209 * ___targetBack_15;
	// UnityEngine.UI.Image GamePlay::targetFront
	Image_t2042527209 * ___targetFront_16;
	// System.Int32 GamePlay::score
	int32_t ___score_17;
	// System.Boolean GamePlay::isGamePlay
	bool ___isGamePlay_18;
	// System.Int32 GamePlay::balls
	int32_t ___balls_19;

public:
	inline static int32_t get_offset_of_txtScore_3() { return static_cast<int32_t>(offsetof(GamePlay_t1646832012, ___txtScore_3)); }
	inline Text_t356221433 * get_txtScore_3() const { return ___txtScore_3; }
	inline Text_t356221433 ** get_address_of_txtScore_3() { return &___txtScore_3; }
	inline void set_txtScore_3(Text_t356221433 * value)
	{
		___txtScore_3 = value;
		Il2CppCodeGenWriteBarrier(&___txtScore_3, value);
	}

	inline static int32_t get_offset_of_ballCount_4() { return static_cast<int32_t>(offsetof(GamePlay_t1646832012, ___ballCount_4)); }
	inline Text_t356221433 * get_ballCount_4() const { return ___ballCount_4; }
	inline Text_t356221433 ** get_address_of_ballCount_4() { return &___ballCount_4; }
	inline void set_ballCount_4(Text_t356221433 * value)
	{
		___ballCount_4 = value;
		Il2CppCodeGenWriteBarrier(&___ballCount_4, value);
	}

	inline static int32_t get_offset_of_gameOverPanel_5() { return static_cast<int32_t>(offsetof(GamePlay_t1646832012, ___gameOverPanel_5)); }
	inline GameObject_t1756533147 * get_gameOverPanel_5() const { return ___gameOverPanel_5; }
	inline GameObject_t1756533147 ** get_address_of_gameOverPanel_5() { return &___gameOverPanel_5; }
	inline void set_gameOverPanel_5(GameObject_t1756533147 * value)
	{
		___gameOverPanel_5 = value;
		Il2CppCodeGenWriteBarrier(&___gameOverPanel_5, value);
	}

	inline static int32_t get_offset_of_SuccessHit_6() { return static_cast<int32_t>(offsetof(GamePlay_t1646832012, ___SuccessHit_6)); }
	inline AudioClip_t1932558630 * get_SuccessHit_6() const { return ___SuccessHit_6; }
	inline AudioClip_t1932558630 ** get_address_of_SuccessHit_6() { return &___SuccessHit_6; }
	inline void set_SuccessHit_6(AudioClip_t1932558630 * value)
	{
		___SuccessHit_6 = value;
		Il2CppCodeGenWriteBarrier(&___SuccessHit_6, value);
	}

	inline static int32_t get_offset_of_RingHit_7() { return static_cast<int32_t>(offsetof(GamePlay_t1646832012, ___RingHit_7)); }
	inline AudioClip_t1932558630 * get_RingHit_7() const { return ___RingHit_7; }
	inline AudioClip_t1932558630 ** get_address_of_RingHit_7() { return &___RingHit_7; }
	inline void set_RingHit_7(AudioClip_t1932558630 * value)
	{
		___RingHit_7 = value;
		Il2CppCodeGenWriteBarrier(&___RingHit_7, value);
	}

	inline static int32_t get_offset_of_BGColors_8() { return static_cast<int32_t>(offsetof(GamePlay_t1646832012, ___BGColors_8)); }
	inline List_1_t1389513207 * get_BGColors_8() const { return ___BGColors_8; }
	inline List_1_t1389513207 ** get_address_of_BGColors_8() { return &___BGColors_8; }
	inline void set_BGColors_8(List_1_t1389513207 * value)
	{
		___BGColors_8 = value;
		Il2CppCodeGenWriteBarrier(&___BGColors_8, value);
	}

	inline static int32_t get_offset_of_isRescued_9() { return static_cast<int32_t>(offsetof(GamePlay_t1646832012, ___isRescued_9)); }
	inline bool get_isRescued_9() const { return ___isRescued_9; }
	inline bool* get_address_of_isRescued_9() { return &___isRescued_9; }
	inline void set_isRescued_9(bool value)
	{
		___isRescued_9 = value;
	}

	inline static int32_t get_offset_of_targetScore_10() { return static_cast<int32_t>(offsetof(GamePlay_t1646832012, ___targetScore_10)); }
	inline int32_t get_targetScore_10() const { return ___targetScore_10; }
	inline int32_t* get_address_of_targetScore_10() { return &___targetScore_10; }
	inline void set_targetScore_10(int32_t value)
	{
		___targetScore_10 = value;
	}

	inline static int32_t get_offset_of_HowToPanel_11() { return static_cast<int32_t>(offsetof(GamePlay_t1646832012, ___HowToPanel_11)); }
	inline GameObject_t1756533147 * get_HowToPanel_11() const { return ___HowToPanel_11; }
	inline GameObject_t1756533147 ** get_address_of_HowToPanel_11() { return &___HowToPanel_11; }
	inline void set_HowToPanel_11(GameObject_t1756533147 * value)
	{
		___HowToPanel_11 = value;
		Il2CppCodeGenWriteBarrier(&___HowToPanel_11, value);
	}

	inline static int32_t get_offset_of_ring_12() { return static_cast<int32_t>(offsetof(GamePlay_t1646832012, ___ring_12)); }
	inline GameObject_t1756533147 * get_ring_12() const { return ___ring_12; }
	inline GameObject_t1756533147 ** get_address_of_ring_12() { return &___ring_12; }
	inline void set_ring_12(GameObject_t1756533147 * value)
	{
		___ring_12 = value;
		Il2CppCodeGenWriteBarrier(&___ring_12, value);
	}

	inline static int32_t get_offset_of_background_13() { return static_cast<int32_t>(offsetof(GamePlay_t1646832012, ___background_13)); }
	inline SpriteRenderer_t1209076198 * get_background_13() const { return ___background_13; }
	inline SpriteRenderer_t1209076198 ** get_address_of_background_13() { return &___background_13; }
	inline void set_background_13(SpriteRenderer_t1209076198 * value)
	{
		___background_13 = value;
		Il2CppCodeGenWriteBarrier(&___background_13, value);
	}

	inline static int32_t get_offset_of_cannonBall_14() { return static_cast<int32_t>(offsetof(GamePlay_t1646832012, ___cannonBall_14)); }
	inline SpriteRenderer_t1209076198 * get_cannonBall_14() const { return ___cannonBall_14; }
	inline SpriteRenderer_t1209076198 ** get_address_of_cannonBall_14() { return &___cannonBall_14; }
	inline void set_cannonBall_14(SpriteRenderer_t1209076198 * value)
	{
		___cannonBall_14 = value;
		Il2CppCodeGenWriteBarrier(&___cannonBall_14, value);
	}

	inline static int32_t get_offset_of_targetBack_15() { return static_cast<int32_t>(offsetof(GamePlay_t1646832012, ___targetBack_15)); }
	inline Image_t2042527209 * get_targetBack_15() const { return ___targetBack_15; }
	inline Image_t2042527209 ** get_address_of_targetBack_15() { return &___targetBack_15; }
	inline void set_targetBack_15(Image_t2042527209 * value)
	{
		___targetBack_15 = value;
		Il2CppCodeGenWriteBarrier(&___targetBack_15, value);
	}

	inline static int32_t get_offset_of_targetFront_16() { return static_cast<int32_t>(offsetof(GamePlay_t1646832012, ___targetFront_16)); }
	inline Image_t2042527209 * get_targetFront_16() const { return ___targetFront_16; }
	inline Image_t2042527209 ** get_address_of_targetFront_16() { return &___targetFront_16; }
	inline void set_targetFront_16(Image_t2042527209 * value)
	{
		___targetFront_16 = value;
		Il2CppCodeGenWriteBarrier(&___targetFront_16, value);
	}

	inline static int32_t get_offset_of_score_17() { return static_cast<int32_t>(offsetof(GamePlay_t1646832012, ___score_17)); }
	inline int32_t get_score_17() const { return ___score_17; }
	inline int32_t* get_address_of_score_17() { return &___score_17; }
	inline void set_score_17(int32_t value)
	{
		___score_17 = value;
	}

	inline static int32_t get_offset_of_isGamePlay_18() { return static_cast<int32_t>(offsetof(GamePlay_t1646832012, ___isGamePlay_18)); }
	inline bool get_isGamePlay_18() const { return ___isGamePlay_18; }
	inline bool* get_address_of_isGamePlay_18() { return &___isGamePlay_18; }
	inline void set_isGamePlay_18(bool value)
	{
		___isGamePlay_18 = value;
	}

	inline static int32_t get_offset_of_balls_19() { return static_cast<int32_t>(offsetof(GamePlay_t1646832012, ___balls_19)); }
	inline int32_t get_balls_19() const { return ___balls_19; }
	inline int32_t* get_address_of_balls_19() { return &___balls_19; }
	inline void set_balls_19(int32_t value)
	{
		___balls_19 = value;
	}
};

struct GamePlay_t1646832012_StaticFields
{
public:
	// GamePlay GamePlay::instance
	GamePlay_t1646832012 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(GamePlay_t1646832012_StaticFields, ___instance_2)); }
	inline GamePlay_t1646832012 * get_instance_2() const { return ___instance_2; }
	inline GamePlay_t1646832012 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(GamePlay_t1646832012 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
