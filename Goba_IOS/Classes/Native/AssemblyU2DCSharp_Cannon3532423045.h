﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Cannon
struct Cannon_t3532423045;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Cannon
struct  Cannon_t3532423045  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Cannon::CannonPoint
	GameObject_t1756533147 * ___CannonPoint_3;
	// UnityEngine.GameObject Cannon::FiringBall
	GameObject_t1756533147 * ___FiringBall_4;
	// System.Single Cannon::travelSpeed
	float ___travelSpeed_5;
	// System.Single Cannon::rotateSpeed
	float ___rotateSpeed_6;

public:
	inline static int32_t get_offset_of_CannonPoint_3() { return static_cast<int32_t>(offsetof(Cannon_t3532423045, ___CannonPoint_3)); }
	inline GameObject_t1756533147 * get_CannonPoint_3() const { return ___CannonPoint_3; }
	inline GameObject_t1756533147 ** get_address_of_CannonPoint_3() { return &___CannonPoint_3; }
	inline void set_CannonPoint_3(GameObject_t1756533147 * value)
	{
		___CannonPoint_3 = value;
		Il2CppCodeGenWriteBarrier(&___CannonPoint_3, value);
	}

	inline static int32_t get_offset_of_FiringBall_4() { return static_cast<int32_t>(offsetof(Cannon_t3532423045, ___FiringBall_4)); }
	inline GameObject_t1756533147 * get_FiringBall_4() const { return ___FiringBall_4; }
	inline GameObject_t1756533147 ** get_address_of_FiringBall_4() { return &___FiringBall_4; }
	inline void set_FiringBall_4(GameObject_t1756533147 * value)
	{
		___FiringBall_4 = value;
		Il2CppCodeGenWriteBarrier(&___FiringBall_4, value);
	}

	inline static int32_t get_offset_of_travelSpeed_5() { return static_cast<int32_t>(offsetof(Cannon_t3532423045, ___travelSpeed_5)); }
	inline float get_travelSpeed_5() const { return ___travelSpeed_5; }
	inline float* get_address_of_travelSpeed_5() { return &___travelSpeed_5; }
	inline void set_travelSpeed_5(float value)
	{
		___travelSpeed_5 = value;
	}

	inline static int32_t get_offset_of_rotateSpeed_6() { return static_cast<int32_t>(offsetof(Cannon_t3532423045, ___rotateSpeed_6)); }
	inline float get_rotateSpeed_6() const { return ___rotateSpeed_6; }
	inline float* get_address_of_rotateSpeed_6() { return &___rotateSpeed_6; }
	inline void set_rotateSpeed_6(float value)
	{
		___rotateSpeed_6 = value;
	}
};

struct Cannon_t3532423045_StaticFields
{
public:
	// Cannon Cannon::instance
	Cannon_t3532423045 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Cannon_t3532423045_StaticFields, ___instance_2)); }
	inline Cannon_t3532423045 * get_instance_2() const { return ___instance_2; }
	inline Cannon_t3532423045 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(Cannon_t3532423045 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
