﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Action`1<System.Boolean>
struct Action_1_t3627374100;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityAds/<ShowAdsWithResult>c__AnonStorey1
struct  U3CShowAdsWithResultU3Ec__AnonStorey1_t1779434140  : public Il2CppObject
{
public:
	// System.Action`1<System.Boolean> UnityAds/<ShowAdsWithResult>c__AnonStorey1::adResult
	Action_1_t3627374100 * ___adResult_0;

public:
	inline static int32_t get_offset_of_adResult_0() { return static_cast<int32_t>(offsetof(U3CShowAdsWithResultU3Ec__AnonStorey1_t1779434140, ___adResult_0)); }
	inline Action_1_t3627374100 * get_adResult_0() const { return ___adResult_0; }
	inline Action_1_t3627374100 ** get_address_of_adResult_0() { return &___adResult_0; }
	inline void set_adResult_0(Action_1_t3627374100 * value)
	{
		___adResult_0 = value;
		Il2CppCodeGenWriteBarrier(&___adResult_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
