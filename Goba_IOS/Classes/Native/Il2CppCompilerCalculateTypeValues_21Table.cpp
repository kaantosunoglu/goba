﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JArray1483708661.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JConstructor3123819808.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JContainer3538280255.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JObject278519297.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JObject_U3CGe1106381237.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JProperty2956441399.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JProperty_JPro572931806.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JProperty_JPr4283205876.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JPropertyKeye4094279377.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JRaw3423748388.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonLoadSettin947661933.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JToken2552644013.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JToken_LineIn3095839586.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JTokenReader3330885370.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JTokenType1307827213.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JTokenWriter3631426868.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JValue300956845.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_Arra4215975755.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_Arra2432514147.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_Arra4151631179.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_Arra1314312291.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_Arra1898492193.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_Arra2480764066.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_Fiel1865846940.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_Fiel3808103778.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_Fiel3948372884.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_Fiel1810058818.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_Fiel1536628058.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_JPat2124135247.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_PathF337893041.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_Quer3689602974.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_Quer4258759568.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_Comp1978404879.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_Bool3023864960.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_Query607959964.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_Quer2844300390.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_Scan4136742319.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Linq_JsonPath_Scan3131425971.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Converters_BinaryC3398462695.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Converters_BsonObj2711362450.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Converters_KeyValue575953290.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Converters_RegexCo3589249209.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonBinaryTyp2009671055.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonBinaryWri2169798354.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonObjectId1719565518.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonToken3582361217.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonObject2841166125.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonArray2790707601.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonValue309805921.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonString3501425379.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonBinary665434999.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonRegex3386721543.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonProperty1491061775.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonType2055433366.h"
#include "Newtonsoft_Json_Newtonsoft_Json_Bson_BsonWriter101776461.h"
#include "Newtonsoft_Json_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "Newtonsoft_Json_U3CPrivateImplementationDetailsU3E2497549767.h"
#include "Newtonsoft_Json_U3CPrivateImplementationDetailsU3E1381760538.h"
#include "Newtonsoft_Json_U3CPrivateImplementationDetailsU3E1381760540.h"
#include "Newtonsoft_Json_U3CPrivateImplementationDetailsU3E_978476019.h"
#include "Newtonsoft_Json_U3CPrivateImplementationDetailsU3E3707359368.h"
#include "UnityEngine_Advertisements_U3CModuleU3E3783534214.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme3914199195.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme1597425191.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme1591511100.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme2698643213.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme2698643214.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme2947320436.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme4146538406.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme1030944433.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertisemen694166726.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme1391478423.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertisemen457710789.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertisemen280717056.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme1358843767.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertisemen456670012.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme1381805072.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme3070849459.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme3667730448.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme1837649965.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertisemen568704329.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme3781135187.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme1646290531.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme2056116968.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme3399460664.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertisement88170910.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertisemen649819413.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme1627639653.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertisemen150098462.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme2914334489.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme3795127723.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme3795127722.h"
#include "UnityEngine_Analytics_U3CModuleU3E3783534214.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt2191537572.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt1068911718.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka1304606600.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka2256174789.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (JArray_t1483708661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2101[1] = 
{
	JArray_t1483708661::get_offset_of__values_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (JConstructor_t3123819808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2102[2] = 
{
	JConstructor_t3123819808::get_offset_of__name_15(),
	JConstructor_t3123819808::get_offset_of__values_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (JContainer_t3538280255), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[2] = 
{
	JContainer_t3538280255::get_offset_of__syncRoot_13(),
	JContainer_t3538280255::get_offset_of__busy_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2104[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (JObject_t278519297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[2] = 
{
	JObject_t278519297::get_offset_of__properties_15(),
	JObject_t278519297::get_offset_of_PropertyChanged_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (U3CGetEnumeratorU3Ed__55_t1106381237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2106[4] = 
{
	U3CGetEnumeratorU3Ed__55_t1106381237::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__55_t1106381237::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__55_t1106381237::get_offset_of_U3CU3E4__this_2(),
	U3CGetEnumeratorU3Ed__55_t1106381237::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (JProperty_t2956441399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2107[2] = 
{
	JProperty_t2956441399::get_offset_of__content_15(),
	JProperty_t2956441399::get_offset_of__name_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (JPropertyList_t572931806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2108[1] = 
{
	JPropertyList_t572931806::get_offset_of__token_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (U3CGetEnumeratorU3Ed__1_t4283205876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2109[3] = 
{
	U3CGetEnumeratorU3Ed__1_t4283205876::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__1_t4283205876::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__1_t4283205876::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (JPropertyKeyedCollection_t4094279377), -1, sizeof(JPropertyKeyedCollection_t4094279377_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2110[2] = 
{
	JPropertyKeyedCollection_t4094279377_StaticFields::get_offset_of_Comparer_2(),
	JPropertyKeyedCollection_t4094279377::get_offset_of__dictionary_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (JRaw_t3423748388), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (JsonLoadSettings_t947661933), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2112[2] = 
{
	JsonLoadSettings_t947661933::get_offset_of__commentHandling_0(),
	JsonLoadSettings_t947661933::get_offset_of__lineInfoHandling_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (JToken_t2552644013), -1, sizeof(JToken_t2552644013_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2113[13] = 
{
	JToken_t2552644013::get_offset_of__parent_0(),
	JToken_t2552644013::get_offset_of__previous_1(),
	JToken_t2552644013::get_offset_of__next_2(),
	JToken_t2552644013::get_offset_of__annotations_3(),
	JToken_t2552644013_StaticFields::get_offset_of_BooleanTypes_4(),
	JToken_t2552644013_StaticFields::get_offset_of_NumberTypes_5(),
	JToken_t2552644013_StaticFields::get_offset_of_StringTypes_6(),
	JToken_t2552644013_StaticFields::get_offset_of_GuidTypes_7(),
	JToken_t2552644013_StaticFields::get_offset_of_TimeSpanTypes_8(),
	JToken_t2552644013_StaticFields::get_offset_of_UriTypes_9(),
	JToken_t2552644013_StaticFields::get_offset_of_CharTypes_10(),
	JToken_t2552644013_StaticFields::get_offset_of_DateTimeTypes_11(),
	JToken_t2552644013_StaticFields::get_offset_of_BytesTypes_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (LineInfoAnnotation_t3095839586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[2] = 
{
	LineInfoAnnotation_t3095839586::get_offset_of_LineNumber_0(),
	LineInfoAnnotation_t3095839586::get_offset_of_LinePosition_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (JTokenReader_t3330885370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2115[4] = 
{
	JTokenReader_t3330885370::get_offset_of__root_15(),
	JTokenReader_t3330885370::get_offset_of__initialPath_16(),
	JTokenReader_t3330885370::get_offset_of__parent_17(),
	JTokenReader_t3330885370::get_offset_of__current_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (JTokenType_t1307827213)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2116[19] = 
{
	JTokenType_t1307827213::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (JTokenWriter_t3631426868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2117[4] = 
{
	JTokenWriter_t3631426868::get_offset_of__token_13(),
	JTokenWriter_t3631426868::get_offset_of__parent_14(),
	JTokenWriter_t3631426868::get_offset_of__value_15(),
	JTokenWriter_t3631426868::get_offset_of__current_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (JValue_t300956845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2118[2] = 
{
	JValue_t300956845::get_offset_of__valueType_13(),
	JValue_t300956845::get_offset_of__value_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (ArrayIndexFilter_t4215975755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2119[1] = 
{
	ArrayIndexFilter_t4215975755::get_offset_of_U3CIndexU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (U3CExecuteFilterU3Ed__4_t2432514147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2120[11] = 
{
	U3CExecuteFilterU3Ed__4_t2432514147::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_t2432514147::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_t2432514147::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_t2432514147::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_t2432514147::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_t2432514147::get_offset_of_U3CU3E4__this_5(),
	U3CExecuteFilterU3Ed__4_t2432514147::get_offset_of_errorWhenNoMatch_6(),
	U3CExecuteFilterU3Ed__4_t2432514147::get_offset_of_U3CU3E3__errorWhenNoMatch_7(),
	U3CExecuteFilterU3Ed__4_t2432514147::get_offset_of_U3CtU3E5__1_8(),
	U3CExecuteFilterU3Ed__4_t2432514147::get_offset_of_U3CU3E7__wrap1_9(),
	U3CExecuteFilterU3Ed__4_t2432514147::get_offset_of_U3CU3E7__wrap2_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (ArrayMultipleIndexFilter_t4151631179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2121[1] = 
{
	ArrayMultipleIndexFilter_t4151631179::get_offset_of_U3CIndexesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (U3CExecuteFilterU3Ed__4_t1314312291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2122[11] = 
{
	U3CExecuteFilterU3Ed__4_t1314312291::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_t1314312291::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_t1314312291::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_t1314312291::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_t1314312291::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_t1314312291::get_offset_of_U3CU3E4__this_5(),
	U3CExecuteFilterU3Ed__4_t1314312291::get_offset_of_U3CtU3E5__1_6(),
	U3CExecuteFilterU3Ed__4_t1314312291::get_offset_of_errorWhenNoMatch_7(),
	U3CExecuteFilterU3Ed__4_t1314312291::get_offset_of_U3CU3E3__errorWhenNoMatch_8(),
	U3CExecuteFilterU3Ed__4_t1314312291::get_offset_of_U3CU3E7__wrap1_9(),
	U3CExecuteFilterU3Ed__4_t1314312291::get_offset_of_U3CU3E7__wrap2_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (ArraySliceFilter_t1898492193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2123[3] = 
{
	ArraySliceFilter_t1898492193::get_offset_of_U3CStartU3Ek__BackingField_0(),
	ArraySliceFilter_t1898492193::get_offset_of_U3CEndU3Ek__BackingField_1(),
	ArraySliceFilter_t1898492193::get_offset_of_U3CStepU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (U3CExecuteFilterU3Ed__12_t2480764066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2124[15] = 
{
	U3CExecuteFilterU3Ed__12_t2480764066::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__12_t2480764066::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__12_t2480764066::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__12_t2480764066::get_offset_of_U3CU3E4__this_3(),
	U3CExecuteFilterU3Ed__12_t2480764066::get_offset_of_current_4(),
	U3CExecuteFilterU3Ed__12_t2480764066::get_offset_of_U3CU3E3__current_5(),
	U3CExecuteFilterU3Ed__12_t2480764066::get_offset_of_U3CaU3E5__1_6(),
	U3CExecuteFilterU3Ed__12_t2480764066::get_offset_of_U3CiU3E5__2_7(),
	U3CExecuteFilterU3Ed__12_t2480764066::get_offset_of_U3CstepCountU3E5__3_8(),
	U3CExecuteFilterU3Ed__12_t2480764066::get_offset_of_U3CstopIndexU3E5__4_9(),
	U3CExecuteFilterU3Ed__12_t2480764066::get_offset_of_U3CpositiveStepU3E5__5_10(),
	U3CExecuteFilterU3Ed__12_t2480764066::get_offset_of_errorWhenNoMatch_11(),
	U3CExecuteFilterU3Ed__12_t2480764066::get_offset_of_U3CU3E3__errorWhenNoMatch_12(),
	U3CExecuteFilterU3Ed__12_t2480764066::get_offset_of_U3CtU3E5__6_13(),
	U3CExecuteFilterU3Ed__12_t2480764066::get_offset_of_U3CU3E7__wrap1_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (FieldFilter_t1865846940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[1] = 
{
	FieldFilter_t1865846940::get_offset_of_U3CNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (U3CExecuteFilterU3Ed__4_t3808103778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[12] = 
{
	U3CExecuteFilterU3Ed__4_t3808103778::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_t3808103778::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_t3808103778::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_t3808103778::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_t3808103778::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_t3808103778::get_offset_of_U3CU3E4__this_5(),
	U3CExecuteFilterU3Ed__4_t3808103778::get_offset_of_errorWhenNoMatch_6(),
	U3CExecuteFilterU3Ed__4_t3808103778::get_offset_of_U3CU3E3__errorWhenNoMatch_7(),
	U3CExecuteFilterU3Ed__4_t3808103778::get_offset_of_U3CoU3E5__1_8(),
	U3CExecuteFilterU3Ed__4_t3808103778::get_offset_of_U3CtU3E5__2_9(),
	U3CExecuteFilterU3Ed__4_t3808103778::get_offset_of_U3CU3E7__wrap1_10(),
	U3CExecuteFilterU3Ed__4_t3808103778::get_offset_of_U3CU3E7__wrap2_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (FieldMultipleFilter_t3948372884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2127[1] = 
{
	FieldMultipleFilter_t3948372884::get_offset_of_U3CNamesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (U3CU3Ec_t1810058818), -1, sizeof(U3CU3Ec_t1810058818_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2128[2] = 
{
	U3CU3Ec_t1810058818_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1810058818_StaticFields::get_offset_of_U3CU3E9__4_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (U3CExecuteFilterU3Ed__4_t1536628058), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2129[13] = 
{
	U3CExecuteFilterU3Ed__4_t1536628058::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_t1536628058::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_t1536628058::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_t1536628058::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_t1536628058::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_t1536628058::get_offset_of_U3CU3E4__this_5(),
	U3CExecuteFilterU3Ed__4_t1536628058::get_offset_of_U3CoU3E5__1_6(),
	U3CExecuteFilterU3Ed__4_t1536628058::get_offset_of_errorWhenNoMatch_7(),
	U3CExecuteFilterU3Ed__4_t1536628058::get_offset_of_U3CU3E3__errorWhenNoMatch_8(),
	U3CExecuteFilterU3Ed__4_t1536628058::get_offset_of_U3CnameU3E5__2_9(),
	U3CExecuteFilterU3Ed__4_t1536628058::get_offset_of_U3CtU3E5__3_10(),
	U3CExecuteFilterU3Ed__4_t1536628058::get_offset_of_U3CU3E7__wrap1_11(),
	U3CExecuteFilterU3Ed__4_t1536628058::get_offset_of_U3CU3E7__wrap2_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (JPath_t2124135247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2130[3] = 
{
	JPath_t2124135247::get_offset_of__expression_0(),
	JPath_t2124135247::get_offset_of_U3CFiltersU3Ek__BackingField_1(),
	JPath_t2124135247::get_offset_of__currentIndex_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (PathFilter_t337893041), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (QueryOperator_t3689602974)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2132[11] = 
{
	QueryOperator_t3689602974::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (QueryExpression_t4258759568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[1] = 
{
	QueryExpression_t4258759568::get_offset_of_U3COperatorU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (CompositeExpression_t1978404879), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2134[1] = 
{
	CompositeExpression_t1978404879::get_offset_of_U3CExpressionsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (BooleanQueryExpression_t3023864960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[2] = 
{
	BooleanQueryExpression_t3023864960::get_offset_of_U3CPathU3Ek__BackingField_1(),
	BooleanQueryExpression_t3023864960::get_offset_of_U3CValueU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (QueryFilter_t607959964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2136[1] = 
{
	QueryFilter_t607959964::get_offset_of_U3CExpressionU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (U3CExecuteFilterU3Ed__4_t2844300390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2137[8] = 
{
	U3CExecuteFilterU3Ed__4_t2844300390::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_t2844300390::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_t2844300390::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_t2844300390::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_t2844300390::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_t2844300390::get_offset_of_U3CU3E4__this_5(),
	U3CExecuteFilterU3Ed__4_t2844300390::get_offset_of_U3CU3E7__wrap1_6(),
	U3CExecuteFilterU3Ed__4_t2844300390::get_offset_of_U3CU3E7__wrap2_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (ScanFilter_t4136742319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[1] = 
{
	ScanFilter_t4136742319::get_offset_of_U3CNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (U3CExecuteFilterU3Ed__4_t3131425971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2139[9] = 
{
	U3CExecuteFilterU3Ed__4_t3131425971::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_t3131425971::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_t3131425971::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_t3131425971::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_t3131425971::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_t3131425971::get_offset_of_U3CU3E4__this_5(),
	U3CExecuteFilterU3Ed__4_t3131425971::get_offset_of_U3CrootU3E5__1_6(),
	U3CExecuteFilterU3Ed__4_t3131425971::get_offset_of_U3CvalueU3E5__2_7(),
	U3CExecuteFilterU3Ed__4_t3131425971::get_offset_of_U3CU3E7__wrap1_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (BinaryConverter_t3398462695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2140[1] = 
{
	BinaryConverter_t3398462695::get_offset_of__reflectionObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (BsonObjectIdConverter_t2711362450), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (KeyValuePairConverter_t575953290), -1, sizeof(KeyValuePairConverter_t575953290_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2142[1] = 
{
	KeyValuePairConverter_t575953290_StaticFields::get_offset_of_ReflectionObjectPerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (RegexConverter_t3589249209), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (BsonBinaryType_t2009671055)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2144[8] = 
{
	BsonBinaryType_t2009671055::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (BsonBinaryWriter_t2169798354), -1, sizeof(BsonBinaryWriter_t2169798354_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2145[4] = 
{
	BsonBinaryWriter_t2169798354_StaticFields::get_offset_of_Encoding_0(),
	BsonBinaryWriter_t2169798354::get_offset_of__writer_1(),
	BsonBinaryWriter_t2169798354::get_offset_of__largeByteBuffer_2(),
	BsonBinaryWriter_t2169798354::get_offset_of_U3CDateTimeKindHandlingU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (BsonObjectId_t1719565518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2146[1] = 
{
	BsonObjectId_t1719565518::get_offset_of_U3CValueU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (BsonToken_t3582361217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2147[2] = 
{
	BsonToken_t3582361217::get_offset_of_U3CParentU3Ek__BackingField_0(),
	BsonToken_t3582361217::get_offset_of_U3CCalculatedSizeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (BsonObject_t2841166125), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2148[1] = 
{
	BsonObject_t2841166125::get_offset_of__children_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (BsonArray_t2790707601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2149[1] = 
{
	BsonArray_t2790707601::get_offset_of__children_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (BsonValue_t309805921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2150[2] = 
{
	BsonValue_t309805921::get_offset_of__value_2(),
	BsonValue_t309805921::get_offset_of__type_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (BsonString_t3501425379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2151[2] = 
{
	BsonString_t3501425379::get_offset_of_U3CByteCountU3Ek__BackingField_4(),
	BsonString_t3501425379::get_offset_of_U3CIncludeLengthU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (BsonBinary_t665434999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[1] = 
{
	BsonBinary_t665434999::get_offset_of_U3CBinaryTypeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (BsonRegex_t3386721543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2153[2] = 
{
	BsonRegex_t3386721543::get_offset_of_U3CPatternU3Ek__BackingField_2(),
	BsonRegex_t3386721543::get_offset_of_U3COptionsU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (BsonProperty_t1491061775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2154[2] = 
{
	BsonProperty_t1491061775::get_offset_of_U3CNameU3Ek__BackingField_0(),
	BsonProperty_t1491061775::get_offset_of_U3CValueU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (BsonType_t2055433366)+ sizeof (Il2CppObject), sizeof(int8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2155[21] = 
{
	BsonType_t2055433366::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (BsonWriter_t101776461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2156[4] = 
{
	BsonWriter_t101776461::get_offset_of__writer_13(),
	BsonWriter_t101776461::get_offset_of__root_14(),
	BsonWriter_t101776461::get_offset_of__parent_15(),
	BsonWriter_t101776461::get_offset_of__propertyName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2157[6] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U33DE43C11C7130AF9014115BCDC2584DFE6B50579_0(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_1(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_ADFD2E1C801C825415DD53F4F2F72A13B389313C_2(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_3(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_DD3AEFEADB1CD615F3017763F1568179FEE640B0_4(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_E92B39D8233061927D9ACDE54665E68E7535635A_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (__StaticArrayInitTypeSizeU3D6_t2497549767)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D6_t2497549767 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (__StaticArrayInitTypeSizeU3D10_t1381760538)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D10_t1381760538 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (__StaticArrayInitTypeSizeU3D12_t1381760540)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D12_t1381760540 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (__StaticArrayInitTypeSizeU3D28_t978476019)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D28_t978476019 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (__StaticArrayInitTypeSizeU3D52_t3707359368)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D52_t3707359368 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (U3CModuleU3E_t3783534223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (Advertisement_t3914199195), -1, sizeof(Advertisement_t3914199195_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2164[7] = 
{
	Advertisement_t3914199195_StaticFields::get_offset_of_s_Initialized_0(),
	Advertisement_t3914199195_StaticFields::get_offset_of_s_Platform_1(),
	Advertisement_t3914199195_StaticFields::get_offset_of_s_EditorSupportedPlatform_2(),
	Advertisement_t3914199195_StaticFields::get_offset_of_s_Showing_3(),
	Advertisement_t3914199195_StaticFields::get_offset_of_s_DebugLevel_4(),
	Advertisement_t3914199195_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
	Advertisement_t3914199195_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (DebugLevelInternal_t1597425191)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2165[6] = 
{
	DebugLevelInternal_t1597425191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (DebugLevel_t1591511100)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2166[6] = 
{
	DebugLevel_t1591511100::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (U3CShowU3Ec__AnonStorey0_t2698643213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2167[1] = 
{
	U3CShowU3Ec__AnonStorey0_t2698643213::get_offset_of_showOptions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (U3CShowU3Ec__AnonStorey1_t2698643214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2168[2] = 
{
	U3CShowU3Ec__AnonStorey1_t2698643214::get_offset_of_finishHandler_0(),
	U3CShowU3Ec__AnonStorey1_t2698643214::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (CallbackExecutor_t2947320436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[1] = 
{
	CallbackExecutor_t2947320436::get_offset_of_s_Queue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (ReadyEventArgs_t4146538406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[1] = 
{
	ReadyEventArgs_t4146538406::get_offset_of_U3CplacementIdU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (StartEventArgs_t1030944433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2171[1] = 
{
	StartEventArgs_t1030944433::get_offset_of_U3CplacementIdU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (FinishEventArgs_t694166726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2172[2] = 
{
	FinishEventArgs_t694166726::get_offset_of_U3CplacementIdU3Ek__BackingField_1(),
	FinishEventArgs_t694166726::get_offset_of_U3CshowResultU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (ErrorEventArgs_t1391478423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2173[2] = 
{
	ErrorEventArgs_t1391478423::get_offset_of_U3CerrorU3Ek__BackingField_1(),
	ErrorEventArgs_t1391478423::get_offset_of_U3CmessageU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (MetaData_t457710789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2175[2] = 
{
	MetaData_t457710789::get_offset_of_m_MetaData_0(),
	MetaData_t457710789::get_offset_of_U3CcategoryU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (PlacementState_t280717056)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2176[6] = 
{
	PlacementState_t280717056::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (ShowOptions_t1358843767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2177[2] = 
{
	ShowOptions_t1358843767::get_offset_of_U3CresultCallbackU3Ek__BackingField_0(),
	ShowOptions_t1358843767::get_offset_of_U3CgamerSidU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (ShowResult_t456670012)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2178[4] = 
{
	ShowResult_t456670012::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (UnsupportedPlatform_t1381805072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2179[1] = 
{
	UnsupportedPlatform_t1381805072::get_offset_of_OnFinish_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (Platform_t3070849459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2180[5] = 
{
	Platform_t3070849459::get_offset_of_m_CurrentActivity_1(),
	Platform_t3070849459::get_offset_of_m_UnityAds_2(),
	Platform_t3070849459::get_offset_of_m_CallbackExecutor_3(),
	Platform_t3070849459::get_offset_of_OnStart_4(),
	Platform_t3070849459::get_offset_of_OnFinish_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (Configuration_t3667730448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2181[3] = 
{
	Configuration_t3667730448::get_offset_of_U3CenabledU3Ek__BackingField_0(),
	Configuration_t3667730448::get_offset_of_U3CdefaultPlacementU3Ek__BackingField_1(),
	Configuration_t3667730448::get_offset_of_U3CplacementsU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (Placeholder_t1837649965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2182[6] = 
{
	Placeholder_t1837649965::get_offset_of_m_LandscapeTexture_2(),
	Placeholder_t1837649965::get_offset_of_m_PortraitTexture_3(),
	Placeholder_t1837649965::get_offset_of_m_Showing_4(),
	Placeholder_t1837649965::get_offset_of_m_PlacementId_5(),
	Placeholder_t1837649965::get_offset_of_m_AllowSkip_6(),
	Placeholder_t1837649965::get_offset_of_OnFinish_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (Platform_t568704329), -1, sizeof(Platform_t568704329_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2183[6] = 
{
	Platform_t568704329_StaticFields::get_offset_of_s_BaseUrl_0(),
	Platform_t568704329::get_offset_of_m_DebugMode_1(),
	Platform_t568704329::get_offset_of_m_Configuration_2(),
	Platform_t568704329::get_offset_of_m_Placeholder_3(),
	Platform_t568704329::get_offset_of_OnStart_4(),
	Platform_t568704329::get_offset_of_OnFinish_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (U3CInitializeU3Ec__AnonStorey0_t3781135187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2184[3] = 
{
	U3CInitializeU3Ec__AnonStorey0_t3781135187::get_offset_of_request_0(),
	U3CInitializeU3Ec__AnonStorey0_t3781135187::get_offset_of_gameId_1(),
	U3CInitializeU3Ec__AnonStorey0_t3781135187::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (Platform_t1646290531), -1, sizeof(Platform_t1646290531_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2185[10] = 
{
	Platform_t1646290531_StaticFields::get_offset_of_s_Instance_0(),
	Platform_t1646290531_StaticFields::get_offset_of_s_CallbackExecutor_1(),
	Platform_t1646290531::get_offset_of_OnReady_2(),
	Platform_t1646290531::get_offset_of_OnStart_3(),
	Platform_t1646290531::get_offset_of_OnFinish_4(),
	Platform_t1646290531::get_offset_of_OnError_5(),
	Platform_t1646290531_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_6(),
	Platform_t1646290531_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_7(),
	Platform_t1646290531_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_8(),
	Platform_t1646290531_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (unityAdsReady_t2056116968), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (unityAdsDidError_t3399460664), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (unityAdsDidStart_t88170910), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (unityAdsDidFinish_t649819413), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (U3CUnityAdsReadyU3Ec__AnonStorey0_t1627639653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2190[2] = 
{
	U3CUnityAdsReadyU3Ec__AnonStorey0_t1627639653::get_offset_of_handler_0(),
	U3CUnityAdsReadyU3Ec__AnonStorey0_t1627639653::get_offset_of_placementId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (U3CUnityAdsDidErrorU3Ec__AnonStorey1_t150098462), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2191[3] = 
{
	U3CUnityAdsDidErrorU3Ec__AnonStorey1_t150098462::get_offset_of_handler_0(),
	U3CUnityAdsDidErrorU3Ec__AnonStorey1_t150098462::get_offset_of_rawError_1(),
	U3CUnityAdsDidErrorU3Ec__AnonStorey1_t150098462::get_offset_of_message_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (U3CUnityAdsDidStartU3Ec__AnonStorey2_t2914334489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2192[2] = 
{
	U3CUnityAdsDidStartU3Ec__AnonStorey2_t2914334489::get_offset_of_handler_0(),
	U3CUnityAdsDidStartU3Ec__AnonStorey2_t2914334489::get_offset_of_placementId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (U3CUnityAdsDidFinishU3Ec__AnonStorey3_t3795127723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2193[2] = 
{
	U3CUnityAdsDidFinishU3Ec__AnonStorey3_t3795127723::get_offset_of_handler_0(),
	U3CUnityAdsDidFinishU3Ec__AnonStorey3_t3795127723::get_offset_of_placementId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (U3CUnityAdsDidFinishU3Ec__AnonStorey4_t3795127722), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2194[2] = 
{
	U3CUnityAdsDidFinishU3Ec__AnonStorey4_t3795127722::get_offset_of_showResult_0(),
	U3CUnityAdsDidFinishU3Ec__AnonStorey4_t3795127722::get_offset_of_U3CU3Ef__refU243_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (U3CModuleU3E_t3783534224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (AnalyticsTracker_t2191537572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2196[5] = 
{
	AnalyticsTracker_t2191537572::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t2191537572::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t2191537572::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (Trigger_t1068911718)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2197[8] = 
{
	Trigger_t1068911718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (TrackableProperty_t1304606600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2198[2] = 
{
	0,
	TrackableProperty_t1304606600::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (FieldWithTarget_t2256174789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2199[6] = 
{
	FieldWithTarget_t2256174789::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t2256174789::get_offset_of_m_Target_1(),
	FieldWithTarget_t2256174789::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t2256174789::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t2256174789::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t2256174789::get_offset_of_m_StaticString_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
