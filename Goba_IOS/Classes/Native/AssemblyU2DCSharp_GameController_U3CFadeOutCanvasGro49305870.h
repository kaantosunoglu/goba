﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController/<FadeOutCanvasGroup>c__Iterator1
struct  U3CFadeOutCanvasGroupU3Ec__Iterator1_t49305870  : public Il2CppObject
{
public:
	// System.Single GameController/<FadeOutCanvasGroup>c__Iterator1::<opacity>__1
	float ___U3CopacityU3E__1_0;
	// UnityEngine.CanvasGroup GameController/<FadeOutCanvasGroup>c__Iterator1::canvasGroup
	CanvasGroup_t3296560743 * ___canvasGroup_1;
	// System.Boolean GameController/<FadeOutCanvasGroup>c__Iterator1::disableOnFadeOut
	bool ___disableOnFadeOut_2;
	// System.Object GameController/<FadeOutCanvasGroup>c__Iterator1::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean GameController/<FadeOutCanvasGroup>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 GameController/<FadeOutCanvasGroup>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CopacityU3E__1_0() { return static_cast<int32_t>(offsetof(U3CFadeOutCanvasGroupU3Ec__Iterator1_t49305870, ___U3CopacityU3E__1_0)); }
	inline float get_U3CopacityU3E__1_0() const { return ___U3CopacityU3E__1_0; }
	inline float* get_address_of_U3CopacityU3E__1_0() { return &___U3CopacityU3E__1_0; }
	inline void set_U3CopacityU3E__1_0(float value)
	{
		___U3CopacityU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_canvasGroup_1() { return static_cast<int32_t>(offsetof(U3CFadeOutCanvasGroupU3Ec__Iterator1_t49305870, ___canvasGroup_1)); }
	inline CanvasGroup_t3296560743 * get_canvasGroup_1() const { return ___canvasGroup_1; }
	inline CanvasGroup_t3296560743 ** get_address_of_canvasGroup_1() { return &___canvasGroup_1; }
	inline void set_canvasGroup_1(CanvasGroup_t3296560743 * value)
	{
		___canvasGroup_1 = value;
		Il2CppCodeGenWriteBarrier(&___canvasGroup_1, value);
	}

	inline static int32_t get_offset_of_disableOnFadeOut_2() { return static_cast<int32_t>(offsetof(U3CFadeOutCanvasGroupU3Ec__Iterator1_t49305870, ___disableOnFadeOut_2)); }
	inline bool get_disableOnFadeOut_2() const { return ___disableOnFadeOut_2; }
	inline bool* get_address_of_disableOnFadeOut_2() { return &___disableOnFadeOut_2; }
	inline void set_disableOnFadeOut_2(bool value)
	{
		___disableOnFadeOut_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CFadeOutCanvasGroupU3Ec__Iterator1_t49305870, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CFadeOutCanvasGroupU3Ec__Iterator1_t49305870, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CFadeOutCanvasGroupU3Ec__Iterator1_t49305870, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
