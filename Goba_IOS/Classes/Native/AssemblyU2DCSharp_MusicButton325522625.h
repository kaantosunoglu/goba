﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MusicButton
struct  MusicButton_t325522625  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button MusicButton::btnMusic
	Button_t2872111280 * ___btnMusic_2;
	// UnityEngine.UI.Image MusicButton::btnMusicImage
	Image_t2042527209 * ___btnMusicImage_3;
	// UnityEngine.Sprite MusicButton::musicOnSprite
	Sprite_t309593783 * ___musicOnSprite_4;
	// UnityEngine.Sprite MusicButton::musicOffSprite
	Sprite_t309593783 * ___musicOffSprite_5;

public:
	inline static int32_t get_offset_of_btnMusic_2() { return static_cast<int32_t>(offsetof(MusicButton_t325522625, ___btnMusic_2)); }
	inline Button_t2872111280 * get_btnMusic_2() const { return ___btnMusic_2; }
	inline Button_t2872111280 ** get_address_of_btnMusic_2() { return &___btnMusic_2; }
	inline void set_btnMusic_2(Button_t2872111280 * value)
	{
		___btnMusic_2 = value;
		Il2CppCodeGenWriteBarrier(&___btnMusic_2, value);
	}

	inline static int32_t get_offset_of_btnMusicImage_3() { return static_cast<int32_t>(offsetof(MusicButton_t325522625, ___btnMusicImage_3)); }
	inline Image_t2042527209 * get_btnMusicImage_3() const { return ___btnMusicImage_3; }
	inline Image_t2042527209 ** get_address_of_btnMusicImage_3() { return &___btnMusicImage_3; }
	inline void set_btnMusicImage_3(Image_t2042527209 * value)
	{
		___btnMusicImage_3 = value;
		Il2CppCodeGenWriteBarrier(&___btnMusicImage_3, value);
	}

	inline static int32_t get_offset_of_musicOnSprite_4() { return static_cast<int32_t>(offsetof(MusicButton_t325522625, ___musicOnSprite_4)); }
	inline Sprite_t309593783 * get_musicOnSprite_4() const { return ___musicOnSprite_4; }
	inline Sprite_t309593783 ** get_address_of_musicOnSprite_4() { return &___musicOnSprite_4; }
	inline void set_musicOnSprite_4(Sprite_t309593783 * value)
	{
		___musicOnSprite_4 = value;
		Il2CppCodeGenWriteBarrier(&___musicOnSprite_4, value);
	}

	inline static int32_t get_offset_of_musicOffSprite_5() { return static_cast<int32_t>(offsetof(MusicButton_t325522625, ___musicOffSprite_5)); }
	inline Sprite_t309593783 * get_musicOffSprite_5() const { return ___musicOffSprite_5; }
	inline Sprite_t309593783 ** get_address_of_musicOffSprite_5() { return &___musicOffSprite_5; }
	inline void set_musicOffSprite_5(Sprite_t309593783 * value)
	{
		___musicOffSprite_5 = value;
		Il2CppCodeGenWriteBarrier(&___musicOffSprite_5, value);
	}
};

struct MusicButton_t325522625_StaticFields
{
public:
	// UnityEngine.Events.UnityAction MusicButton::<>f__am$cache0
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(MusicButton_t325522625_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
