﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainScreen
struct  MainScreen_t1502697305  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text MainScreen::txtStatus1
	Text_t356221433 * ___txtStatus1_2;
	// UnityEngine.UI.Text MainScreen::txtStatus2
	Text_t356221433 * ___txtStatus2_3;
	// UnityEngine.UI.Text MainScreen::levelText
	Text_t356221433 * ___levelText_4;
	// UnityEngine.GameObject MainScreen::endOfLevels
	GameObject_t1756533147 * ___endOfLevels_5;
	// System.Int32 MainScreen::interstitialBannerFrequency
	int32_t ___interstitialBannerFrequency_6;

public:
	inline static int32_t get_offset_of_txtStatus1_2() { return static_cast<int32_t>(offsetof(MainScreen_t1502697305, ___txtStatus1_2)); }
	inline Text_t356221433 * get_txtStatus1_2() const { return ___txtStatus1_2; }
	inline Text_t356221433 ** get_address_of_txtStatus1_2() { return &___txtStatus1_2; }
	inline void set_txtStatus1_2(Text_t356221433 * value)
	{
		___txtStatus1_2 = value;
		Il2CppCodeGenWriteBarrier(&___txtStatus1_2, value);
	}

	inline static int32_t get_offset_of_txtStatus2_3() { return static_cast<int32_t>(offsetof(MainScreen_t1502697305, ___txtStatus2_3)); }
	inline Text_t356221433 * get_txtStatus2_3() const { return ___txtStatus2_3; }
	inline Text_t356221433 ** get_address_of_txtStatus2_3() { return &___txtStatus2_3; }
	inline void set_txtStatus2_3(Text_t356221433 * value)
	{
		___txtStatus2_3 = value;
		Il2CppCodeGenWriteBarrier(&___txtStatus2_3, value);
	}

	inline static int32_t get_offset_of_levelText_4() { return static_cast<int32_t>(offsetof(MainScreen_t1502697305, ___levelText_4)); }
	inline Text_t356221433 * get_levelText_4() const { return ___levelText_4; }
	inline Text_t356221433 ** get_address_of_levelText_4() { return &___levelText_4; }
	inline void set_levelText_4(Text_t356221433 * value)
	{
		___levelText_4 = value;
		Il2CppCodeGenWriteBarrier(&___levelText_4, value);
	}

	inline static int32_t get_offset_of_endOfLevels_5() { return static_cast<int32_t>(offsetof(MainScreen_t1502697305, ___endOfLevels_5)); }
	inline GameObject_t1756533147 * get_endOfLevels_5() const { return ___endOfLevels_5; }
	inline GameObject_t1756533147 ** get_address_of_endOfLevels_5() { return &___endOfLevels_5; }
	inline void set_endOfLevels_5(GameObject_t1756533147 * value)
	{
		___endOfLevels_5 = value;
		Il2CppCodeGenWriteBarrier(&___endOfLevels_5, value);
	}

	inline static int32_t get_offset_of_interstitialBannerFrequency_6() { return static_cast<int32_t>(offsetof(MainScreen_t1502697305, ___interstitialBannerFrequency_6)); }
	inline int32_t get_interstitialBannerFrequency_6() const { return ___interstitialBannerFrequency_6; }
	inline int32_t* get_address_of_interstitialBannerFrequency_6() { return &___interstitialBannerFrequency_6; }
	inline void set_interstitialBannerFrequency_6(int32_t value)
	{
		___interstitialBannerFrequency_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
