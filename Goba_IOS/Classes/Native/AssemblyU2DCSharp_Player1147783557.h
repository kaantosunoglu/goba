﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player
struct  Player_t1147783557  : public Il2CppObject
{
public:
	// System.Int32 Player::<level>k__BackingField
	int32_t ___U3ClevelU3Ek__BackingField_0;
	// System.Int32 Player::<adsFrequency>k__BackingField
	int32_t ___U3CadsFrequencyU3Ek__BackingField_1;
	// System.Int32 Player::<levelTry>k__BackingField
	int32_t ___U3ClevelTryU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3ClevelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___U3ClevelU3Ek__BackingField_0)); }
	inline int32_t get_U3ClevelU3Ek__BackingField_0() const { return ___U3ClevelU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3ClevelU3Ek__BackingField_0() { return &___U3ClevelU3Ek__BackingField_0; }
	inline void set_U3ClevelU3Ek__BackingField_0(int32_t value)
	{
		___U3ClevelU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CadsFrequencyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___U3CadsFrequencyU3Ek__BackingField_1)); }
	inline int32_t get_U3CadsFrequencyU3Ek__BackingField_1() const { return ___U3CadsFrequencyU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CadsFrequencyU3Ek__BackingField_1() { return &___U3CadsFrequencyU3Ek__BackingField_1; }
	inline void set_U3CadsFrequencyU3Ek__BackingField_1(int32_t value)
	{
		___U3CadsFrequencyU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3ClevelTryU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___U3ClevelTryU3Ek__BackingField_2)); }
	inline int32_t get_U3ClevelTryU3Ek__BackingField_2() const { return ___U3ClevelTryU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3ClevelTryU3Ek__BackingField_2() { return &___U3ClevelTryU3Ek__BackingField_2; }
	inline void set_U3ClevelTryU3Ek__BackingField_2(int32_t value)
	{
		___U3ClevelTryU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
