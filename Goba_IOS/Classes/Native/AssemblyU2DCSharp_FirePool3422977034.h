﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// FirePool
struct FirePool_t3422977034;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FirePool
struct  FirePool_t3422977034  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject FirePool::ball
	GameObject_t1756533147 * ___ball_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> FirePool::FireBalls
	List_1_t1125654279 * ___FireBalls_4;

public:
	inline static int32_t get_offset_of_ball_3() { return static_cast<int32_t>(offsetof(FirePool_t3422977034, ___ball_3)); }
	inline GameObject_t1756533147 * get_ball_3() const { return ___ball_3; }
	inline GameObject_t1756533147 ** get_address_of_ball_3() { return &___ball_3; }
	inline void set_ball_3(GameObject_t1756533147 * value)
	{
		___ball_3 = value;
		Il2CppCodeGenWriteBarrier(&___ball_3, value);
	}

	inline static int32_t get_offset_of_FireBalls_4() { return static_cast<int32_t>(offsetof(FirePool_t3422977034, ___FireBalls_4)); }
	inline List_1_t1125654279 * get_FireBalls_4() const { return ___FireBalls_4; }
	inline List_1_t1125654279 ** get_address_of_FireBalls_4() { return &___FireBalls_4; }
	inline void set_FireBalls_4(List_1_t1125654279 * value)
	{
		___FireBalls_4 = value;
		Il2CppCodeGenWriteBarrier(&___FireBalls_4, value);
	}
};

struct FirePool_t3422977034_StaticFields
{
public:
	// FirePool FirePool::instance
	FirePool_t3422977034 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(FirePool_t3422977034_StaticFields, ___instance_2)); }
	inline FirePool_t3422977034 * get_instance_2() const { return ___instance_2; }
	inline FirePool_t3422977034 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(FirePool_t3422977034 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
