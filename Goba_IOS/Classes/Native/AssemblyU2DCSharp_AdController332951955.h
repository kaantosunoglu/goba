﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// AdController
struct AdController_t332951955;
// GoogleMobileAds.Api.InterstitialAd
struct InterstitialAd_t3805611425;
// GoogleMobileAds.Api.BannerView
struct BannerView_t1745853549;
// GoogleMobileAds.Api.AdRequest
struct AdRequest_t3179524098;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdController
struct  AdController_t332951955  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 AdController::interstitialBannerFrequency
	int32_t ___interstitialBannerFrequency_3;
	// GoogleMobileAds.Api.InterstitialAd AdController::interstitial
	InterstitialAd_t3805611425 * ___interstitial_4;
	// GoogleMobileAds.Api.BannerView AdController::bannerView
	BannerView_t1745853549 * ___bannerView_5;
	// GoogleMobileAds.Api.AdRequest AdController::bannerRequest
	AdRequest_t3179524098 * ___bannerRequest_6;
	// System.Int32 AdController::refreshInterval
	int32_t ___refreshInterval_7;

public:
	inline static int32_t get_offset_of_interstitialBannerFrequency_3() { return static_cast<int32_t>(offsetof(AdController_t332951955, ___interstitialBannerFrequency_3)); }
	inline int32_t get_interstitialBannerFrequency_3() const { return ___interstitialBannerFrequency_3; }
	inline int32_t* get_address_of_interstitialBannerFrequency_3() { return &___interstitialBannerFrequency_3; }
	inline void set_interstitialBannerFrequency_3(int32_t value)
	{
		___interstitialBannerFrequency_3 = value;
	}

	inline static int32_t get_offset_of_interstitial_4() { return static_cast<int32_t>(offsetof(AdController_t332951955, ___interstitial_4)); }
	inline InterstitialAd_t3805611425 * get_interstitial_4() const { return ___interstitial_4; }
	inline InterstitialAd_t3805611425 ** get_address_of_interstitial_4() { return &___interstitial_4; }
	inline void set_interstitial_4(InterstitialAd_t3805611425 * value)
	{
		___interstitial_4 = value;
		Il2CppCodeGenWriteBarrier(&___interstitial_4, value);
	}

	inline static int32_t get_offset_of_bannerView_5() { return static_cast<int32_t>(offsetof(AdController_t332951955, ___bannerView_5)); }
	inline BannerView_t1745853549 * get_bannerView_5() const { return ___bannerView_5; }
	inline BannerView_t1745853549 ** get_address_of_bannerView_5() { return &___bannerView_5; }
	inline void set_bannerView_5(BannerView_t1745853549 * value)
	{
		___bannerView_5 = value;
		Il2CppCodeGenWriteBarrier(&___bannerView_5, value);
	}

	inline static int32_t get_offset_of_bannerRequest_6() { return static_cast<int32_t>(offsetof(AdController_t332951955, ___bannerRequest_6)); }
	inline AdRequest_t3179524098 * get_bannerRequest_6() const { return ___bannerRequest_6; }
	inline AdRequest_t3179524098 ** get_address_of_bannerRequest_6() { return &___bannerRequest_6; }
	inline void set_bannerRequest_6(AdRequest_t3179524098 * value)
	{
		___bannerRequest_6 = value;
		Il2CppCodeGenWriteBarrier(&___bannerRequest_6, value);
	}

	inline static int32_t get_offset_of_refreshInterval_7() { return static_cast<int32_t>(offsetof(AdController_t332951955, ___refreshInterval_7)); }
	inline int32_t get_refreshInterval_7() const { return ___refreshInterval_7; }
	inline int32_t* get_address_of_refreshInterval_7() { return &___refreshInterval_7; }
	inline void set_refreshInterval_7(int32_t value)
	{
		___refreshInterval_7 = value;
	}
};

struct AdController_t332951955_StaticFields
{
public:
	// AdController AdController::instance
	AdController_t332951955 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(AdController_t332951955_StaticFields, ___instance_2)); }
	inline AdController_t332951955 * get_instance_2() const { return ___instance_2; }
	inline AdController_t332951955 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(AdController_t332951955 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
