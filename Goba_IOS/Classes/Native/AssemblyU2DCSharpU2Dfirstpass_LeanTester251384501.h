﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeanTester
struct  LeanTester_t251384501  : public MonoBehaviour_t1158329972
{
public:
	// System.Single LeanTester::timeout
	float ___timeout_2;

public:
	inline static int32_t get_offset_of_timeout_2() { return static_cast<int32_t>(offsetof(LeanTester_t251384501, ___timeout_2)); }
	inline float get_timeout_2() const { return ___timeout_2; }
	inline float* get_address_of_timeout_2() { return &___timeout_2; }
	inline void set_timeout_2(float value)
	{
		___timeout_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
