﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Ring
struct Ring_t1566642426;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ring
struct  Ring_t1566642426  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] Ring::currentRings
	GameObjectU5BU5D_t3057952154* ___currentRings_3;

public:
	inline static int32_t get_offset_of_currentRings_3() { return static_cast<int32_t>(offsetof(Ring_t1566642426, ___currentRings_3)); }
	inline GameObjectU5BU5D_t3057952154* get_currentRings_3() const { return ___currentRings_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_currentRings_3() { return &___currentRings_3; }
	inline void set_currentRings_3(GameObjectU5BU5D_t3057952154* value)
	{
		___currentRings_3 = value;
		Il2CppCodeGenWriteBarrier(&___currentRings_3, value);
	}
};

struct Ring_t1566642426_StaticFields
{
public:
	// Ring Ring::instance
	Ring_t1566642426 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Ring_t1566642426_StaticFields, ___instance_2)); }
	inline Ring_t1566642426 * get_instance_2() const { return ___instance_2; }
	inline Ring_t1566642426 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(Ring_t1566642426 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
