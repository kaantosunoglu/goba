﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WindowTransition
struct  WindowTransition_t4263452729  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean WindowTransition::doAnimateOnLoad
	bool ___doAnimateOnLoad_2;
	// System.Boolean WindowTransition::doAnimateOnDestroy
	bool ___doAnimateOnDestroy_3;
	// System.Boolean WindowTransition::doFadeInBackLayOnLoad
	bool ___doFadeInBackLayOnLoad_4;
	// System.Boolean WindowTransition::doFadeOutBacklayOnDestroy
	bool ___doFadeOutBacklayOnDestroy_5;
	// System.Boolean WindowTransition::DestroyOnFinish
	bool ___DestroyOnFinish_6;
	// UnityEngine.UI.Image WindowTransition::BackLay
	Image_t2042527209 * ___BackLay_7;
	// UnityEngine.GameObject WindowTransition::WindowContent
	GameObject_t1756533147 * ___WindowContent_8;
	// System.Single WindowTransition::TransitionDuration
	float ___TransitionDuration_9;
	// UnityEngine.Vector3 WindowTransition::initialPosition
	Vector3_t2243707580  ___initialPosition_10;

public:
	inline static int32_t get_offset_of_doAnimateOnLoad_2() { return static_cast<int32_t>(offsetof(WindowTransition_t4263452729, ___doAnimateOnLoad_2)); }
	inline bool get_doAnimateOnLoad_2() const { return ___doAnimateOnLoad_2; }
	inline bool* get_address_of_doAnimateOnLoad_2() { return &___doAnimateOnLoad_2; }
	inline void set_doAnimateOnLoad_2(bool value)
	{
		___doAnimateOnLoad_2 = value;
	}

	inline static int32_t get_offset_of_doAnimateOnDestroy_3() { return static_cast<int32_t>(offsetof(WindowTransition_t4263452729, ___doAnimateOnDestroy_3)); }
	inline bool get_doAnimateOnDestroy_3() const { return ___doAnimateOnDestroy_3; }
	inline bool* get_address_of_doAnimateOnDestroy_3() { return &___doAnimateOnDestroy_3; }
	inline void set_doAnimateOnDestroy_3(bool value)
	{
		___doAnimateOnDestroy_3 = value;
	}

	inline static int32_t get_offset_of_doFadeInBackLayOnLoad_4() { return static_cast<int32_t>(offsetof(WindowTransition_t4263452729, ___doFadeInBackLayOnLoad_4)); }
	inline bool get_doFadeInBackLayOnLoad_4() const { return ___doFadeInBackLayOnLoad_4; }
	inline bool* get_address_of_doFadeInBackLayOnLoad_4() { return &___doFadeInBackLayOnLoad_4; }
	inline void set_doFadeInBackLayOnLoad_4(bool value)
	{
		___doFadeInBackLayOnLoad_4 = value;
	}

	inline static int32_t get_offset_of_doFadeOutBacklayOnDestroy_5() { return static_cast<int32_t>(offsetof(WindowTransition_t4263452729, ___doFadeOutBacklayOnDestroy_5)); }
	inline bool get_doFadeOutBacklayOnDestroy_5() const { return ___doFadeOutBacklayOnDestroy_5; }
	inline bool* get_address_of_doFadeOutBacklayOnDestroy_5() { return &___doFadeOutBacklayOnDestroy_5; }
	inline void set_doFadeOutBacklayOnDestroy_5(bool value)
	{
		___doFadeOutBacklayOnDestroy_5 = value;
	}

	inline static int32_t get_offset_of_DestroyOnFinish_6() { return static_cast<int32_t>(offsetof(WindowTransition_t4263452729, ___DestroyOnFinish_6)); }
	inline bool get_DestroyOnFinish_6() const { return ___DestroyOnFinish_6; }
	inline bool* get_address_of_DestroyOnFinish_6() { return &___DestroyOnFinish_6; }
	inline void set_DestroyOnFinish_6(bool value)
	{
		___DestroyOnFinish_6 = value;
	}

	inline static int32_t get_offset_of_BackLay_7() { return static_cast<int32_t>(offsetof(WindowTransition_t4263452729, ___BackLay_7)); }
	inline Image_t2042527209 * get_BackLay_7() const { return ___BackLay_7; }
	inline Image_t2042527209 ** get_address_of_BackLay_7() { return &___BackLay_7; }
	inline void set_BackLay_7(Image_t2042527209 * value)
	{
		___BackLay_7 = value;
		Il2CppCodeGenWriteBarrier(&___BackLay_7, value);
	}

	inline static int32_t get_offset_of_WindowContent_8() { return static_cast<int32_t>(offsetof(WindowTransition_t4263452729, ___WindowContent_8)); }
	inline GameObject_t1756533147 * get_WindowContent_8() const { return ___WindowContent_8; }
	inline GameObject_t1756533147 ** get_address_of_WindowContent_8() { return &___WindowContent_8; }
	inline void set_WindowContent_8(GameObject_t1756533147 * value)
	{
		___WindowContent_8 = value;
		Il2CppCodeGenWriteBarrier(&___WindowContent_8, value);
	}

	inline static int32_t get_offset_of_TransitionDuration_9() { return static_cast<int32_t>(offsetof(WindowTransition_t4263452729, ___TransitionDuration_9)); }
	inline float get_TransitionDuration_9() const { return ___TransitionDuration_9; }
	inline float* get_address_of_TransitionDuration_9() { return &___TransitionDuration_9; }
	inline void set_TransitionDuration_9(float value)
	{
		___TransitionDuration_9 = value;
	}

	inline static int32_t get_offset_of_initialPosition_10() { return static_cast<int32_t>(offsetof(WindowTransition_t4263452729, ___initialPosition_10)); }
	inline Vector3_t2243707580  get_initialPosition_10() const { return ___initialPosition_10; }
	inline Vector3_t2243707580 * get_address_of_initialPosition_10() { return &___initialPosition_10; }
	inline void set_initialPosition_10(Vector3_t2243707580  value)
	{
		___initialPosition_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
