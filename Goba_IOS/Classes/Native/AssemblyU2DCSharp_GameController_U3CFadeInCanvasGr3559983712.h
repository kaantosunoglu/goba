﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController/<FadeInCanvasGroup>c__Iterator0
struct  U3CFadeInCanvasGroupU3Ec__Iterator0_t3559983712  : public Il2CppObject
{
public:
	// System.Single GameController/<FadeInCanvasGroup>c__Iterator0::<opacity>__1
	float ___U3CopacityU3E__1_0;
	// UnityEngine.CanvasGroup GameController/<FadeInCanvasGroup>c__Iterator0::canvasGroup
	CanvasGroup_t3296560743 * ___canvasGroup_1;
	// System.Object GameController/<FadeInCanvasGroup>c__Iterator0::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean GameController/<FadeInCanvasGroup>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 GameController/<FadeInCanvasGroup>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CopacityU3E__1_0() { return static_cast<int32_t>(offsetof(U3CFadeInCanvasGroupU3Ec__Iterator0_t3559983712, ___U3CopacityU3E__1_0)); }
	inline float get_U3CopacityU3E__1_0() const { return ___U3CopacityU3E__1_0; }
	inline float* get_address_of_U3CopacityU3E__1_0() { return &___U3CopacityU3E__1_0; }
	inline void set_U3CopacityU3E__1_0(float value)
	{
		___U3CopacityU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_canvasGroup_1() { return static_cast<int32_t>(offsetof(U3CFadeInCanvasGroupU3Ec__Iterator0_t3559983712, ___canvasGroup_1)); }
	inline CanvasGroup_t3296560743 * get_canvasGroup_1() const { return ___canvasGroup_1; }
	inline CanvasGroup_t3296560743 ** get_address_of_canvasGroup_1() { return &___canvasGroup_1; }
	inline void set_canvasGroup_1(CanvasGroup_t3296560743 * value)
	{
		___canvasGroup_1 = value;
		Il2CppCodeGenWriteBarrier(&___canvasGroup_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CFadeInCanvasGroupU3Ec__Iterator0_t3559983712, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CFadeInCanvasGroupU3Ec__Iterator0_t3559983712, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CFadeInCanvasGroupU3Ec__Iterator0_t3559983712, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
