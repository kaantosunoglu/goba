﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetBorderPosition
struct  SetBorderPosition_t3912786437  : public MonoBehaviour_t1158329972
{
public:
	// System.Single SetBorderPosition::CameraOtrhoSize
	float ___CameraOtrhoSize_2;
	// UnityEngine.GameObject SetBorderPosition::borderLeft
	GameObject_t1756533147 * ___borderLeft_3;
	// UnityEngine.GameObject SetBorderPosition::borderRight
	GameObject_t1756533147 * ___borderRight_4;
	// UnityEngine.GameObject SetBorderPosition::borderTop
	GameObject_t1756533147 * ___borderTop_5;
	// UnityEngine.GameObject SetBorderPosition::borderBottom
	GameObject_t1756533147 * ___borderBottom_6;

public:
	inline static int32_t get_offset_of_CameraOtrhoSize_2() { return static_cast<int32_t>(offsetof(SetBorderPosition_t3912786437, ___CameraOtrhoSize_2)); }
	inline float get_CameraOtrhoSize_2() const { return ___CameraOtrhoSize_2; }
	inline float* get_address_of_CameraOtrhoSize_2() { return &___CameraOtrhoSize_2; }
	inline void set_CameraOtrhoSize_2(float value)
	{
		___CameraOtrhoSize_2 = value;
	}

	inline static int32_t get_offset_of_borderLeft_3() { return static_cast<int32_t>(offsetof(SetBorderPosition_t3912786437, ___borderLeft_3)); }
	inline GameObject_t1756533147 * get_borderLeft_3() const { return ___borderLeft_3; }
	inline GameObject_t1756533147 ** get_address_of_borderLeft_3() { return &___borderLeft_3; }
	inline void set_borderLeft_3(GameObject_t1756533147 * value)
	{
		___borderLeft_3 = value;
		Il2CppCodeGenWriteBarrier(&___borderLeft_3, value);
	}

	inline static int32_t get_offset_of_borderRight_4() { return static_cast<int32_t>(offsetof(SetBorderPosition_t3912786437, ___borderRight_4)); }
	inline GameObject_t1756533147 * get_borderRight_4() const { return ___borderRight_4; }
	inline GameObject_t1756533147 ** get_address_of_borderRight_4() { return &___borderRight_4; }
	inline void set_borderRight_4(GameObject_t1756533147 * value)
	{
		___borderRight_4 = value;
		Il2CppCodeGenWriteBarrier(&___borderRight_4, value);
	}

	inline static int32_t get_offset_of_borderTop_5() { return static_cast<int32_t>(offsetof(SetBorderPosition_t3912786437, ___borderTop_5)); }
	inline GameObject_t1756533147 * get_borderTop_5() const { return ___borderTop_5; }
	inline GameObject_t1756533147 ** get_address_of_borderTop_5() { return &___borderTop_5; }
	inline void set_borderTop_5(GameObject_t1756533147 * value)
	{
		___borderTop_5 = value;
		Il2CppCodeGenWriteBarrier(&___borderTop_5, value);
	}

	inline static int32_t get_offset_of_borderBottom_6() { return static_cast<int32_t>(offsetof(SetBorderPosition_t3912786437, ___borderBottom_6)); }
	inline GameObject_t1756533147 * get_borderBottom_6() const { return ___borderBottom_6; }
	inline GameObject_t1756533147 ** get_address_of_borderBottom_6() { return &___borderBottom_6; }
	inline void set_borderBottom_6(GameObject_t1756533147 * value)
	{
		___borderBottom_6 = value;
		Il2CppCodeGenWriteBarrier(&___borderBottom_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
