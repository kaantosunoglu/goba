﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundButton
struct  SoundButton_t2170099235  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button SoundButton::btnSound
	Button_t2872111280 * ___btnSound_2;
	// UnityEngine.UI.Image SoundButton::btnSoundImage
	Image_t2042527209 * ___btnSoundImage_3;
	// UnityEngine.Sprite SoundButton::soundOnSprite
	Sprite_t309593783 * ___soundOnSprite_4;
	// UnityEngine.Sprite SoundButton::soundOffSprite
	Sprite_t309593783 * ___soundOffSprite_5;

public:
	inline static int32_t get_offset_of_btnSound_2() { return static_cast<int32_t>(offsetof(SoundButton_t2170099235, ___btnSound_2)); }
	inline Button_t2872111280 * get_btnSound_2() const { return ___btnSound_2; }
	inline Button_t2872111280 ** get_address_of_btnSound_2() { return &___btnSound_2; }
	inline void set_btnSound_2(Button_t2872111280 * value)
	{
		___btnSound_2 = value;
		Il2CppCodeGenWriteBarrier(&___btnSound_2, value);
	}

	inline static int32_t get_offset_of_btnSoundImage_3() { return static_cast<int32_t>(offsetof(SoundButton_t2170099235, ___btnSoundImage_3)); }
	inline Image_t2042527209 * get_btnSoundImage_3() const { return ___btnSoundImage_3; }
	inline Image_t2042527209 ** get_address_of_btnSoundImage_3() { return &___btnSoundImage_3; }
	inline void set_btnSoundImage_3(Image_t2042527209 * value)
	{
		___btnSoundImage_3 = value;
		Il2CppCodeGenWriteBarrier(&___btnSoundImage_3, value);
	}

	inline static int32_t get_offset_of_soundOnSprite_4() { return static_cast<int32_t>(offsetof(SoundButton_t2170099235, ___soundOnSprite_4)); }
	inline Sprite_t309593783 * get_soundOnSprite_4() const { return ___soundOnSprite_4; }
	inline Sprite_t309593783 ** get_address_of_soundOnSprite_4() { return &___soundOnSprite_4; }
	inline void set_soundOnSprite_4(Sprite_t309593783 * value)
	{
		___soundOnSprite_4 = value;
		Il2CppCodeGenWriteBarrier(&___soundOnSprite_4, value);
	}

	inline static int32_t get_offset_of_soundOffSprite_5() { return static_cast<int32_t>(offsetof(SoundButton_t2170099235, ___soundOffSprite_5)); }
	inline Sprite_t309593783 * get_soundOffSprite_5() const { return ___soundOffSprite_5; }
	inline Sprite_t309593783 ** get_address_of_soundOffSprite_5() { return &___soundOffSprite_5; }
	inline void set_soundOffSprite_5(Sprite_t309593783 * value)
	{
		___soundOffSprite_5 = value;
		Il2CppCodeGenWriteBarrier(&___soundOffSprite_5, value);
	}
};

struct SoundButton_t2170099235_StaticFields
{
public:
	// UnityEngine.Events.UnityAction SoundButton::<>f__am$cache0
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(SoundButton_t2170099235_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
