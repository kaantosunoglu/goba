﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Loader
struct  Loader_t1876627619  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Loader::loadedContents
	int32_t ___loadedContents_2;

public:
	inline static int32_t get_offset_of_loadedContents_2() { return static_cast<int32_t>(offsetof(Loader_t1876627619, ___loadedContents_2)); }
	inline int32_t get_loadedContents_2() const { return ___loadedContents_2; }
	inline int32_t* get_address_of_loadedContents_2() { return &___loadedContents_2; }
	inline void set_loadedContents_2(int32_t value)
	{
		___loadedContents_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
