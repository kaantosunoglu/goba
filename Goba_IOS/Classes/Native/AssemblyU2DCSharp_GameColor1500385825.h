﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameColor
struct  GameColor_t1500385825  : public Il2CppObject
{
public:
	// UnityEngine.Color GameColor::<backgroudColor>k__BackingField
	Color_t2020392075  ___U3CbackgroudColorU3Ek__BackingField_0;
	// UnityEngine.Color GameColor::<targetBackColor>k__BackingField
	Color_t2020392075  ___U3CtargetBackColorU3Ek__BackingField_1;
	// UnityEngine.Color GameColor::<targetFrontColor>k__BackingField
	Color_t2020392075  ___U3CtargetFrontColorU3Ek__BackingField_2;
	// UnityEngine.Color GameColor::<bulletColor>k__BackingField
	Color_t2020392075  ___U3CbulletColorU3Ek__BackingField_3;
	// UnityEngine.Color GameColor::<cannonColor>k__BackingField
	Color_t2020392075  ___U3CcannonColorU3Ek__BackingField_4;
	// UnityEngine.Color GameColor::<borderColor>k__BackingField
	Color_t2020392075  ___U3CborderColorU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CbackgroudColorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GameColor_t1500385825, ___U3CbackgroudColorU3Ek__BackingField_0)); }
	inline Color_t2020392075  get_U3CbackgroudColorU3Ek__BackingField_0() const { return ___U3CbackgroudColorU3Ek__BackingField_0; }
	inline Color_t2020392075 * get_address_of_U3CbackgroudColorU3Ek__BackingField_0() { return &___U3CbackgroudColorU3Ek__BackingField_0; }
	inline void set_U3CbackgroudColorU3Ek__BackingField_0(Color_t2020392075  value)
	{
		___U3CbackgroudColorU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CtargetBackColorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GameColor_t1500385825, ___U3CtargetBackColorU3Ek__BackingField_1)); }
	inline Color_t2020392075  get_U3CtargetBackColorU3Ek__BackingField_1() const { return ___U3CtargetBackColorU3Ek__BackingField_1; }
	inline Color_t2020392075 * get_address_of_U3CtargetBackColorU3Ek__BackingField_1() { return &___U3CtargetBackColorU3Ek__BackingField_1; }
	inline void set_U3CtargetBackColorU3Ek__BackingField_1(Color_t2020392075  value)
	{
		___U3CtargetBackColorU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CtargetFrontColorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GameColor_t1500385825, ___U3CtargetFrontColorU3Ek__BackingField_2)); }
	inline Color_t2020392075  get_U3CtargetFrontColorU3Ek__BackingField_2() const { return ___U3CtargetFrontColorU3Ek__BackingField_2; }
	inline Color_t2020392075 * get_address_of_U3CtargetFrontColorU3Ek__BackingField_2() { return &___U3CtargetFrontColorU3Ek__BackingField_2; }
	inline void set_U3CtargetFrontColorU3Ek__BackingField_2(Color_t2020392075  value)
	{
		___U3CtargetFrontColorU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CbulletColorU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GameColor_t1500385825, ___U3CbulletColorU3Ek__BackingField_3)); }
	inline Color_t2020392075  get_U3CbulletColorU3Ek__BackingField_3() const { return ___U3CbulletColorU3Ek__BackingField_3; }
	inline Color_t2020392075 * get_address_of_U3CbulletColorU3Ek__BackingField_3() { return &___U3CbulletColorU3Ek__BackingField_3; }
	inline void set_U3CbulletColorU3Ek__BackingField_3(Color_t2020392075  value)
	{
		___U3CbulletColorU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CcannonColorU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GameColor_t1500385825, ___U3CcannonColorU3Ek__BackingField_4)); }
	inline Color_t2020392075  get_U3CcannonColorU3Ek__BackingField_4() const { return ___U3CcannonColorU3Ek__BackingField_4; }
	inline Color_t2020392075 * get_address_of_U3CcannonColorU3Ek__BackingField_4() { return &___U3CcannonColorU3Ek__BackingField_4; }
	inline void set_U3CcannonColorU3Ek__BackingField_4(Color_t2020392075  value)
	{
		___U3CcannonColorU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CborderColorU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GameColor_t1500385825, ___U3CborderColorU3Ek__BackingField_5)); }
	inline Color_t2020392075  get_U3CborderColorU3Ek__BackingField_5() const { return ___U3CborderColorU3Ek__BackingField_5; }
	inline Color_t2020392075 * get_address_of_U3CborderColorU3Ek__BackingField_5() { return &___U3CborderColorU3Ek__BackingField_5; }
	inline void set_U3CborderColorU3Ek__BackingField_5(Color_t2020392075  value)
	{
		___U3CborderColorU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
