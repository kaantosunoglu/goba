﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GameController_U3CGetUIScreenU3Ec758236687.h"
#include "AssemblyU2DCSharp_GameController_U3CFadeInCanvasGr3559983712.h"
#include "AssemblyU2DCSharp_GameController_U3CFadeOutCanvasGro49305870.h"
#include "AssemblyU2DCSharp_StoreController3314850793.h"
#include "AssemblyU2DCSharp_DespawnParticle3195118616.h"
#include "AssemblyU2DCSharp_ExtentionMethods1398113628.h"
#include "AssemblyU2DCSharp_FirePool3422977034.h"
#include "AssemblyU2DCSharp_GameOver1156344616.h"
#include "AssemblyU2DCSharp_GamePlay1646832012.h"
#include "AssemblyU2DCSharp_InputManager1610719423.h"
#include "AssemblyU2DCSharp_InputManager_U3CEnbaleTouchAfter1549736386.h"
#include "AssemblyU2DCSharp_Loader1876627619.h"
#include "AssemblyU2DCSharp_Loader_U3CGoToGameU3Ec__Iterator0127672042.h"
#include "AssemblyU2DCSharp_MainScreen1502697305.h"
#include "AssemblyU2DCSharp_MainScreen_U3CWaitForFacebookReq2246752708.h"
#include "AssemblyU2DCSharp_MainScreen_U3CWaitForTwitterRequ4136419160.h"
#include "AssemblyU2DCSharp_Model873752437.h"
#include "AssemblyU2DCSharp_RingBorder477331216.h"
#include "AssemblyU2DCSharp_Level866853782.h"
#include "AssemblyU2DCSharp_Levels748035019.h"
#include "AssemblyU2DCSharp_Player1147783557.h"
#include "AssemblyU2DCSharp_GameColor1500385825.h"
#include "AssemblyU2DCSharp_GameColors1153883414.h"
#include "AssemblyU2DCSharp_MusicButton325522625.h"
#include "AssemblyU2DCSharp_ParticlePool3081421488.h"
#include "AssemblyU2DCSharp_PassedPanel1159949582.h"
#include "AssemblyU2DCSharp_Paused4245159726.h"
#include "AssemblyU2DCSharp_QuitConfirm3413857337.h"
#include "AssemblyU2DCSharp_Ring1566642426.h"
#include "AssemblyU2DCSharp_SetBorderPosition3912786437.h"
#include "AssemblyU2DCSharp_ShakeObject4224104523.h"
#include "AssemblyU2DCSharp_SoundButton2170099235.h"
#include "AssemblyU2DCSharp_UnityAds2588158543.h"
#include "AssemblyU2DCSharp_UnityAds_U3CShowAdsU3Ec__AnonSto1075161772.h"
#include "AssemblyU2DCSharp_UnityAds_U3CShowAdsWithResultU3E1779434140.h"
#include "AssemblyU2DCSharp_WindowTransition4263452729.h"
#include "AssemblyU2DCSharp_JsonNetSample2758271331.h"
#include "AssemblyU2DCSharp_JsonNetSample_Product2714520675.h"
#include "AssemblyU2DCSharp_JsonNetSample_CharacterListItem1706204900.h"
#include "AssemblyU2DCSharp_JsonNetSample_Movie2835007224.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (U3CGetUIScreenU3Ec__AnonStorey3_t758236687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[1] = 
{
	U3CGetUIScreenU3Ec__AnonStorey3_t758236687::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (U3CFadeInCanvasGroupU3Ec__Iterator0_t3559983712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2301[5] = 
{
	U3CFadeInCanvasGroupU3Ec__Iterator0_t3559983712::get_offset_of_U3CopacityU3E__1_0(),
	U3CFadeInCanvasGroupU3Ec__Iterator0_t3559983712::get_offset_of_canvasGroup_1(),
	U3CFadeInCanvasGroupU3Ec__Iterator0_t3559983712::get_offset_of_U24current_2(),
	U3CFadeInCanvasGroupU3Ec__Iterator0_t3559983712::get_offset_of_U24disposing_3(),
	U3CFadeInCanvasGroupU3Ec__Iterator0_t3559983712::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (U3CFadeOutCanvasGroupU3Ec__Iterator1_t49305870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2302[6] = 
{
	U3CFadeOutCanvasGroupU3Ec__Iterator1_t49305870::get_offset_of_U3CopacityU3E__1_0(),
	U3CFadeOutCanvasGroupU3Ec__Iterator1_t49305870::get_offset_of_canvasGroup_1(),
	U3CFadeOutCanvasGroupU3Ec__Iterator1_t49305870::get_offset_of_disableOnFadeOut_2(),
	U3CFadeOutCanvasGroupU3Ec__Iterator1_t49305870::get_offset_of_U24current_3(),
	U3CFadeOutCanvasGroupU3Ec__Iterator1_t49305870::get_offset_of_U24disposing_4(),
	U3CFadeOutCanvasGroupU3Ec__Iterator1_t49305870::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (StoreController_t3314850793), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (DespawnParticle_t3195118616), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2304[1] = 
{
	DespawnParticle_t3195118616::get_offset_of_DespawnDelay_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (ExtentionMethods_t1398113628), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (FirePool_t3422977034), -1, sizeof(FirePool_t3422977034_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2306[3] = 
{
	FirePool_t3422977034_StaticFields::get_offset_of_instance_2(),
	FirePool_t3422977034::get_offset_of_ball_3(),
	FirePool_t3422977034::get_offset_of_FireBalls_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (GameOver_t1156344616), -1, sizeof(GameOver_t1156344616_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2307[2] = 
{
	GameOver_t1156344616::get_offset_of_GameOverAudio_2(),
	GameOver_t1156344616_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (GamePlay_t1646832012), -1, sizeof(GamePlay_t1646832012_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2308[18] = 
{
	GamePlay_t1646832012_StaticFields::get_offset_of_instance_2(),
	GamePlay_t1646832012::get_offset_of_txtScore_3(),
	GamePlay_t1646832012::get_offset_of_ballCount_4(),
	GamePlay_t1646832012::get_offset_of_gameOverPanel_5(),
	GamePlay_t1646832012::get_offset_of_SuccessHit_6(),
	GamePlay_t1646832012::get_offset_of_RingHit_7(),
	GamePlay_t1646832012::get_offset_of_BGColors_8(),
	GamePlay_t1646832012::get_offset_of_isRescued_9(),
	GamePlay_t1646832012::get_offset_of_targetScore_10(),
	GamePlay_t1646832012::get_offset_of_HowToPanel_11(),
	GamePlay_t1646832012::get_offset_of_ring_12(),
	GamePlay_t1646832012::get_offset_of_background_13(),
	GamePlay_t1646832012::get_offset_of_cannonBall_14(),
	GamePlay_t1646832012::get_offset_of_targetBack_15(),
	GamePlay_t1646832012::get_offset_of_targetFront_16(),
	GamePlay_t1646832012::get_offset_of_score_17(),
	GamePlay_t1646832012::get_offset_of_isGamePlay_18(),
	GamePlay_t1646832012::get_offset_of_balls_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (InputManager_t1610719423), -1, sizeof(InputManager_t1610719423_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2309[9] = 
{
	InputManager_t1610719423_StaticFields::get_offset_of_OnTouchDownEvent_2(),
	InputManager_t1610719423_StaticFields::get_offset_of_OnTouchUpEvent_3(),
	InputManager_t1610719423_StaticFields::get_offset_of_OnMouseDownEvent_4(),
	InputManager_t1610719423_StaticFields::get_offset_of_OnMouseUpEvent_5(),
	InputManager_t1610719423_StaticFields::get_offset_of_OnBackButtonPressedEvent_6(),
	InputManager_t1610719423_StaticFields::get_offset_of_instance_7(),
	InputManager_t1610719423_StaticFields::get_offset_of_isTouchAvailable_8(),
	InputManager_t1610719423::get_offset_of_eventSystem_9(),
	InputManager_t1610719423::get_offset_of_ClickSound_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (U3CEnbaleTouchAfterDelayU3Ec__Iterator0_t1549736386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2310[5] = 
{
	U3CEnbaleTouchAfterDelayU3Ec__Iterator0_t1549736386::get_offset_of_delay_0(),
	U3CEnbaleTouchAfterDelayU3Ec__Iterator0_t1549736386::get_offset_of_U24this_1(),
	U3CEnbaleTouchAfterDelayU3Ec__Iterator0_t1549736386::get_offset_of_U24current_2(),
	U3CEnbaleTouchAfterDelayU3Ec__Iterator0_t1549736386::get_offset_of_U24disposing_3(),
	U3CEnbaleTouchAfterDelayU3Ec__Iterator0_t1549736386::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (Loader_t1876627619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2311[1] = 
{
	Loader_t1876627619::get_offset_of_loadedContents_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (U3CGoToGameU3Ec__Iterator0_t127672042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2312[4] = 
{
	U3CGoToGameU3Ec__Iterator0_t127672042::get_offset_of_U24this_0(),
	U3CGoToGameU3Ec__Iterator0_t127672042::get_offset_of_U24current_1(),
	U3CGoToGameU3Ec__Iterator0_t127672042::get_offset_of_U24disposing_2(),
	U3CGoToGameU3Ec__Iterator0_t127672042::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (MainScreen_t1502697305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2313[5] = 
{
	MainScreen_t1502697305::get_offset_of_txtStatus1_2(),
	MainScreen_t1502697305::get_offset_of_txtStatus2_3(),
	MainScreen_t1502697305::get_offset_of_levelText_4(),
	MainScreen_t1502697305::get_offset_of_endOfLevels_5(),
	MainScreen_t1502697305::get_offset_of_interstitialBannerFrequency_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (U3CWaitForFacebookRequestU3Ec__Iterator0_t2246752708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[4] = 
{
	U3CWaitForFacebookRequestU3Ec__Iterator0_t2246752708::get_offset_of_www_0(),
	U3CWaitForFacebookRequestU3Ec__Iterator0_t2246752708::get_offset_of_U24current_1(),
	U3CWaitForFacebookRequestU3Ec__Iterator0_t2246752708::get_offset_of_U24disposing_2(),
	U3CWaitForFacebookRequestU3Ec__Iterator0_t2246752708::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (U3CWaitForTwitterRequestU3Ec__Iterator1_t4136419160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2315[4] = 
{
	U3CWaitForTwitterRequestU3Ec__Iterator1_t4136419160::get_offset_of_www_0(),
	U3CWaitForTwitterRequestU3Ec__Iterator1_t4136419160::get_offset_of_U24current_1(),
	U3CWaitForTwitterRequestU3Ec__Iterator1_t4136419160::get_offset_of_U24disposing_2(),
	U3CWaitForTwitterRequestU3Ec__Iterator1_t4136419160::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (Model_t873752437), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (RingBorder_t477331216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2317[5] = 
{
	RingBorder_t477331216::get_offset_of_U3CringPrefabU3Ek__BackingField_0(),
	RingBorder_t477331216::get_offset_of_U3CrotationSpeedU3Ek__BackingField_1(),
	RingBorder_t477331216::get_offset_of_U3CrotationDirectionU3Ek__BackingField_2(),
	RingBorder_t477331216::get_offset_of_U3CringPositionU3Ek__BackingField_3(),
	RingBorder_t477331216::get_offset_of_U3CrotationU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (Level_t866853782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2318[2] = 
{
	Level_t866853782::get_offset_of_U3CringsU3Ek__BackingField_0(),
	Level_t866853782::get_offset_of_ballCount_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (Levels_t748035019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2319[1] = 
{
	Levels_t748035019::get_offset_of_U3ClevelsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (Player_t1147783557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2320[3] = 
{
	Player_t1147783557::get_offset_of_U3ClevelU3Ek__BackingField_0(),
	Player_t1147783557::get_offset_of_U3CadsFrequencyU3Ek__BackingField_1(),
	Player_t1147783557::get_offset_of_U3ClevelTryU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (GameColor_t1500385825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2321[6] = 
{
	GameColor_t1500385825::get_offset_of_U3CbackgroudColorU3Ek__BackingField_0(),
	GameColor_t1500385825::get_offset_of_U3CtargetBackColorU3Ek__BackingField_1(),
	GameColor_t1500385825::get_offset_of_U3CtargetFrontColorU3Ek__BackingField_2(),
	GameColor_t1500385825::get_offset_of_U3CbulletColorU3Ek__BackingField_3(),
	GameColor_t1500385825::get_offset_of_U3CcannonColorU3Ek__BackingField_4(),
	GameColor_t1500385825::get_offset_of_U3CborderColorU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (GameColors_t1153883414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[1] = 
{
	GameColors_t1153883414::get_offset_of_U3CgameColorsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (MusicButton_t325522625), -1, sizeof(MusicButton_t325522625_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2323[5] = 
{
	MusicButton_t325522625::get_offset_of_btnMusic_2(),
	MusicButton_t325522625::get_offset_of_btnMusicImage_3(),
	MusicButton_t325522625::get_offset_of_musicOnSprite_4(),
	MusicButton_t325522625::get_offset_of_musicOffSprite_5(),
	MusicButton_t325522625_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (ParticlePool_t3081421488), -1, sizeof(ParticlePool_t3081421488_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2324[3] = 
{
	ParticlePool_t3081421488_StaticFields::get_offset_of_instance_2(),
	ParticlePool_t3081421488::get_offset_of_ParticleRing_3(),
	ParticlePool_t3081421488::get_offset_of_ParticleRings_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (PassedPanel_t1159949582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2325[2] = 
{
	PassedPanel_t1159949582::get_offset_of_appstoreButton_2(),
	PassedPanel_t1159949582::get_offset_of_googleplayButton_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (Paused_t4245159726), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (QuitConfirm_t3413857337), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (Ring_t1566642426), -1, sizeof(Ring_t1566642426_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2328[2] = 
{
	Ring_t1566642426_StaticFields::get_offset_of_instance_2(),
	Ring_t1566642426::get_offset_of_currentRings_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (SetBorderPosition_t3912786437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2329[5] = 
{
	SetBorderPosition_t3912786437::get_offset_of_CameraOtrhoSize_2(),
	SetBorderPosition_t3912786437::get_offset_of_borderLeft_3(),
	SetBorderPosition_t3912786437::get_offset_of_borderRight_4(),
	SetBorderPosition_t3912786437::get_offset_of_borderTop_5(),
	SetBorderPosition_t3912786437::get_offset_of_borderBottom_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (ShakeObject_t4224104523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2330[7] = 
{
	ShakeObject_t4224104523::get_offset_of_objectOrigin_2(),
	ShakeObject_t4224104523::get_offset_of_strength_3(),
	ShakeObject_t4224104523::get_offset_of_strengthDefault_4(),
	ShakeObject_t4224104523::get_offset_of_decay_5(),
	ShakeObject_t4224104523::get_offset_of_shakeTime_6(),
	ShakeObject_t4224104523::get_offset_of_shakeTimeDefault_7(),
	ShakeObject_t4224104523::get_offset_of_isShaking_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (SoundButton_t2170099235), -1, sizeof(SoundButton_t2170099235_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2331[5] = 
{
	SoundButton_t2170099235::get_offset_of_btnSound_2(),
	SoundButton_t2170099235::get_offset_of_btnSoundImage_3(),
	SoundButton_t2170099235::get_offset_of_soundOnSprite_4(),
	SoundButton_t2170099235::get_offset_of_soundOffSprite_5(),
	SoundButton_t2170099235_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (UnityAds_t2588158543), -1, sizeof(UnityAds_t2588158543_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2332[3] = 
{
	UnityAds_t2588158543_StaticFields::get_offset_of_instance_2(),
	UnityAds_t2588158543::get_offset_of_androidAppID_3(),
	UnityAds_t2588158543::get_offset_of_iOSAppID_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (U3CShowAdsU3Ec__AnonStorey0_t1075161772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2333[1] = 
{
	U3CShowAdsU3Ec__AnonStorey0_t1075161772::get_offset_of_adResult_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (U3CShowAdsWithResultU3Ec__AnonStorey1_t1779434140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2334[1] = 
{
	U3CShowAdsWithResultU3Ec__AnonStorey1_t1779434140::get_offset_of_adResult_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (WindowTransition_t4263452729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2335[9] = 
{
	WindowTransition_t4263452729::get_offset_of_doAnimateOnLoad_2(),
	WindowTransition_t4263452729::get_offset_of_doAnimateOnDestroy_3(),
	WindowTransition_t4263452729::get_offset_of_doFadeInBackLayOnLoad_4(),
	WindowTransition_t4263452729::get_offset_of_doFadeOutBacklayOnDestroy_5(),
	WindowTransition_t4263452729::get_offset_of_DestroyOnFinish_6(),
	WindowTransition_t4263452729::get_offset_of_BackLay_7(),
	WindowTransition_t4263452729::get_offset_of_WindowContent_8(),
	WindowTransition_t4263452729::get_offset_of_TransitionDuration_9(),
	WindowTransition_t4263452729::get_offset_of_initialPosition_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (JsonNetSample_t2758271331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2336[1] = 
{
	JsonNetSample_t2758271331::get_offset_of_Output_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (Product_t2714520675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2337[4] = 
{
	Product_t2714520675::get_offset_of_Name_0(),
	Product_t2714520675::get_offset_of_ExpiryDate_1(),
	Product_t2714520675::get_offset_of_Price_2(),
	Product_t2714520675::get_offset_of_Sizes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (CharacterListItem_t1706204900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2338[5] = 
{
	CharacterListItem_t1706204900::get_offset_of_U3CIdU3Ek__BackingField_0(),
	CharacterListItem_t1706204900::get_offset_of_U3CNameU3Ek__BackingField_1(),
	CharacterListItem_t1706204900::get_offset_of_U3CLevelU3Ek__BackingField_2(),
	CharacterListItem_t1706204900::get_offset_of_U3CClassU3Ek__BackingField_3(),
	CharacterListItem_t1706204900::get_offset_of_U3CSexU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (Movie_t2835007224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2339[6] = 
{
	Movie_t2835007224::get_offset_of_U3CNameU3Ek__BackingField_0(),
	Movie_t2835007224::get_offset_of_U3CDescriptionU3Ek__BackingField_1(),
	Movie_t2835007224::get_offset_of_U3CClassificationU3Ek__BackingField_2(),
	Movie_t2835007224::get_offset_of_U3CStudioU3Ek__BackingField_3(),
	Movie_t2835007224::get_offset_of_U3CReleaseDateU3Ek__BackingField_4(),
	Movie_t2835007224::get_offset_of_U3CReleaseCountriesU3Ek__BackingField_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
