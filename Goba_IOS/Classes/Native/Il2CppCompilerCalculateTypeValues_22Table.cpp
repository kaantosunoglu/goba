﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LTDescr1981209097.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LTDescr_EaseTypeDele2852699939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LTDescr_ActionMethod1923010138.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LTDescrOptional91551083.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LeanAudioStream376238626.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LeanAudio4194475270.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LeanAudioOptions2925434024.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LeanAudioOptions_Lea3864441651.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LeanTester251384501.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LeanTester_U3Ctimeou2112552326.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LeanTest1978549332.h"
#include "AssemblyU2DCSharpU2Dfirstpass_TweenAction3330578889.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LeanTweenType1294766541.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LeanTween4120592429.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LTUtility80391392.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LTBezier3622621193.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LTBezierPath292484408.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LTSpline3159710915.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LTRect2720449186.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LTEvent25137460.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LTGUI1089343333.h"
#include "AssemblyU2DCSharpU2Dfirstpass_LTGUI_Element_Type980937599.h"
#include "AssemblyU2DCSharpU2Dfirstpass_DentedPixel_LeanDumm3369275062.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_AdFailedToLo1756611910.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_NativeAdType1094124130.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_AdLoader554394170.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_AdLoader_Build54889671.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_AdPosition2595513602.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_AdRequest3179524098.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_AdRequest_Bu2008174359.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_AdSize3231673570.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_BannerView1745853549.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_CustomNative2658458077.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_CustomNative2034144705.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_Gender3528073263.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_PurchaseResol467326495.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_Interstitial3805611425.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_Mediation_Me1641207307.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_NativeExpres1750838217.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_Reward1753549929.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Api_RewardBasedV2581948736.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Common_DummyClie1330686537.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_Common_Utils1179620951.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_GoogleMobileAdsCl898766308.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_NativeAdType3944121833.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_AdLoaderClien506419447.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_AdLoaderClie3126817269.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_AdLoaderClien619382744.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_BannerClient2837939223.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_BannerClient3611450851.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_BannerClient2294077762.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_BannerClient2607757429.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_BannerClient2257715507.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_BannerClient1946169147.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_CustomNative3776928493.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_CustomNative3121063597.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_Externs2948936873.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_Interstitial2538051773.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_Interstitial3343584307.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_Interstitial1829207408.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_InterstitialC387623197.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_Interstitial4025611083.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_InterstitialC216612155.h"
#include "AssemblyU2DCSharp_MonoPInvokeCallbackAttribute1970456718.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_NativeExpres2805846113.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_NativeExpres2677637168.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_NativeExpres3648222485.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_NativeExpres3478706784.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_NativeExpress784484480.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_NativeExpress548165752.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedV2282664017.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedV4169257859.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedVi862929376.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedVi587935421.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedVid25341677.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedV2453903099.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedVi129051320.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_RewardBasedV2167763867.h"
#include "AssemblyU2DCSharp_GoogleMobileAds_iOS_Utils984021165.h"
#include "AssemblyU2DCSharp_AppstoreHandler4742128.h"
#include "AssemblyU2DCSharp_AppstoreTestScene652637152.h"
#include "AssemblyU2DCSharp_AudioManager4222704959.h"
#include "AssemblyU2DCSharp_Ball3972794301.h"
#include "AssemblyU2DCSharp_Cannon3532423045.h"
#include "AssemblyU2DCSharp_AdController332951955.h"
#include "AssemblyU2DCSharp_BGMusicController4257240066.h"
#include "AssemblyU2DCSharp_Constants2437308775.h"
#include "AssemblyU2DCSharp_GameController3607102586.h"
#include "AssemblyU2DCSharp_GameController_GameStatus3461812585.h"
#include "AssemblyU2DCSharp_GameController_U3CSpawnUIScreenU3909795947.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (U3CModuleU3E_t3783534225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (LTDescr_t1981209097), -1, sizeof(LTDescr_t1981209097_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2201[52] = 
{
	LTDescr_t1981209097::get_offset_of_toggle_0(),
	LTDescr_t1981209097::get_offset_of_useEstimatedTime_1(),
	LTDescr_t1981209097::get_offset_of_useFrames_2(),
	LTDescr_t1981209097::get_offset_of_useManualTime_3(),
	LTDescr_t1981209097::get_offset_of_usesNormalDt_4(),
	LTDescr_t1981209097::get_offset_of_hasInitiliazed_5(),
	LTDescr_t1981209097::get_offset_of_hasExtraOnCompletes_6(),
	LTDescr_t1981209097::get_offset_of_hasPhysics_7(),
	LTDescr_t1981209097::get_offset_of_onCompleteOnRepeat_8(),
	LTDescr_t1981209097::get_offset_of_onCompleteOnStart_9(),
	LTDescr_t1981209097::get_offset_of_useRecursion_10(),
	LTDescr_t1981209097::get_offset_of_ratioPassed_11(),
	LTDescr_t1981209097::get_offset_of_passed_12(),
	LTDescr_t1981209097::get_offset_of_delay_13(),
	LTDescr_t1981209097::get_offset_of_time_14(),
	LTDescr_t1981209097::get_offset_of_speed_15(),
	LTDescr_t1981209097::get_offset_of_lastVal_16(),
	LTDescr_t1981209097::get_offset_of__id_17(),
	LTDescr_t1981209097::get_offset_of_loopCount_18(),
	LTDescr_t1981209097::get_offset_of_counter_19(),
	LTDescr_t1981209097::get_offset_of_direction_20(),
	LTDescr_t1981209097::get_offset_of_directionLast_21(),
	LTDescr_t1981209097::get_offset_of_overshoot_22(),
	LTDescr_t1981209097::get_offset_of_period_23(),
	LTDescr_t1981209097::get_offset_of_scale_24(),
	LTDescr_t1981209097::get_offset_of_destroyOnComplete_25(),
	LTDescr_t1981209097::get_offset_of_trans_26(),
	LTDescr_t1981209097::get_offset_of_ltRect_27(),
	LTDescr_t1981209097::get_offset_of_fromInternal_28(),
	LTDescr_t1981209097::get_offset_of_toInternal_29(),
	LTDescr_t1981209097::get_offset_of_diff_30(),
	LTDescr_t1981209097::get_offset_of_diffDiv2_31(),
	LTDescr_t1981209097::get_offset_of_type_32(),
	LTDescr_t1981209097::get_offset_of_easeType_33(),
	LTDescr_t1981209097::get_offset_of_loopType_34(),
	LTDescr_t1981209097::get_offset_of_hasUpdateCallback_35(),
	LTDescr_t1981209097::get_offset_of_easeMethod_36(),
	LTDescr_t1981209097::get_offset_of_U3CeaseInternalU3Ek__BackingField_37(),
	LTDescr_t1981209097::get_offset_of_U3CinitInternalU3Ek__BackingField_38(),
	LTDescr_t1981209097::get_offset_of_spriteRen_39(),
	LTDescr_t1981209097::get_offset_of_rectTransform_40(),
	LTDescr_t1981209097::get_offset_of_uiText_41(),
	LTDescr_t1981209097::get_offset_of_uiImage_42(),
	LTDescr_t1981209097::get_offset_of_rawImage_43(),
	LTDescr_t1981209097::get_offset_of_sprites_44(),
	LTDescr_t1981209097::get_offset_of__optional_45(),
	LTDescr_t1981209097_StaticFields::get_offset_of_global_counter_46(),
	LTDescr_t1981209097_StaticFields::get_offset_of_val_47(),
	LTDescr_t1981209097_StaticFields::get_offset_of_dt_48(),
	LTDescr_t1981209097_StaticFields::get_offset_of_newVect_49(),
	LTDescr_t1981209097_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_50(),
	LTDescr_t1981209097_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_51(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (EaseTypeDelegate_t2852699939), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (ActionMethodDelegate_t1923010138), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (LTDescrOptional_t91551083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2204[23] = 
{
	LTDescrOptional_t91551083::get_offset_of_U3CtoTransU3Ek__BackingField_0(),
	LTDescrOptional_t91551083::get_offset_of_U3CpointU3Ek__BackingField_1(),
	LTDescrOptional_t91551083::get_offset_of_U3CaxisU3Ek__BackingField_2(),
	LTDescrOptional_t91551083::get_offset_of_U3ClastValU3Ek__BackingField_3(),
	LTDescrOptional_t91551083::get_offset_of_U3CorigRotationU3Ek__BackingField_4(),
	LTDescrOptional_t91551083::get_offset_of_U3CpathU3Ek__BackingField_5(),
	LTDescrOptional_t91551083::get_offset_of_U3CsplineU3Ek__BackingField_6(),
	LTDescrOptional_t91551083::get_offset_of_animationCurve_7(),
	LTDescrOptional_t91551083::get_offset_of_initFrameCount_8(),
	LTDescrOptional_t91551083::get_offset_of_U3CltRectU3Ek__BackingField_9(),
	LTDescrOptional_t91551083::get_offset_of_U3ConUpdateFloatU3Ek__BackingField_10(),
	LTDescrOptional_t91551083::get_offset_of_U3ConUpdateFloatRatioU3Ek__BackingField_11(),
	LTDescrOptional_t91551083::get_offset_of_U3ConUpdateFloatObjectU3Ek__BackingField_12(),
	LTDescrOptional_t91551083::get_offset_of_U3ConUpdateVector2U3Ek__BackingField_13(),
	LTDescrOptional_t91551083::get_offset_of_U3ConUpdateVector3U3Ek__BackingField_14(),
	LTDescrOptional_t91551083::get_offset_of_U3ConUpdateVector3ObjectU3Ek__BackingField_15(),
	LTDescrOptional_t91551083::get_offset_of_U3ConUpdateColorU3Ek__BackingField_16(),
	LTDescrOptional_t91551083::get_offset_of_U3ConUpdateColorObjectU3Ek__BackingField_17(),
	LTDescrOptional_t91551083::get_offset_of_U3ConCompleteU3Ek__BackingField_18(),
	LTDescrOptional_t91551083::get_offset_of_U3ConCompleteObjectU3Ek__BackingField_19(),
	LTDescrOptional_t91551083::get_offset_of_U3ConCompleteParamU3Ek__BackingField_20(),
	LTDescrOptional_t91551083::get_offset_of_U3ConUpdateParamU3Ek__BackingField_21(),
	LTDescrOptional_t91551083::get_offset_of_U3ConStartU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (LeanAudioStream_t376238626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2205[3] = 
{
	LeanAudioStream_t376238626::get_offset_of_position_0(),
	LeanAudioStream_t376238626::get_offset_of_audioClip_1(),
	LeanAudioStream_t376238626::get_offset_of_audioArr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (LeanAudio_t4194475270), -1, sizeof(LeanAudio_t4194475270_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2206[6] = 
{
	LeanAudio_t4194475270_StaticFields::get_offset_of_MIN_FREQEUNCY_PERIOD_0(),
	LeanAudio_t4194475270_StaticFields::get_offset_of_PROCESSING_ITERATIONS_MAX_1(),
	LeanAudio_t4194475270_StaticFields::get_offset_of_generatedWaveDistances_2(),
	LeanAudio_t4194475270_StaticFields::get_offset_of_generatedWaveDistancesCount_3(),
	LeanAudio_t4194475270_StaticFields::get_offset_of_longList_4(),
	LeanAudio_t4194475270_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (LeanAudioOptions_t2925434024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2207[8] = 
{
	LeanAudioOptions_t2925434024::get_offset_of_waveStyle_0(),
	LeanAudioOptions_t2925434024::get_offset_of_vibrato_1(),
	LeanAudioOptions_t2925434024::get_offset_of_modulation_2(),
	LeanAudioOptions_t2925434024::get_offset_of_frequencyRate_3(),
	LeanAudioOptions_t2925434024::get_offset_of_waveNoiseScale_4(),
	LeanAudioOptions_t2925434024::get_offset_of_waveNoiseInfluence_5(),
	LeanAudioOptions_t2925434024::get_offset_of_useSetData_6(),
	LeanAudioOptions_t2925434024::get_offset_of_stream_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (LeanAudioWaveStyle_t3864441651)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2208[5] = 
{
	LeanAudioWaveStyle_t3864441651::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (LeanTester_t251384501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2209[1] = 
{
	LeanTester_t251384501::get_offset_of_timeout_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (U3CtimeoutCheckU3Ec__Iterator0_t2112552326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2210[5] = 
{
	U3CtimeoutCheckU3Ec__Iterator0_t2112552326::get_offset_of_U3CpauseEndTimeU3E__0_0(),
	U3CtimeoutCheckU3Ec__Iterator0_t2112552326::get_offset_of_U24this_1(),
	U3CtimeoutCheckU3Ec__Iterator0_t2112552326::get_offset_of_U24current_2(),
	U3CtimeoutCheckU3Ec__Iterator0_t2112552326::get_offset_of_U24disposing_3(),
	U3CtimeoutCheckU3Ec__Iterator0_t2112552326::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (LeanTest_t1978549332), -1, sizeof(LeanTest_t1978549332_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2211[6] = 
{
	LeanTest_t1978549332_StaticFields::get_offset_of_expected_0(),
	LeanTest_t1978549332_StaticFields::get_offset_of_tests_1(),
	LeanTest_t1978549332_StaticFields::get_offset_of_passes_2(),
	LeanTest_t1978549332_StaticFields::get_offset_of_timeout_3(),
	LeanTest_t1978549332_StaticFields::get_offset_of_timeoutStarted_4(),
	LeanTest_t1978549332_StaticFields::get_offset_of_testsFinished_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (TweenAction_t3330578889)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2212[51] = 
{
	TweenAction_t3330578889::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (LeanTweenType_t1294766541)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2213[40] = 
{
	LeanTweenType_t1294766541::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (LeanTween_t4120592429), -1, sizeof(LeanTween_t4120592429_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2214[28] = 
{
	LeanTween_t4120592429_StaticFields::get_offset_of_throwErrors_2(),
	LeanTween_t4120592429_StaticFields::get_offset_of_tau_3(),
	LeanTween_t4120592429_StaticFields::get_offset_of_PI_DIV2_4(),
	LeanTween_t4120592429_StaticFields::get_offset_of_tweens_5(),
	LeanTween_t4120592429_StaticFields::get_offset_of_tweensFinished_6(),
	LeanTween_t4120592429_StaticFields::get_offset_of_tween_7(),
	LeanTween_t4120592429_StaticFields::get_offset_of_tweenMaxSearch_8(),
	LeanTween_t4120592429_StaticFields::get_offset_of_maxTweens_9(),
	LeanTween_t4120592429_StaticFields::get_offset_of_frameRendered_10(),
	LeanTween_t4120592429_StaticFields::get_offset_of__tweenEmpty_11(),
	LeanTween_t4120592429_StaticFields::get_offset_of_dtEstimated_12(),
	LeanTween_t4120592429_StaticFields::get_offset_of_dtManual_13(),
	LeanTween_t4120592429_StaticFields::get_offset_of_dtActual_14(),
	LeanTween_t4120592429_StaticFields::get_offset_of_i_15(),
	LeanTween_t4120592429_StaticFields::get_offset_of_j_16(),
	LeanTween_t4120592429_StaticFields::get_offset_of_finishedCnt_17(),
	LeanTween_t4120592429_StaticFields::get_offset_of_punch_18(),
	LeanTween_t4120592429_StaticFields::get_offset_of_shake_19(),
	LeanTween_t4120592429_StaticFields::get_offset_of_maxTweenReached_20(),
	LeanTween_t4120592429_StaticFields::get_offset_of_startSearch_21(),
	LeanTween_t4120592429_StaticFields::get_offset_of_d_22(),
	LeanTween_t4120592429_StaticFields::get_offset_of_eventListeners_23(),
	LeanTween_t4120592429_StaticFields::get_offset_of_goListeners_24(),
	LeanTween_t4120592429_StaticFields::get_offset_of_eventsMaxSearch_25(),
	LeanTween_t4120592429_StaticFields::get_offset_of_EVENTS_MAX_26(),
	LeanTween_t4120592429_StaticFields::get_offset_of_LISTENERS_MAX_27(),
	LeanTween_t4120592429_StaticFields::get_offset_of_INIT_LISTENERS_MAX_28(),
	LeanTween_t4120592429_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (LTUtility_t80391392), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (LTBezier_t3622621193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2216[7] = 
{
	LTBezier_t3622621193::get_offset_of_length_0(),
	LTBezier_t3622621193::get_offset_of_a_1(),
	LTBezier_t3622621193::get_offset_of_aa_2(),
	LTBezier_t3622621193::get_offset_of_bb_3(),
	LTBezier_t3622621193::get_offset_of_cc_4(),
	LTBezier_t3622621193::get_offset_of_len_5(),
	LTBezier_t3622621193::get_offset_of_arcLengths_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (LTBezierPath_t292484408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2217[8] = 
{
	LTBezierPath_t292484408::get_offset_of_pts_0(),
	LTBezierPath_t292484408::get_offset_of_length_1(),
	LTBezierPath_t292484408::get_offset_of_orientToPath_2(),
	LTBezierPath_t292484408::get_offset_of_orientToPath2d_3(),
	LTBezierPath_t292484408::get_offset_of_beziers_4(),
	LTBezierPath_t292484408::get_offset_of_lengthRatio_5(),
	LTBezierPath_t292484408::get_offset_of_currentBezier_6(),
	LTBezierPath_t292484408::get_offset_of_previousBezier_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (LTSpline_t3159710915), -1, sizeof(LTSpline_t3159710915_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2218[11] = 
{
	LTSpline_t3159710915_StaticFields::get_offset_of_DISTANCE_COUNT_0(),
	LTSpline_t3159710915_StaticFields::get_offset_of_SUBLINE_COUNT_1(),
	LTSpline_t3159710915::get_offset_of_distance_2(),
	LTSpline_t3159710915::get_offset_of_constantSpeed_3(),
	LTSpline_t3159710915::get_offset_of_pts_4(),
	LTSpline_t3159710915::get_offset_of_ptsAdj_5(),
	LTSpline_t3159710915::get_offset_of_ptsAdjLength_6(),
	LTSpline_t3159710915::get_offset_of_orientToPath_7(),
	LTSpline_t3159710915::get_offset_of_orientToPath2d_8(),
	LTSpline_t3159710915::get_offset_of_numSections_9(),
	LTSpline_t3159710915::get_offset_of_currPt_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (LTRect_t2720449186), -1, sizeof(LTRect_t2720449186_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2219[21] = 
{
	LTRect_t2720449186::get_offset_of__rect_0(),
	LTRect_t2720449186::get_offset_of_alpha_1(),
	LTRect_t2720449186::get_offset_of_rotation_2(),
	LTRect_t2720449186::get_offset_of_pivot_3(),
	LTRect_t2720449186::get_offset_of_margin_4(),
	LTRect_t2720449186::get_offset_of_relativeRect_5(),
	LTRect_t2720449186::get_offset_of_rotateEnabled_6(),
	LTRect_t2720449186::get_offset_of_rotateFinished_7(),
	LTRect_t2720449186::get_offset_of_alphaEnabled_8(),
	LTRect_t2720449186::get_offset_of_labelStr_9(),
	LTRect_t2720449186::get_offset_of_type_10(),
	LTRect_t2720449186::get_offset_of_style_11(),
	LTRect_t2720449186::get_offset_of_useColor_12(),
	LTRect_t2720449186::get_offset_of_color_13(),
	LTRect_t2720449186::get_offset_of_fontScaleToFit_14(),
	LTRect_t2720449186::get_offset_of_useSimpleScale_15(),
	LTRect_t2720449186::get_offset_of_sizeByHeight_16(),
	LTRect_t2720449186::get_offset_of_texture_17(),
	LTRect_t2720449186::get_offset_of__id_18(),
	LTRect_t2720449186::get_offset_of_counter_19(),
	LTRect_t2720449186_StaticFields::get_offset_of_colorTouched_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (LTEvent_t25137460), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2220[2] = 
{
	LTEvent_t25137460::get_offset_of_id_0(),
	LTEvent_t25137460::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (LTGUI_t1089343333), -1, sizeof(LTGUI_t1089343333_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2221[12] = 
{
	LTGUI_t1089343333_StaticFields::get_offset_of_RECT_LEVELS_0(),
	LTGUI_t1089343333_StaticFields::get_offset_of_RECTS_PER_LEVEL_1(),
	LTGUI_t1089343333_StaticFields::get_offset_of_BUTTONS_MAX_2(),
	LTGUI_t1089343333_StaticFields::get_offset_of_levels_3(),
	LTGUI_t1089343333_StaticFields::get_offset_of_levelDepths_4(),
	LTGUI_t1089343333_StaticFields::get_offset_of_buttons_5(),
	LTGUI_t1089343333_StaticFields::get_offset_of_buttonLevels_6(),
	LTGUI_t1089343333_StaticFields::get_offset_of_buttonLastFrame_7(),
	LTGUI_t1089343333_StaticFields::get_offset_of_r_8(),
	LTGUI_t1089343333_StaticFields::get_offset_of_color_9(),
	LTGUI_t1089343333_StaticFields::get_offset_of_isGUIEnabled_10(),
	LTGUI_t1089343333_StaticFields::get_offset_of_global_counter_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (Element_Type_t980937599)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2222[3] = 
{
	Element_Type_t980937599::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (LeanDummy_t3369275062), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (U3CModuleU3E_t3783534226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (AdFailedToLoadEventArgs_t1756611910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2225[1] = 
{
	AdFailedToLoadEventArgs_t1756611910::get_offset_of_U3CMessageU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (NativeAdType_t1094124130)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2226[2] = 
{
	NativeAdType_t1094124130::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (AdLoader_t554394170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2227[7] = 
{
	AdLoader_t554394170::get_offset_of_adLoaderClient_0(),
	AdLoader_t554394170::get_offset_of_OnAdFailedToLoad_1(),
	AdLoader_t554394170::get_offset_of_OnCustomNativeTemplateAdLoaded_2(),
	AdLoader_t554394170::get_offset_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3(),
	AdLoader_t554394170::get_offset_of_U3CAdUnitIdU3Ek__BackingField_4(),
	AdLoader_t554394170::get_offset_of_U3CAdTypesU3Ek__BackingField_5(),
	AdLoader_t554394170::get_offset_of_U3CTemplateIdsU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (Builder_t54889671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2228[4] = 
{
	Builder_t54889671::get_offset_of_U3CAdUnitIdU3Ek__BackingField_0(),
	Builder_t54889671::get_offset_of_U3CAdTypesU3Ek__BackingField_1(),
	Builder_t54889671::get_offset_of_U3CTemplateIdsU3Ek__BackingField_2(),
	Builder_t54889671::get_offset_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (AdPosition_t2595513602)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2229[8] = 
{
	AdPosition_t2595513602::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (AdRequest_t3179524098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[9] = 
{
	0,
	0,
	AdRequest_t3179524098::get_offset_of_U3CTestDevicesU3Ek__BackingField_2(),
	AdRequest_t3179524098::get_offset_of_U3CKeywordsU3Ek__BackingField_3(),
	AdRequest_t3179524098::get_offset_of_U3CBirthdayU3Ek__BackingField_4(),
	AdRequest_t3179524098::get_offset_of_U3CGenderU3Ek__BackingField_5(),
	AdRequest_t3179524098::get_offset_of_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6(),
	AdRequest_t3179524098::get_offset_of_U3CExtrasU3Ek__BackingField_7(),
	AdRequest_t3179524098::get_offset_of_U3CMediationExtrasU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (Builder_t2008174359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2231[7] = 
{
	Builder_t2008174359::get_offset_of_U3CTestDevicesU3Ek__BackingField_0(),
	Builder_t2008174359::get_offset_of_U3CKeywordsU3Ek__BackingField_1(),
	Builder_t2008174359::get_offset_of_U3CBirthdayU3Ek__BackingField_2(),
	Builder_t2008174359::get_offset_of_U3CGenderU3Ek__BackingField_3(),
	Builder_t2008174359::get_offset_of_U3CChildDirectedTreatmentTagU3Ek__BackingField_4(),
	Builder_t2008174359::get_offset_of_U3CExtrasU3Ek__BackingField_5(),
	Builder_t2008174359::get_offset_of_U3CMediationExtrasU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (AdSize_t3231673570), -1, sizeof(AdSize_t3231673570_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2232[8] = 
{
	AdSize_t3231673570::get_offset_of_isSmartBanner_0(),
	AdSize_t3231673570::get_offset_of_width_1(),
	AdSize_t3231673570::get_offset_of_height_2(),
	AdSize_t3231673570_StaticFields::get_offset_of_Banner_3(),
	AdSize_t3231673570_StaticFields::get_offset_of_MediumRectangle_4(),
	AdSize_t3231673570_StaticFields::get_offset_of_IABBanner_5(),
	AdSize_t3231673570_StaticFields::get_offset_of_Leaderboard_6(),
	AdSize_t3231673570_StaticFields::get_offset_of_SmartBanner_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (BannerView_t1745853549), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2233[6] = 
{
	BannerView_t1745853549::get_offset_of_client_0(),
	BannerView_t1745853549::get_offset_of_OnAdLoaded_1(),
	BannerView_t1745853549::get_offset_of_OnAdFailedToLoad_2(),
	BannerView_t1745853549::get_offset_of_OnAdOpening_3(),
	BannerView_t1745853549::get_offset_of_OnAdClosed_4(),
	BannerView_t1745853549::get_offset_of_OnAdLeavingApplication_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (CustomNativeEventArgs_t2658458077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2234[1] = 
{
	CustomNativeEventArgs_t2658458077::get_offset_of_U3CnativeAdU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (CustomNativeTemplateAd_t2034144705), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2235[1] = 
{
	CustomNativeTemplateAd_t2034144705::get_offset_of_client_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (Gender_t3528073263)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2236[4] = 
{
	Gender_t3528073263::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (PurchaseResolutionType_t467326495)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2238[5] = 
{
	PurchaseResolutionType_t467326495::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (InterstitialAd_t3805611425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2239[6] = 
{
	InterstitialAd_t3805611425::get_offset_of_client_0(),
	InterstitialAd_t3805611425::get_offset_of_OnAdLoaded_1(),
	InterstitialAd_t3805611425::get_offset_of_OnAdFailedToLoad_2(),
	InterstitialAd_t3805611425::get_offset_of_OnAdOpening_3(),
	InterstitialAd_t3805611425::get_offset_of_OnAdClosed_4(),
	InterstitialAd_t3805611425::get_offset_of_OnAdLeavingApplication_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (MediationExtras_t1641207307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2240[1] = 
{
	MediationExtras_t1641207307::get_offset_of_U3CExtrasU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (NativeExpressAdView_t1750838217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2241[6] = 
{
	NativeExpressAdView_t1750838217::get_offset_of_client_0(),
	NativeExpressAdView_t1750838217::get_offset_of_OnAdLoaded_1(),
	NativeExpressAdView_t1750838217::get_offset_of_OnAdFailedToLoad_2(),
	NativeExpressAdView_t1750838217::get_offset_of_OnAdOpening_3(),
	NativeExpressAdView_t1750838217::get_offset_of_OnAdClosed_4(),
	NativeExpressAdView_t1750838217::get_offset_of_OnAdLeavingApplication_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (Reward_t1753549929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2242[2] = 
{
	Reward_t1753549929::get_offset_of_U3CTypeU3Ek__BackingField_1(),
	Reward_t1753549929::get_offset_of_U3CAmountU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (RewardBasedVideoAd_t2581948736), -1, sizeof(RewardBasedVideoAd_t2581948736_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2243[9] = 
{
	RewardBasedVideoAd_t2581948736::get_offset_of_client_0(),
	RewardBasedVideoAd_t2581948736_StaticFields::get_offset_of_instance_1(),
	RewardBasedVideoAd_t2581948736::get_offset_of_OnAdLoaded_2(),
	RewardBasedVideoAd_t2581948736::get_offset_of_OnAdFailedToLoad_3(),
	RewardBasedVideoAd_t2581948736::get_offset_of_OnAdOpening_4(),
	RewardBasedVideoAd_t2581948736::get_offset_of_OnAdStarted_5(),
	RewardBasedVideoAd_t2581948736::get_offset_of_OnAdClosed_6(),
	RewardBasedVideoAd_t2581948736::get_offset_of_OnAdRewarded_7(),
	RewardBasedVideoAd_t2581948736::get_offset_of_OnAdLeavingApplication_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (DummyClient_t1330686537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2244[8] = 
{
	DummyClient_t1330686537::get_offset_of_OnAdLoaded_0(),
	DummyClient_t1330686537::get_offset_of_OnAdFailedToLoad_1(),
	DummyClient_t1330686537::get_offset_of_OnAdOpening_2(),
	DummyClient_t1330686537::get_offset_of_OnAdStarted_3(),
	DummyClient_t1330686537::get_offset_of_OnAdClosed_4(),
	DummyClient_t1330686537::get_offset_of_OnAdRewarded_5(),
	DummyClient_t1330686537::get_offset_of_OnAdLeavingApplication_6(),
	DummyClient_t1330686537::get_offset_of_OnCustomNativeTemplateAdLoaded_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (Utils_t1179620951), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (GoogleMobileAdsClientFactory_t898766308), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (NativeAdTypes_t3944121833)+ sizeof (Il2CppObject), sizeof(NativeAdTypes_t3944121833_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2253[3] = 
{
	NativeAdTypes_t3944121833::get_offset_of_CustomTemplateAd_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NativeAdTypes_t3944121833::get_offset_of_AppInstallAd_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NativeAdTypes_t3944121833::get_offset_of_ContentAd_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (AdLoaderClient_t506419447), -1, sizeof(AdLoaderClient_t506419447_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2254[7] = 
{
	AdLoaderClient_t506419447::get_offset_of_adLoaderPtr_0(),
	AdLoaderClient_t506419447::get_offset_of_adLoaderClientPtr_1(),
	AdLoaderClient_t506419447::get_offset_of_customNativeTemplateCallbacks_2(),
	AdLoaderClient_t506419447::get_offset_of_OnCustomNativeTemplateAdLoaded_3(),
	AdLoaderClient_t506419447::get_offset_of_OnAdFailedToLoad_4(),
	AdLoaderClient_t506419447_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_5(),
	AdLoaderClient_t506419447_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t3126817269), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t619382744), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (BannerClient_t2837939223), -1, sizeof(BannerClient_t2837939223_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2257[17] = 
{
	BannerClient_t2837939223::get_offset_of_bannerViewPtr_0(),
	BannerClient_t2837939223::get_offset_of_bannerClientPtr_1(),
	BannerClient_t2837939223::get_offset_of_OnAdLoaded_2(),
	BannerClient_t2837939223::get_offset_of_OnAdFailedToLoad_3(),
	BannerClient_t2837939223::get_offset_of_OnAdOpening_4(),
	BannerClient_t2837939223::get_offset_of_OnAdClosed_5(),
	BannerClient_t2837939223::get_offset_of_OnAdLeavingApplication_6(),
	BannerClient_t2837939223_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
	BannerClient_t2837939223_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_8(),
	BannerClient_t2837939223_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_9(),
	BannerClient_t2837939223_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_10(),
	BannerClient_t2837939223_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_11(),
	BannerClient_t2837939223_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_12(),
	BannerClient_t2837939223_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_13(),
	BannerClient_t2837939223_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_14(),
	BannerClient_t2837939223_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_15(),
	BannerClient_t2837939223_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (GADUAdViewDidReceiveAdCallback_t3611450851), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (GADUAdViewDidFailToReceiveAdWithErrorCallback_t2294077762), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (GADUAdViewWillPresentScreenCallback_t2607757429), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (GADUAdViewDidDismissScreenCallback_t2257715507), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (GADUAdViewWillLeaveApplicationCallback_t1946169147), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (CustomNativeTemplateClient_t3776928493), -1, sizeof(CustomNativeTemplateClient_t3776928493_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2263[4] = 
{
	CustomNativeTemplateClient_t3776928493::get_offset_of_customNativeAdPtr_0(),
	CustomNativeTemplateClient_t3776928493::get_offset_of_customNativeTemplateAdClientPtr_1(),
	CustomNativeTemplateClient_t3776928493::get_offset_of_clickHandler_2(),
	CustomNativeTemplateClient_t3776928493_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (GADUNativeCustomTemplateDidReceiveClick_t3121063597), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (Externs_t2948936873), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (InterstitialClient_t2538051773), -1, sizeof(InterstitialClient_t2538051773_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2266[12] = 
{
	InterstitialClient_t2538051773::get_offset_of_interstitialPtr_0(),
	InterstitialClient_t2538051773::get_offset_of_interstitialClientPtr_1(),
	InterstitialClient_t2538051773::get_offset_of_OnAdLoaded_2(),
	InterstitialClient_t2538051773::get_offset_of_OnAdFailedToLoad_3(),
	InterstitialClient_t2538051773::get_offset_of_OnAdOpening_4(),
	InterstitialClient_t2538051773::get_offset_of_OnAdClosed_5(),
	InterstitialClient_t2538051773::get_offset_of_OnAdLeavingApplication_6(),
	InterstitialClient_t2538051773_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
	InterstitialClient_t2538051773_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_8(),
	InterstitialClient_t2538051773_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_9(),
	InterstitialClient_t2538051773_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_10(),
	InterstitialClient_t2538051773_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (GADUInterstitialDidReceiveAdCallback_t3343584307), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1829207408), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (GADUInterstitialWillPresentScreenCallback_t387623197), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (GADUInterstitialDidDismissScreenCallback_t4025611083), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (GADUInterstitialWillLeaveApplicationCallback_t216612155), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (MonoPInvokeCallbackAttribute_t1970456718), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (NativeExpressAdClient_t2805846113), -1, sizeof(NativeExpressAdClient_t2805846113_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2273[17] = 
{
	NativeExpressAdClient_t2805846113::get_offset_of_nativeExpressAdViewPtr_0(),
	NativeExpressAdClient_t2805846113::get_offset_of_nativeExpressAdClientPtr_1(),
	NativeExpressAdClient_t2805846113::get_offset_of_OnAdLoaded_2(),
	NativeExpressAdClient_t2805846113::get_offset_of_OnAdFailedToLoad_3(),
	NativeExpressAdClient_t2805846113::get_offset_of_OnAdOpening_4(),
	NativeExpressAdClient_t2805846113::get_offset_of_OnAdClosed_5(),
	NativeExpressAdClient_t2805846113::get_offset_of_OnAdLeavingApplication_6(),
	NativeExpressAdClient_t2805846113_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
	NativeExpressAdClient_t2805846113_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_8(),
	NativeExpressAdClient_t2805846113_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_9(),
	NativeExpressAdClient_t2805846113_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_10(),
	NativeExpressAdClient_t2805846113_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_11(),
	NativeExpressAdClient_t2805846113_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_12(),
	NativeExpressAdClient_t2805846113_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_13(),
	NativeExpressAdClient_t2805846113_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_14(),
	NativeExpressAdClient_t2805846113_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_15(),
	NativeExpressAdClient_t2805846113_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (GADUNativeExpressAdViewDidReceiveAdCallback_t2677637168), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (GADUNativeExpressAdViewDidFailToReceiveAdWithErrorCallback_t3648222485), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (GADUNativeExpressAdViewWillPresentScreenCallback_t3478706784), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (GADUNativeExpressAdViewDidDismissScreenCallback_t784484480), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (GADUNativeExpressAdViewWillLeaveApplicationCallback_t548165752), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (RewardBasedVideoAdClient_t2282664017), -1, sizeof(RewardBasedVideoAdClient_t2282664017_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2279[16] = 
{
	RewardBasedVideoAdClient_t2282664017::get_offset_of_rewardBasedVideoAdPtr_0(),
	RewardBasedVideoAdClient_t2282664017::get_offset_of_rewardBasedVideoAdClientPtr_1(),
	RewardBasedVideoAdClient_t2282664017::get_offset_of_OnAdLoaded_2(),
	RewardBasedVideoAdClient_t2282664017::get_offset_of_OnAdFailedToLoad_3(),
	RewardBasedVideoAdClient_t2282664017::get_offset_of_OnAdOpening_4(),
	RewardBasedVideoAdClient_t2282664017::get_offset_of_OnAdStarted_5(),
	RewardBasedVideoAdClient_t2282664017::get_offset_of_OnAdClosed_6(),
	RewardBasedVideoAdClient_t2282664017::get_offset_of_OnAdRewarded_7(),
	RewardBasedVideoAdClient_t2282664017::get_offset_of_OnAdLeavingApplication_8(),
	RewardBasedVideoAdClient_t2282664017_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_9(),
	RewardBasedVideoAdClient_t2282664017_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_10(),
	RewardBasedVideoAdClient_t2282664017_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_11(),
	RewardBasedVideoAdClient_t2282664017_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_12(),
	RewardBasedVideoAdClient_t2282664017_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_13(),
	RewardBasedVideoAdClient_t2282664017_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_14(),
	RewardBasedVideoAdClient_t2282664017_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (GADURewardBasedVideoAdDidReceiveAdCallback_t4169257859), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t862929376), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (GADURewardBasedVideoAdDidOpenCallback_t587935421), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (GADURewardBasedVideoAdDidStartCallback_t25341677), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (GADURewardBasedVideoAdDidCloseCallback_t2453903099), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (GADURewardBasedVideoAdDidRewardCallback_t129051320), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (GADURewardBasedVideoAdWillLeaveApplicationCallback_t2167763867), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (Utils_t984021165), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (AppstoreHandler_t4742128), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (AppstoreTestScene_t652637152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2289[2] = 
{
	AppstoreTestScene_t652637152::get_offset_of_m_appID_IOS_2(),
	AppstoreTestScene_t652637152::get_offset_of_m_appID_Android_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2290[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (AudioManager_t4222704959), -1, sizeof(AudioManager_t4222704959_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2291[5] = 
{
	AudioManager_t4222704959_StaticFields::get_offset_of_OnSoundStatusChangedEvent_2(),
	AudioManager_t4222704959_StaticFields::get_offset_of_OnMusicStatusChangedEvent_3(),
	AudioManager_t4222704959::get_offset_of_isSoundEnabled_4(),
	AudioManager_t4222704959::get_offset_of_isMusicEnabled_5(),
	AudioManager_t4222704959_StaticFields::get_offset_of__instance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (Ball_t3972794301), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (Cannon_t3532423045), -1, sizeof(Cannon_t3532423045_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2293[5] = 
{
	Cannon_t3532423045_StaticFields::get_offset_of_instance_2(),
	Cannon_t3532423045::get_offset_of_CannonPoint_3(),
	Cannon_t3532423045::get_offset_of_FiringBall_4(),
	Cannon_t3532423045::get_offset_of_travelSpeed_5(),
	Cannon_t3532423045::get_offset_of_rotateSpeed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (AdController_t332951955), -1, sizeof(AdController_t332951955_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2294[6] = 
{
	AdController_t332951955_StaticFields::get_offset_of_instance_2(),
	AdController_t332951955::get_offset_of_interstitialBannerFrequency_3(),
	AdController_t332951955::get_offset_of_interstitial_4(),
	AdController_t332951955::get_offset_of_bannerView_5(),
	AdController_t332951955::get_offset_of_bannerRequest_6(),
	AdController_t332951955::get_offset_of_refreshInterval_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (BGMusicController_t4257240066), -1, sizeof(BGMusicController_t4257240066_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2295[2] = 
{
	BGMusicController_t4257240066_StaticFields::get_offset_of_instance_2(),
	BGMusicController_t4257240066::get_offset_of_ispaused_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (Constants_t2437308775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2296[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (GameController_t3607102586), -1, sizeof(GameController_t3607102586_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2297[13] = 
{
	GameController_t3607102586_StaticFields::get_offset_of_instance_2(),
	GameController_t3607102586::get_offset_of_UICamera_3(),
	GameController_t3607102586::get_offset_of_UICanvas_4(),
	GameController_t3607102586::get_offset_of_eventSystem_5(),
	GameController_t3607102586::get_offset_of_Levels_6(),
	GameController_t3607102586::get_offset_of_Player_7(),
	GameController_t3607102586::get_offset_of_GameColors_8(),
	GameController_t3607102586::get_offset_of_GameColor_9(),
	GameController_t3607102586::get_offset_of_onTheFly_10(),
	GameController_t3607102586::get_offset_of_gameStatus_11(),
	GameController_t3607102586::get_offset_of_isGamePaused_12(),
	GameController_t3607102586::get_offset_of_GameScreens_13(),
	GameController_t3607102586::get_offset_of_LastScreen_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (GameStatus_t3461812585)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2298[4] = 
{
	GameStatus_t3461812585::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (U3CSpawnUIScreenU3Ec__AnonStorey2_t3909795947), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2299[1] = 
{
	U3CSpawnUIScreenU3Ec__AnonStorey2_t3909795947::get_offset_of_name_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
