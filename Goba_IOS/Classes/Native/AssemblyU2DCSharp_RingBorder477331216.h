﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RingBorder
struct  RingBorder_t477331216  : public Il2CppObject
{
public:
	// System.String RingBorder::<ringPrefab>k__BackingField
	String_t* ___U3CringPrefabU3Ek__BackingField_0;
	// System.Single RingBorder::<rotationSpeed>k__BackingField
	float ___U3CrotationSpeedU3Ek__BackingField_1;
	// System.Single RingBorder::<rotationDirection>k__BackingField
	float ___U3CrotationDirectionU3Ek__BackingField_2;
	// UnityEngine.Vector2 RingBorder::<ringPosition>k__BackingField
	Vector2_t2243707579  ___U3CringPositionU3Ek__BackingField_3;
	// System.Single RingBorder::<rotation>k__BackingField
	float ___U3CrotationU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CringPrefabU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RingBorder_t477331216, ___U3CringPrefabU3Ek__BackingField_0)); }
	inline String_t* get_U3CringPrefabU3Ek__BackingField_0() const { return ___U3CringPrefabU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CringPrefabU3Ek__BackingField_0() { return &___U3CringPrefabU3Ek__BackingField_0; }
	inline void set_U3CringPrefabU3Ek__BackingField_0(String_t* value)
	{
		___U3CringPrefabU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CringPrefabU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CrotationSpeedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RingBorder_t477331216, ___U3CrotationSpeedU3Ek__BackingField_1)); }
	inline float get_U3CrotationSpeedU3Ek__BackingField_1() const { return ___U3CrotationSpeedU3Ek__BackingField_1; }
	inline float* get_address_of_U3CrotationSpeedU3Ek__BackingField_1() { return &___U3CrotationSpeedU3Ek__BackingField_1; }
	inline void set_U3CrotationSpeedU3Ek__BackingField_1(float value)
	{
		___U3CrotationSpeedU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CrotationDirectionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RingBorder_t477331216, ___U3CrotationDirectionU3Ek__BackingField_2)); }
	inline float get_U3CrotationDirectionU3Ek__BackingField_2() const { return ___U3CrotationDirectionU3Ek__BackingField_2; }
	inline float* get_address_of_U3CrotationDirectionU3Ek__BackingField_2() { return &___U3CrotationDirectionU3Ek__BackingField_2; }
	inline void set_U3CrotationDirectionU3Ek__BackingField_2(float value)
	{
		___U3CrotationDirectionU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CringPositionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RingBorder_t477331216, ___U3CringPositionU3Ek__BackingField_3)); }
	inline Vector2_t2243707579  get_U3CringPositionU3Ek__BackingField_3() const { return ___U3CringPositionU3Ek__BackingField_3; }
	inline Vector2_t2243707579 * get_address_of_U3CringPositionU3Ek__BackingField_3() { return &___U3CringPositionU3Ek__BackingField_3; }
	inline void set_U3CringPositionU3Ek__BackingField_3(Vector2_t2243707579  value)
	{
		___U3CringPositionU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CrotationU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RingBorder_t477331216, ___U3CrotationU3Ek__BackingField_4)); }
	inline float get_U3CrotationU3Ek__BackingField_4() const { return ___U3CrotationU3Ek__BackingField_4; }
	inline float* get_address_of_U3CrotationU3Ek__BackingField_4() { return &___U3CrotationU3Ek__BackingField_4; }
	inline void set_U3CrotationU3Ek__BackingField_4(float value)
	{
		___U3CrotationU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
