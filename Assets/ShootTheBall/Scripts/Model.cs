﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Model {
}

public class RingBorder{
	public string ringPrefab { get; set; }
	public float rotationSpeed { get; set; }
	public float rotationDirection { get; set; }
	public Vector2 ringPosition { get; set; }
	public float rotation { get; set; }


	public RingBorder(JToken data){
		JObject o = data.Value<JObject> ();

		ringPrefab = o ["ring"].ToString();
		rotationSpeed = (float)o ["rotationSpeed"];
		rotationDirection = (float)o ["rotationDirection"];
		ringPosition = new Vector2 ((float)o ["positionX"], (float)o ["positionY"]);
		rotation = (float)o ["rotation"];
	}
}

public class Level{
	public RingBorder[] rings{ get; set; }
	public int ballCount;
	public Level(JToken data){
		ballCount = (int)data ["ballCount"];
		JArray r = JArray.Parse (data ["rings"].ToString ());;
		rings = new RingBorder[r.Count];
		for (int i = 0; i < r.Count; i++) {
			rings [i] = new RingBorder (r [i]);
		}

	}
}

public class Levels{
	public Level[] levels { get; set; }
	public Levels(JArray data){
		levels = new Level[data.Count];
		for (int i = 0; i < data.Count; i++) {
			JObject o = JObject.Parse (data [i].ToString ());
			levels [i] = new Level (JToken.Parse (o.ToString ()));
		}
	}
}

public class Player{
	public int level { get; set; }
	public int adsFrequency { get; set; }
	public int levelTry { get; set; }
}

public class GameColor{
	public Color backgroudColor { get; set; }
	public Color targetBackColor { get; set; }
	public Color targetFrontColor { get; set; }
	public Color bulletColor { get; set; }
	public Color cannonColor { get; set; }
	public Color borderColor { get; set; }

	public GameColor(JToken data){
		JArray bc = JArray.Parse (data ["background"].ToString ());
		backgroudColor = new Color ((float)bc [0] / 255, (float)bc [1] / 255, (float)bc [2] / 255, 1);
		JArray tbc = JArray.Parse (data ["targetBack"].ToString ());
		targetBackColor = new Color ((float)tbc [0] / 255, (float)tbc [1] / 255, (float)tbc [2] / 255, 1);
		JArray tfc = JArray.Parse (data ["targetFront"].ToString ());
		targetFrontColor = new Color ((float)tfc [0] / 255, (float)tfc [1] / 255, (float)tfc [2] / 255, 1);
		JArray cc = JArray.Parse (data ["cannon"].ToString ());
		cannonColor = new Color ((float)cc [0] / 255, (float)cc [1] / 255, (float)cc [2] / 255, 1);
		JArray buc = JArray.Parse (data ["bullet"].ToString ());
		bulletColor = new Color ((float)buc [0] / 255, (float)buc [1] / 255, (float)buc [2] / 255, 1);
		JArray boc = JArray.Parse (data ["borders"].ToString ());
		borderColor = new Color ((float)boc [0] / 255, (float)boc [1] / 255, (float)boc [2] / 255, 1);
	}
}

public class GameColors{
	public GameColor[] gameColors{ get; set; }
	public GameColors(JArray data){
		gameColors = new GameColor[data.Count];
		for (int i = 0; i < data.Count; i++) {
			JObject o = JObject.Parse (data [i].ToString ());
			gameColors [i] = new GameColor (JToken.Parse (o.ToString ()));
		}
	}
}