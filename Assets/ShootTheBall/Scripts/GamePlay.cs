﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Analytics;
using UnityEngine.EventSystems;

public class GamePlay : MonoBehaviour, IPointerDownHandler
{
	public static GamePlay instance;
	public Text txtScore;
	public Text ballCount;
	public GameObject gameOverPanel;

	public AudioClip SuccessHit;
	public AudioClip RingHit;

	public List<Color> BGColors = new List<Color>();

	private bool isRescued;
	private int targetScore;

	public GameObject HowToPanel;

	public GameObject ring;

	public SpriteRenderer background;
	public SpriteRenderer cannonBall;
	public Image targetBack;
	public Image targetFront;


	[HideInInspector] public int score = 0;
	[HideInInspector] public bool isGamePlay; 
	[HideInInspector] private int balls = 0;
	// event for score updation.
	//public static event Action<int> OnScoreUpdatedEvent;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake()
	{
		if (instance == null) {
			instance = this;
			return;
		}
	}

	/// <summary>
	/// Raises the enable event.
	/// </summary>
	void OnEnable()
	{
		BGMusicController.instance.StartBGMusic ();
		isGamePlay = true;
		isRescued = false;
		score = 0;
		txtScore.text = "0";
		targetScore = balls = GameController.instance.Levels.levels [GameController.instance.Player.level].ballCount;
		ballCount.text = balls.ToString ();
		if (GameController.instance.Player.level == 0) {
			HowToPanel.SetActive (true);
		} else {
			HowToPanel.SetActive (false);
		}

		if (GameController.instance.Player.level > 0) {
			background.color = GameController.instance.GameColor.backgroudColor;
			cannonBall.color = GameController.instance.GameColor.cannonColor;
			targetBack.color = GameController.instance.GameColor.targetBackColor;
			targetFront.color = GameController.instance.GameColor.targetFrontColor;
			ballCount.color = GameController.instance.GameColor.bulletColor;
		}
	}

	/// <summary>
	/// Raises the game over event.
	/// </summary>
	public void OnGameOver ()
	{
		if (AudioManager.instance.isSoundEnabled) {
			GetComponent<AudioSource> ().PlayOneShot (RingHit);
		}

		if (!isRescued) {
			//show watch video
			isGamePlay = false;
			Invoke ("setGameOverPanel", 1F);

		} else {
			isGamePlay = false;
			Invoke ("ExecuteGameOver", 1F);
			gameOverPanel.SetActive(false);

		}
	}

	private void setGameOverPanel(){
		gameOverPanel.SetActive(true);

	}

	public void RescuePlayer(){
		balls += GameController.instance.onTheFly;
		GameController.instance.onTheFly = 0;
		ballCount.text = balls.ToString ();
		isGamePlay = true;
		isRescued = true;
		gameOverPanel.SetActive (false);
		int playerLevel = GameController.instance.Player.level;
		int levelTry = GameController.instance.Player.levelTry;
		Analytics.CustomEvent("Resque Level", new Dictionary<string, object>
			{
				{ "level", playerLevel },
				{ "try", levelTry },
				{ "balls", balls}
			});
		
	}

	public void ExecuteGameOver()
	{
		GameController.instance.gameStatus = GameController.GameStatus.Fail;
		GameController.instance.onTheFly = 0;
		GameController.instance.Player.levelTry++;
		PlayerPrefs.SetInt ("try", GameController.instance.Player.levelTry); 
		//gameObject.GetComponent<ShakeObject> ().StopShake ();

		gameOverPanel.SetActive (false);
		GameController.instance.OnGameOver (gameObject);
		BGMusicController.instance.StartBGMusic ();
		int playerLevel = GameController.instance.Player.level;
		int levelTry = GameController.instance.Player.levelTry;

		Analytics.CustomEvent("Game Over Level", new Dictionary<string, object>
			{
				{ "level", playerLevel },
				{ "try", levelTry },
				{ "balls", balls},
				{ "isResqued", isRescued}
			});
		AdController.instance.ShowInterstitialBanner (isRescued);
	}

	public void ExecuteNewLevel()
	{
		GameController.instance.Player.levelTry = 0;
		PlayerPrefs.SetInt ("try", GameController.instance.Player.levelTry);
		GameController.instance.Player.level++;
		PlayerPrefs.SetInt("Level", GameController.instance.Player.level);
		GameController.instance.gameStatus = GameController.GameStatus.Success;
		GameController.instance.OnGameOver (gameObject);
		AdController.instance.ShowInterstitialBanner (isRescued);
	}

	/// <summary>
	/// Raises the score updated event.
	/// </summary>
	/// <param name="count">Count.</param>
	public void OnScoreUpdated (int count)
	{
		score += count;
		txtScore.text = score.ToString();
		//OnScoreUpdatedEvent.Invoke (score);
			
		if (AudioManager.instance.isSoundEnabled) {
			GetComponent<AudioSource> ().PlayOneShot (SuccessHit);
		}

		if (score == targetScore) {
			ring.GetComponent<Ring> ().EndGameAnimation ();
			Invoke ("ExecuteNewLevel", 1F);
		}
		GameController.instance.onTheFly--;
	}




	#region IPointerDownHandler implementation
	/// <summary>
	/// Raises the pointer down event.
	/// Ball will be fired on pointer down in gameplay mode.
	/// </summary>
	/// <param name="eventData">Event data.</param>
	public void OnPointerDown (PointerEventData eventData)
	{
		if (isGamePlay && balls > 0) 
		{
			balls--;
			ballCount.text = balls.ToString ();

			Cannon.instance.FireBall();	
		}
	}
	#endregion

	/// <summary>
	/// Raises the pause button pressed event.
	/// </summary>
	public void OnPauseButtonPressed()
	{
		if (InputManager.instance.canInput ()) {
			InputManager.instance.DisableTouchForDelay ();
			InputManager.instance.AddButtonTouchEffect ();
			GameController.instance.PauseGame();
		}
	}


}
