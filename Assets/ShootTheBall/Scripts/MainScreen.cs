﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class MainScreen : MonoBehaviour {

	public Text txtStatus1;
	public Text txtStatus2;
	public Text levelText;
	public GameObject endOfLevels;

	private int interstitialBannerFrequency = -1;

	/// <summary>
	/// Raises the enable event.
	/// </summary>
	void OnEnable()
	{
		

		levelText.text = "";
		txtStatus1.text = "";
		txtStatus2.text = "";
		//txtBest.text = "BEST : " + PlayerPrefs.GetInt ("BestScore", 0).ToString("00");
		if (PlayerPrefs.GetInt ("Level") != 0) {
			levelText.text = "LEVEL " + PlayerPrefs.GetInt ("Level").ToString ();
		}

	
		if (GameController.instance) {
			if (GameController.instance.gameStatus != null) {
				if (GameController.instance.gameStatus == GameController.GameStatus.Fail) {
					txtStatus1.text = "FAIL!";
					txtStatus2.text = "RETRY LEVEL...";
					txtStatus1.color = new Color (232F / 255F, 29F / 255F, 98F / 255F);
					txtStatus2.color = new Color (232F / 255F, 29F / 255F, 98F / 255F);
				} else if (GameController.instance.gameStatus == GameController.GameStatus.Success) {
					txtStatus1.text = "SUCCESS";
					txtStatus2.text = "NEXT LEVEL...";
					txtStatus1.color = new Color (89F / 238F, 174F / 255F, 97F / 255F);
					txtStatus2.color = new Color (89F / 238F, 174F / 255F, 97F / 255F);

				}
				if (GameController.instance.Player.level > GameController.instance.Levels.levels.Length - 1) {
					endOfLevels.SetActive (true);
				}
			}
		}

		if (GameController.instance.Player.level > 0) {
			int rnd = UnityEngine.Random.Range (0, GameController.instance.GameColors.gameColors.Length);
			GameController.instance.GameColor = GameController.instance.GameColors.gameColors [rnd];
		}

	}

	void Start(){
		AdController.instance.RequestBanner ();
	}



	/// <summary>
	/// Raises the play button pressed event.
	/// </summary>
	public void OnPlayButtonPressed()
	{
		if (InputManager.instance.canInput()) {
			InputManager.instance.DisableTouchForDelay ();
			InputManager.instance.AddButtonTouchEffect ();
			GameController.instance.StartGamePlay(gameObject);
			Analytics.CustomEvent("Start Level", new Dictionary<string, object>
				{
					{ "level", GameController.instance.Player.level},
					{ "try", GameController.instance.Player.levelTry }
				});
		}
	}

	public void OnReviewButtonPressed()
	{
		if (InputManager.instance.canInput ()) {
			InputManager.instance.DisableTouchForDelay ();
			InputManager.instance.AddButtonTouchEffect ();
			#if UNITY_ANDROID
			Application.OpenURL(Constants.GOOGLEPLAY_URL);
			#else
			Application.OpenURL(Constants.APPSTORE_URL);
			#endif
		}
	}

	public void OnListButtonPressed()
	{
		if (InputManager.instance.canInput()) {
			InputManager.instance.DisableTouchForDelay ();
			InputManager.instance.AddButtonTouchEffect ();
			GameController.instance.viewerSc(gameObject);
		}
	}

	void OnFacebookButtonPressed ()
	{
		WWW www = new WWW(Constants.FACEBOOK_URL);
		StartCoroutine(WaitForFacebookRequest(www));
	}

	IEnumerator WaitForFacebookRequest(WWW www)
	{
		yield return www;

		// check for errors
		if (www.error == null)
		{
			
		}
		else
		{
			//error. Open normal address
			Application.OpenURL (Constants.FACEBOOK_URL_ALTERNATE);

		}    
	}
		

	public void OnTwitterButtonPressed(){
		WWW www = new WWW(Constants.TWITTER_URL);
		StartCoroutine(WaitForTwitterRequest(www));
	}
	IEnumerator WaitForTwitterRequest(WWW www)
	{
		yield return www;

		// check for errors
		if (www.error == null)
		{

		}
		else
		{
			//error. Open normal address
			Application.OpenURL (Constants.TWITTER_URL_ALTERNATE);

		}    
	}


}
