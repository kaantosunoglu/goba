﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using DentedPixel;

/// <summary>
/// This is the rotating ring of the game.
/// </summary>
public class Ring : MonoBehaviour 
{
	public static Ring instance;

		/// All the different typed of rings are assigned to this list from the inspector.
	//public List<GameObject> Rings = new List<GameObject>();
	GameObject[] currentRings;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake()
	{
		if (instance == null) {
			instance = this;
			return;
		}
	}

	/// <summary>
	/// Raises the enable event.
	/// </summary>
	void OnEnable()
	{
		
		currentRings = new GameObject[GameController.instance.Levels.levels[GameController.instance.Player.level].rings.Length];

		for (int i = 0; i < currentRings.Length; i++) {
			GameObject cr = Instantiate (Resources.Load("Prefabs/Rings/" + GameController.instance.Levels.levels [GameController.instance.Player.level].rings[i].ringPrefab, typeof(GameObject)), this.transform) as GameObject;
			cr.transform.localPosition =  GameController.instance.Levels.levels [GameController.instance.Player.level].rings[i].ringPosition;
				cr.transform.Rotate (0, 0, GameController.instance.Levels.levels [GameController.instance.Player.level].rings[i].rotation);
				cr.SetActive (true);
			if (GameController.instance.Player.level > 0) {
				Transform go = cr.transform.FindChild ("New Sprite"); 
				go.gameObject.GetComponent<SpriteRenderer> ().color = GameController.instance.GameColor.borderColor;
			}
				currentRings [i] = cr;
			}

	
		LeanTween.init();
		StartRotation ();
	}

	/// <summary>
	/// Raises the disable event.
	/// </summary>
	void OnDisable()
	{
		//GamePlay.OnScoreUpdatedEvent -= OnScoreUpdated;
		//EGTween.Stop (gameObject);
		LeanTween.cancelAll();
		for (int i = 0; i < currentRings.Length; i++) {
			//currentRings[i].SetActive (false);
			Destroy (currentRings [i]);

		}
		currentRings = null;
	}


	/// <summary>
	/// Starts the rotation.
	/// </summary>
	public void StartRotation()
	{
		
		for (int i = 0; i < currentRings.Length; i++) {
			LeanTween.rotateAround (currentRings [i], new Vector3(0, 0, GameController.instance.Levels.levels [GameController.instance.Player.level].rings[i].rotationDirection), 360.0f, (GameController.instance.Levels.levels [GameController.instance.Player.level].rings[i].rotationSpeed)).setLoopType (LeanTweenType.linear);
		}
	}


	public void EndGameAnimation(){
		for (int i = 0; i < currentRings.Length; i++) {
			LeanTween.scale(currentRings [i], new Vector3(4F, 4F), (float)((1 + i)*2));
			LeanTween.color (currentRings [i], new Color (1f, 1f, 1f, 0f), 0.5F);

		}
	}

	void Update(){

	}
}
