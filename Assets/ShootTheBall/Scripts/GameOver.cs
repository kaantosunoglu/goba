using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class GameOver : MonoBehaviour 
{
	
	public AudioClip GameOverAudio;

	/// <summary>
	/// Raises the enable event.
	/// </summary>
	void OnEnable()
	{
		BGMusicController.instance.PauseBGMusic ();
		if (AudioManager.instance.isSoundEnabled) {
			GetComponent<AudioSource>().PlayOneShot(GameOverAudio);
		}

		#if UNITY_ANDROID || UNITY_IOS
		//If ad is not available, rescue button will be deactivated.


		if (!UnityAds.instance.IsReady("rewardedVideo")) {
			transform.FindChild("Panel/btn-rescue").gameObject.SetActive(false);
		}
		else{
			transform.FindChild("Panel/btn-rescue").gameObject.SetActive(true);
		}
		#endif
	}

	/// <summary>
	/// Raises the replay button pressed event.
	/// </summary>
	public void OnReplayButtonPressed ()
	{
		if (InputManager.instance.canInput ()) {
			InputManager.instance.DisableTouchForDelay ();
			InputManager.instance.AddButtonTouchEffect ();
			GameController.instance.ReloadGame(gameObject);
		}
	}

	/// <summary>
	/// Raises the home button pressed event.
	/// </summary>
	public void OnHomeButtonPressed ()
	{
		if (InputManager.instance.canInput ()) {
			InputManager.instance.DisableTouchForDelay ();
			InputManager.instance.AddButtonTouchEffect ();
			GameController.instance.ExitToMainScreenFromGameOver(gameObject);
		}
	}

	/// <summary>
	/// Raises the rescue button pressed event.
	/// </summary>
	public void OnRescueButtonPressed()
	{
		if (InputManager.instance.canInput ()) {
			InputManager.instance.DisableTouchForDelay ();
			InputManager.instance.AddButtonTouchEffect ();

			//	

			#if UNITY_ANDROID || UNITY_IOS
			UnityAds.instance.ShowAdsWithResult ("rewardedVideo", result => 
			{
				if(result == true)
				{
					//Rescue Successful
					PlayerPrefs.SetInt("isRescued",1);
					GameController.instance.ResumeGame();
						AdController.instance.showBanner();
						BGMusicController.instance.PauseBGMusic ();
				}
				else 
				{
					//Rescue Failed
						AdController.instance.showBanner();
						BGMusicController.instance.PauseBGMusic ();
				}
			});
			#endif
		}
	}
}
