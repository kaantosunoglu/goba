﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Constants.
/// </summary>
public class Constants 
{
	public const string APPSTORE_URL = "1230958847";
	public const string GOOGLEPLAY_URL = "com.thequixoticssolutions.goba";
	public const string FACEBOOK_URL= "fb://page/TheQuixotics";
	public const string FACEBOOK_URL_ALTERNATE= "https://facebook.com/TheQuixotics";
	public const string TWITTER_URL = "twitter:///user?screen_name=the_Quixotics";
	public const string TWITTER_URL_ALTERNATE= "https://twitter.com/the_Quixotics";
	public const string ANDROID_INTERSTITIAL_AD_UNIT_ID = "ca-app-pub-9432369139321400/7838117578";
	public const string IOS_INTERSTITIAL_AD_UNIT_ID = "ca-app-pub-9432369139321400/6221783570";
	public const string ANDROID_BANNER_AD_UNIT_ID = "ca-app-pub-9432369139321400/6361384370";
	public const string IOS_BANNER_AD_UNIT_ID = "ca-app-pub-9432369139321400/3268317176";

}
