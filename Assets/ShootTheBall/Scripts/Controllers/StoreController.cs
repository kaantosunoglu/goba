﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OpenStore()
	{	
			
			if(!Application.isEditor)
			{
				#if UNITY_IPHONE
			AppstoreHandler.Instance.openAppInStore(Constants.APPSTORE_URL);
				#endif

				#if UNITY_ANDROID
			AppstoreHandler.Instance.openAppInStore(Constants.GOOGLEPLAY_URL);
				#endif
			} else
			{	Debug.Log("AppstoreTestScene:: Cannot view app in Editor.");
			}
		}

}
