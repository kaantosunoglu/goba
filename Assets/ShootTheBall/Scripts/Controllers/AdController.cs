﻿using UnityEngine;
using System;
using System.Collections;
using GoogleMobileAds;
using GoogleMobileAds.Api;

public class AdController : MonoBehaviour
{
	public static AdController instance;

	public int interstitialBannerFrequency = 0;

	private InterstitialAd interstitial;
	private BannerView bannerView;
	private AdRequest bannerRequest;
	private int refreshInterval = 1;

	// Use this for initialization
	void Start ()
	{
		RequestInterstitial ();
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	void Awake()
	{
		if (instance == null) {
			instance = this;
			return;
		}
		Destroy (gameObject);
	}

	public void RequestInterstitial()
	{
		#if UNITY_ANDROID
		string adUnitId = Constants.ANDROID_INTERSTITIAL_AD_UNIT_ID;
		#elif UNITY_IPHONE
		string adUnitId = Constants.IOS_INTERSTITIAL_AD_UNIT_ID;
		#endif

		if( interstitial != null )
			interstitial.Destroy();

		// Initialize an InterstitialAd.
		interstitial = new InterstitialAd(adUnitId);
		interstitial.OnAdClosed += insterstitialClosed;

		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		// Load the interstitial with the request.
		interstitial.LoadAd(request);
	}

	private void insterstitialClosed(object sender, EventArgs args){
		showBanner ();
		RequestInterstitial ();
	}

	public void RequestBanner(){
		#if UNITY_EDITOR
		string adUnitId = "unused";
		#elif UNITY_ANDROID
		string adUnitId = Constants.ANDROID_BANNER_AD_UNIT_ID;
		#elif UNITY_IPHONE
		string adUnitId = Constants.IOS_BANNER_AD_UNIT_ID;
		#else
		string adUnitId = "unexpected_platform";
		#endif
		Debug.Log ("RequestBanner");
		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);
		// Create an empty ad request.
		bannerRequest = new AdRequest.Builder().Build();
		// Load the banner with the request.
		//bannerView.Hide ();
		bannerView.OnAdLoaded += adloaded;
		bannerView.LoadAd(bannerRequest);

	}
	
		private void adloaded(object sender, EventArgs args){
		Debug.Log ("ON AD LOADED");
		}

	public void hideBanner(){
		bannerView.Hide ();
	}

	public void showBanner(){
		bannerView.Show ();
	}

	public void ShowInterstitialBanner(bool isRescued)
	{
		if (GameController.instance.Player.level < 5) {
			return;
		} else {
			if (interstitialBannerFrequency <= 0) {
				//show banner
				if (interstitial.IsLoaded()) {
					interstitial.Show();
					hideBanner ();
				}
				interstitialBannerFrequency = UnityEngine.Random.Range (1, 10);
			} else {
				
				interstitialBannerFrequency = interstitialBannerFrequency - (isRescued ? 1 : 2);
			}
		}
	}

		
}

