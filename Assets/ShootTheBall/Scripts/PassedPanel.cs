﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PassedPanel : MonoBehaviour {


	public Button appstoreButton;
	public Button googleplayButton;

	// Use this for initialization
	void Start () {
		#if UNITY_ANDROID
		appstoreButton.gameObject.SetActive(false);
		#else
		googleplayButton.gameObject.SetActive(false);
		#endif
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void gotoAppStore(){
		Application.OpenURL(Constants.APPSTORE_URL);
	}

	public void gotoGooglePlay(){
		Application.OpenURL(Constants.GOOGLEPLAY_URL);
	}

	public void gotoFacebook(){
		Application.OpenURL(Constants.FACEBOOK_URL);
	}
}
