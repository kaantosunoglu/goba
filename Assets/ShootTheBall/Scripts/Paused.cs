﻿using UnityEngine;
using System.Collections;

public class Paused : MonoBehaviour {

	/// <summary>
	/// Raises the enable event.
	/// </summary>
	void OnEnable()
	{
		BGMusicController.instance.PauseBGMusic ();
		GameController.instance.isGamePaused = true;

	}

	/// <summary>
	/// Raises the disable event.
	/// </summary>
	void OnDisable()
	{
		BGMusicController.instance.PauseBGMusic ();
		GameController.instance.isGamePaused = false;
	}



	/// <summary>
	/// Raises the exit button pressed event.
	/// </summary>
	public void OnExitButtonPressed()
	{
		if (InputManager.instance.canInput()) {
			InputManager.instance.DisableTouchForDelay ();
			InputManager.instance.AddButtonTouchEffect ();
			GameController.instance.ExitToMainScreenFromPause(gameObject);
			BGMusicController.instance.PauseBGMusic ();
		}

	}

	public void OnResumeButtonPressed()
	{
		if (InputManager.instance.canInput ()) {
			InputManager.instance.EnableTouch ();
			GameController.instance.ResumeAfterPauseGame();

		}
	}
}
