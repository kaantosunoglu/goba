﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Loader : MonoBehaviour {

	private int loadedContents = 0;

	// Use this for initialization
	void Start () {
		//PlayerPrefs.DeleteAll ();
		createLevels ();
		setPlayerPrefs ();
		GameController.instance.gameStatus = GameController.GameStatus.NotStarted;
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	private void createLevels(){

		JObject l = JObject.Parse (LoadLevels ());
		GameController.instance.Levels = new Levels (JArray.Parse(l["data"]["levels"].ToString()));
		JObject c = JObject.Parse (LoadColors ()); 
		GameController.instance.GameColors = new GameColors (JArray.Parse(c["data"]["colors"].ToString()));

		controlLoading ();
	}


	private string LoadLevels()
	{
		TextAsset targetFile = Resources.Load<TextAsset>("levels");
		return targetFile.text;

	}

	private string LoadColors()
	{
		TextAsset targetFile = Resources.Load<TextAsset>("colors");
		return targetFile.text;

	}

	private void setPlayerPrefs(){

		GameController.instance.Player = new Player ();
		if (PlayerPrefs.GetInt ("Level", 0)  == 0) {
			PlayerPrefs.SetInt ("Level", 0);
			GameController.instance.Player.level = 0;
		} else {
			GameController.instance.Player.level = PlayerPrefs.GetInt ("Level");
		}

		if (PlayerPrefs.GetInt ("Try", 0) == 0) {
			PlayerPrefs.SetInt ("Try", 0);
			GameController.instance.Player.levelTry = 0;
		} else {
			GameController.instance.Player.levelTry = PlayerPrefs.GetInt ("try");
		}

		GameController.instance.Player.adsFrequency = ((int)Random.Range (0, 10)) + 1;
		controlLoading ();
	}
	private void controlLoading(){
		loadedContents++;
		if (loadedContents > 1) {
			StartCoroutine(GoToGame ());
		}
	}

	private IEnumerator GoToGame()
	{
		yield return new WaitForSeconds(2);
		GameController.instance.GotoMainScreen (gameObject);
	}
}
